


var payments_table = $('#payments_table').DataTable({
    responsive: true,
    "language": {
        "search": "Filter current table:"
    },
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false,
    "ordering": false
});



var units_table = $('#units_table').DataTable({
    responsive: true,
    "language": {
        "search": "Filter current table:"
    },
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false
});



var admin_items_table = $('#admin_items_table').DataTable({
    responsive: true,
    "language": {
        "search": "Filter current table:"
    },
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false
});



var users_table = $('#users_table').DataTable({
    responsive: true,
    "language": {
        "search": "Filter current table:"
    },
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false
});


   function checkUser() {
        $('#server-msg-div').slideUp("slow");

        $.ajax(backendHostUrl + '/api/v1/current_users?admin=true', {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            },
            method: 'POST',
            data: JSON.stringify({
                'message': 'incoming user'
            }),
            contentType: 'application/json'
        }).then(function (data) {
            console.log(data)

            if (data.success) {

                if (data.is_driver) {
                       $('.admin_list_items').each(function(){
                   
                        $(this).css("display", "none");

                
                     });
                       
                }
                loginUser(data.user_dict.id);

            } else {
                logoutUser();
                console.log('auth failed')

            }

            // Refresh notebook display.
            console.log(data)
            brainTreeClientToken = data.client_token

        });


    }


  function displayServerMessage(data) {
                    $('#server_msg').text(data.message);
                if(data.success == true){
                  $('.alert').addClass('alert-success')
                   $('.alert').removeClass('alert-danger')
                    $('#server_msg_header').text('Success!')
                } else {
                  $('.alert').addClass('alert-danger')
                  $('.alert').removeClass('alert-success')
                    $('#server_msg_header').text('Error!')
                }
                // Refresh notebook display.
                console.log(data)
                
                $('#server-msg-div').slideUp("slow");
                $('#server-msg-div').slideDown("slow");
  }

