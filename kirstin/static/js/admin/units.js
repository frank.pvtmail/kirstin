    function getUnitSizes() {
        $.ajax(backendHostUrl + '/api/v1/admins/units/sizes', {
            /* Set header for the XMLHttpRequest to get data from the web server
    associated with userIdToken */
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {

            console.log(data)



            data.sizes.forEach(function(size) {
                $('#prefix').append('<option>' + size + '</option>');
            });

        });
    }



    function addPods() {

        t = $('#total').val();
        prefix = $('#prefix').val();
        pn = $('#position_number').val();
        un = $('#unit_name').val();


        $.ajax(backendHostUrl + '/api/v1/admins/units', {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            },
            method: 'POST',
            data: JSON.stringify({
                't': t,
                'prefix': prefix,
                'position_number': pn, 
                'unit_name': un

            }),
            contentType: 'application/json'
        }).then(function(data) {
            console.log(data);
           displayServerMessage(data)

            Messenger().post({
                message: data.message,
                type: 'success',
                showCloseButton: true
              });


        });
    }


   function initDates() {
        var now = new Date();

        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);

        var today = now.getFullYear() + "-" + (month) + "-" + (day);

        $('#start_date').val(today);
        $('#end_date').val(today);


    }

function initUnitManager(){
        getUnitSizes()
        localStorage.setItem("mode", "filter")
        localStorage.setItem("filter_name", 'available')
        localStorage.setItem("filter_value", 'false')
        localStorage.setItem("cursor", "None")
        units_table.clear().draw();
        $("#load_more").attr('disabled', false)
        fetchUnitsByFilter(localStorage.getItem('filter_name'), localStorage.getItem('filter_value'))

}


    function populateTableWithData(data){
            $(".loader").hide()
            console.log(data)

          if (data.next_page == null || data.next_page == 'None') {
            $("#load_more").attr('disabled', true)
        }

            localStorage.setItem("cursor", data.next_page);
            data.units.forEach(function(unit) {
               

                link = "#"
                if (unit.owner_name != 'No owner') {
                    link = "/users/" + unit.owner_id
                }
                var archived_text = ""
                if (unit.archived) {
                    archived_text = "UNIT NOT FOR RENT"

                } else {
                    archived_text = "UNIT AVAILABLE FOR RENT"

                }

                units_table.row.add([

                    '<a href="' + link + '">' + unit.owner_name + '</a>',


                    '<a href="/units/' + unit.id + '">' + unit.unit_name + '</a>',
                     unit.position_number ,
                    unit.dropoff_date,
                    unit.pickup_date,
                    unit.status,
                    unit.term_end,
                    archived_text

                ]).draw(false);
            });


    }


 function fetchUnitsByFilter(filter, value=true) {

        $.ajax(backendHostUrl + '/api/v1/admins/units?'+filter+'=' + value + "&p=" + localStorage.getItem("cursor") , {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {
                populateTableWithData(data)
            // Iterate over user data to display user's notes from database.
        });
    }



    function searchUnits(query) {
         
        $.ajax(backendHostUrl + '/api/v1/admins/units?search=' + query + "&p=" + localStorage.getItem("cursor") , {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {



                populateTableWithData(data)
            // Iterate over user data to display user's notes from database.
        });
    }


    $(function() {

       $("#add-pods").submit(function(event) {
            event.preventDefault();
            $('#server-msg-div').slideUp("slow");
            addPods();
        });
        
      

        var loadmoreBtn = $('#load_more');
        loadmoreBtn.click(function(event) {

            if (localStorage.getItem('mode') == "filter") {
                fetchUnitsByFilter(localStorage.getItem('filter_name'), localStorage.getItem('filter_value'))

         
            } else {
                searchUnits($('#name').val())
            }

            
        });

        var searchBtn = $('#search-btn');
        searchBtn.click(function(event) {
            $(".loader").show()
            localStorage.setItem("mode", "search")
            localStorage.setItem("cursor", "None")
            units_table.clear().draw();
            $("#load_more").attr('disabled', false)
            searchUnits($('#name').val())

        });


        var byFilterBtn = $(".filter-btn")
            byFilterBtn.click(function(event) {
                $(".loader").show()
                var filter_name = this.id.split(":")[0]
                var filter_value = this.id.split(":")[1]
                localStorage.setItem("mode", "filter")
                localStorage.setItem("filter_name", filter_name)
                localStorage.setItem("filter_value", filter_value)
                localStorage.setItem("cursor", "None")
                units_table.clear().draw();
                $("#load_more").attr('disabled', false)
                fetchUnitsByFilter(localStorage.getItem('filter_name'), localStorage.getItem('filter_value'))

        });

    });


$( document ).ready(function() {


    console.log()
    initDates();
    $("#load_more").attr('disabled', true)
    waitForUserAuth(initUnitManager);
});



