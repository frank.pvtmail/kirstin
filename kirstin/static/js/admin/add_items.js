  $( document ).ready(function() {

      waitForAuth();
      getCategories(); 
      
    });


function getCategories() {
  $.ajax(backendHostUrl + '/a/categories', {
    /* Set header for the XMLHttpRequest to get data from the web server
      associated with userIdToken */
    headers: {
      'Authorization': 'Bearer ' + userIdToken
    }
  }).then(function(data){
    $('#categories').children('option').remove();
  
    data.categories.forEach(function(category){
      $('#categories').append('<option>'+category+'</option>');
    });

  });
}

  function altPost(){

    var category  = $('#categories').find(":selected").text();
    var form = document.getElementById('add-item-form');
    var formData = new FormData(form);


    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState === 4) {
        //callback(request.response);
          console.log(request.response)
              Messenger().post({
                     message: request.response.message,
                     type: 'success',
                     showCloseButton: true
                 });
        
      }
    }
    request.open('POST', backendHostUrl + '/a/items?item_category=' + category);
    request.responseType = 'json';
    request.send(formData);
 

  }



          $(function(){

        var addBtn = $('#add-btn');
        addBtn.click(function(event) {
          
         
          event.preventDefault(); 
          altPost();    
          // var name = $('#name').val();
          // var category  = $('#categories').find(":selected").text();
          // var price = $('#price').val();
          // var description = $('#description').val();

          // var form = document.getElementById('add-item-form');
          // var formData = new FormData(form);


          //     $.ajax(backendHostUrl + '/a/items?item_category=' + category, {
          //       headers: {
          //         'Authorization': 'Bearer ' + userIdToken
          //       },
          //       method: 'POST',
          //       data: JSON.stringify({'name': name,'price': price,'category': category, 'description': description}),
          //       contentType : 'application/json'
          //     }).then(function(data){
          //         Messenger().post({
          //           message: 'Successfully added item',
          //           type: 'success',
          //           showCloseButton: true
          //       });
                  

          //     });
        });

        var addCatBtn = $('#add-category-btn');
        addCatBtn.click(function(event) {
          console.log('add category button pressed')
          
         
          event.preventDefault();     
          var name = $('#category_name').val();
          
          var description = $('#category_description').val();


              $.ajax(backendHostUrl + '/a/categories', {
                headers: {
                  'Authorization': 'Bearer ' + userIdToken
                },
                method: 'POST',
                data: JSON.stringify({'name': name, 'description': description}),
                contentType : 'application/json'
              }).then(function(data){
                console.log(data)
                getCategories();
                  Messenger().post({
                    message: data.message,
                    type: data.status,
                    showCloseButton: true
                });
                  

              });
        });


      });


  function waitForAuth() {
    if (is_auth == false) {//we want it to match
      setTimeout(waitForAuth, 50);//wait 50 millisecnds then recheck
      return;
    }

    //getUnits('all', 'undefined');
    console.log("success!")
  }
