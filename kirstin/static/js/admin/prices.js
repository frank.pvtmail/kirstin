function getPrices() {
    $.ajax(backendHostUrl + '/api/v1/admins/units/prices', {
        /* Set header for the XMLHttpRequest to get data from the web server
    associated with userIdToken */
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){

        //drawChart(data.occupancy)
        console.log(data)
        $("#price_form").slideDown()
        $(".loader").hide()
        $("#sb").val(data.B)
        $("#mini").val(data.MP)
        $("#standard").val(data.P)
        $("#large").val(data.LP)
        $("#jumbo").val(data.JP)
        $("#c").val(data.C)
        $("#hc").val(data.HC)
        $("#delivery").val(data.delivery_cost)
        $("#per_km_cost").val(data.per_km_cost)
        $("#slot_limit").val(data.slot_limit)
        $("#max_distance").val(data.max_distance)
        $("#min_distance").val(data.min_distance)
        $("#cutoff").val(data.cutoff)

        $("#jp_lb_cost").val(data.jp_lb_cost)
        $("#c_lb_cost").val(data.c_lb_cost)
        $("#hc_lb_cost").val(data.hc_lb_cost)

       

    });
}

function postPrices(){

    /* Send note data to backend, storing in database with existing data
                associated with userIdToken */
    $.ajax(backendHostUrl + '/api/v1/admins/units/prices', {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        },
        method: 'POST',
        data: JSON.stringify({'mp': $('#mini').val(),
                              'p': $('#standard').val() ,
                              'lp': $('#large').val() ,
                              'jp': $('#jumbo').val(),
                              'c': $('#c').val(),
                              'sb' : $("#sb").val(),
                              'jp_lb_cost': $('#jp_lb_cost').val(),
                              'hc_lb_cost': $('#hc_lb_cost').val(), 
                              'c_lb_cost': $('#c_lb_cost').val(),
                              'hc': $('#hc').val(),
                              'delivery_cost':  $('#delivery').val(),
                              'slot_limit':  $('#slot_limit').val(),
                              'per_km_cost':  $('#per_km_cost').val(),
                              'min_distance':  $('#min_distance').val(),
                              'max_distance':  $('#max_distance').val(),
                              'cutoff': $('#cutoff').val()
                               }),


        contentType : 'application/json'
    }).then(function(data){
        displayServerMessage(data)
        console.log(data);
        // Refresh notebook display.
    });
}

$(function(){
    var updateBtn = $('#update-btn');
    updateBtn.click(function(event) {
        console.log('gg')
        postPrices()

    });
});

$( document ).ready(function() {
    console.log()
    waitForUserAuth(getPrices);
});