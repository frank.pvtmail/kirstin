    function getUnitSizes() {
        $.ajax(backendHostUrl + '/a/unit/sizes', {
            /* Set header for the XMLHttpRequest to get data from the web server
    associated with userIdToken */
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {

            console.log(data)



            data.sizes.forEach(function(size) {
                $('#prefix').append('<option>' + size + '</option>');
            });

        });
    }



    function addPods() {

        t = $('#total').val();
        prefix = $('#prefix').val();


        /* Send note data to backend, storing in database with existing data
  associated with userIdToken */
        $.ajax(backendHostUrl + '/api/v1/admins/units', {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            },
            method: 'POST',
            data: JSON.stringify({
                't': t,
                'prefix': prefix
            }),
            contentType: 'application/json'
        }).then(function(data) {
            console.log(data);
           displayServerMessage(data)


        });
    }









    function initDates() {
        var now = new Date();

        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);

        var today = now.getFullYear() + "-" + (month) + "-" + (day);

        $('#start_date').val(today);
        $('#end_date').val(today);


    }
















    function getUnits(filter, cursor, query) {

        $.ajax(backendHostUrl + '/admin_units?filter=' + filter + '&p=' + cursor + '&query=' + query + '&sd=' + $('#start_date').val() + '&ed=' + $('#end_date').val(), {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {
            localStorage.setItem("cursor", data.next_page);
            console.log(localStorage.getItem("cursor"))
            console.log(data);
            if (localStorage.getItem("cursor") == 'undefined') {
                $("#nextpage").prop('disabled', true)
            }
            data.units.forEach(function(unit) {
                console.log(unit.friendly_id)
                link = "#"
                if (unit.owner_name != 'No owner') {
                    link = "/user/" + unit.owner_id
                }
                units_table.row.add([

                    '<a href="' + link + '">' + unit.owner_name + '</a>',

                    '<a href="/unit/' + unit.id + '">' + unit.unit_name + '</a>',
                    unit.dropoff_date,
                    unit.pickup_date,

                    unit.status
                ]).draw(false);
            });
            // Iterate over user data to display user's notes from database.
        });
    }


    function searchUnits(query) {

        $.ajax(backendHostUrl + '/api/v1/admins/units?search=' + query, {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {

            data.units.forEach(function(unit) {

                link = "#"
                if (unit.owner_name != 'No owner') {
                    link = "/user/" + unit.owner_id
                }

                units_table.row.add([

                    '<a href="' + link + '">' + unit.owner_name + '</a>',

                    '<a href="/unit/' + unit.id + '">' + unit.unit_name + '</a>',
                    unit.dropoff_date,
                    unit.pickup_date,

                    unit.status
                ]).draw(false);
            });
            // Iterate over user data to display user's notes from database.
        });
    }





    function searchUnitsByDates() {

        $.ajax(backendHostUrl + '/a/units/search_dates_two?ed=' + $('#end_date').val() + '&sd=' + $('#start_date').val(), {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {

            console.log(data)
            displayTimeTable(data.pickups);




            data.units.forEach(function(unit) {

                link = "#"
                if (unit.owner_name != 'No owner') {
                    link = "/user/" + unit.owner_id
                }

                units_table.row.add([

                    '<a href="' + link + '">' + unit.owner_name + '</a>',

                    '<a href="/unit/' + unit.id + '">' + unit.unit_name + '</a>',
                    unit.dropoff_date,
                    unit.pickup_date,

                    unit.status
                ]).draw(false);

            });
            // Iterate over user data to display user's notes from database.
        });
    }








    function getUnitsByStatus(filter, cursor) {

        $.ajax(backendHostUrl + '/a/units?p=' + cursor, {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {
            localStorage.setItem("cursor", data.next_page);
            console.log(localStorage.getItem("cursor"))
            console.log(data);
            if (localStorage.getItem("cursor") == 'undefined') {
                $("#nextpage").prop('disabled', true)
            }
            data.units.forEach(function(unit) {
                console.log(unit.friendly_id)
                link = "#"
                if (unit.owner_name != 'No owner') {
                    link = "/users/" + unit.owner_id
                }
                units_table.row.add([

                    '<a href="' + link + '">' + unit.owner_name + '</a>',

                    '<a href="/units/' + unit.id + '">' + unit.unit_name + '</a>',
                    unit.dropoff_date,
                    unit.pickup_date,

                    unit.status
                ]).draw(false);
            });
            // Iterate over user data to display user's notes from database.

        });
    }

    $(function() {

       $("#add-pods").submit(function(event) {
            event.preventDefault();
            $('#server-msg-div').slideUp("slow");
            addPods();
        });

        var deleteBtn = $('#delete-btn');
        deleteBtn.click(function(event) {

            $.ajax(backendHostUrl + '/a/delete_pods', {
                headers: {
                    'Authorization': 'Bearer ' + userIdToken
                },
            }).then(function(data) {
                console.log(data);

            });
        });

        var loadmoreBtn = $('#load_more');
        loadmoreBtn.click(function(event) {
        if (localStorage.getItem("mode") == 'outstanding_charges') {
            usersWithUnpaidCharges();
        }  else {
            searchUsers($('#name').val())
        }

        });

        var searchBtn = $('#search-btn');
        searchBtn.click(function(event) {
            units_table.clear().draw();
            searchUnits($('#name').val())

        });

        var searchDatesBtn = $('#search-dates-btn');
        searchDatesBtn.click(function(event) {
            units_table.clear().draw();
            searchUnitsByDates()

        });

        var thisWeekBtn = $('#this-week-btn');
        thisWeekBtn.click(function(event) {
            units_table.clear().draw();
            thisWeek()

        });



    });


$( document ).ready(function() {
    console.log()
    initDates();
    waitForUserAuth(getUnitSizes);
});



