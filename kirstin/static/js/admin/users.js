
    function updateTable(data) {
        console.log(data.next_page)
        if (data.next_page == null || data.next_page == 'None' || data.users.length == 0) {
            $("#load_more").attr('disabled', true)
        }
        data.users.forEach(function(user) {
            users_table.row.add([
                '<a href=/users/' + user.user_id + ' >View</a>',
                user.name,
                user.units,
                user.cell_no,
                user.email,
                user.friendly_id,
                user.address,
                'R ' + user.outstanding_charges,
               // 'R ' + user.test,
                user.last_seen
            ]).draw(false);
            
        });



        $(".loader").hide()
    }

    function clearTableStoreMode(mode){
            $(".loader").show()
            $("#load_more").attr('disabled', false)
            users_table.clear().draw();
            localStorage.setItem("mode", mode);
            localStorage.setItem("cursor", 'None');

    }


    function callServer(urlparams = 'recent=1') {

        $.ajax(backendHostUrl + '/api/v1/admins/users?'+urlparams +'&p=' + localStorage.getItem("cursor"), {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {
            console.log(data);
            localStorage.setItem("cursor", data.next_page);
            updateTable(data);
            // Iterate over user data to display user's notes from database.
        });
    }



    //Button handlers
  $(function() {

        var searchBtn = $('#search-btn');
        searchBtn.click(function(event) {
            clearTableStoreMode('search')
            callServer('search='+$('#name').val())
            //searchUsers()
        });
        var outstandingBtn = $('#outstanding');
        outstandingBtn.click(function(event) {
            clearTableStoreMode('outstanding_charges')
            callServer('unpaid=1')
            //usersWithUnpaidCharges();
        });

        var allbtn = $('#all');
        allbtn.click(function(event) {
            clearTableStoreMode('recent')
            callServer('recent=1')
        });

        var loadmoreBtn = $('#load_more');
        loadmoreBtn.click(function(event) {
            $(".loader").show()
            if (localStorage.getItem("mode") == 'outstanding_charges') {
                callServer('unpaid=1')
            }  
            if (localStorage.getItem("mode") == 'recent') {
                callServer('recent=1')
            }
            if (localStorage.getItem("mode") == 'search') {
                callServer('search=1')
            }
        });


    });

    $( document ).ready(function() {
        localStorage.setItem("mode", 'recent')
        localStorage.setItem("cursor", 'None');

        waitForUserAuth(callServer);
    });