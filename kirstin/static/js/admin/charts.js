  function getStats() {











    $.ajax(backendHostUrl + '/api/v1/admins/stats', {
    /* Set header for the XMLHttpRequest to get data from the web server
    associated with userIdToken */
    headers: {
      'Authorization': 'Bearer ' + userIdToken
    }
  }).then(function(data){

      //drawChart(data.occupancy)
        console.log(data)
      $("#vacant").text(data.occupancy[1])
      $("#occupied").text(data.occupancy[0])

      $("#xxs").text(data.sizes[0])
      $("#xs").text(data.sizes[1])
      $("#s").text(data.sizes[2])
      $("#m").text(data.sizes[3])

      $("#mp_count_occ").text(data.unit_counts[0])
      $("#sp_count_occ").text(data.unit_counts[1])
      $("#lp_count_occ").text(data.unit_counts[2])
      $("#jp_count_occ").text(data.unit_counts[3])

      $("#mp_count").text(data.unit_totals[0])
      $("#sp_count").text(data.unit_totals[1])
      $("#lp_count").text(data.unit_totals[2])
      $("#jp_count").text(data.unit_totals[3])
  
      $("#logged_out_pods").text(data.logged_out)
      $("#logged_in_pods").text(data.logged_in)



     
      $("#total").text(data.total)
      drawChart2(data.occupancy)
         drawChart3(data.sizes)

   
    
    
  });
}
  function drawChart2(dataIn){

var canvas = document.getElementById("can");
var ctx = canvas.getContext("2d");
var lastend = 0;
var data = dataIn;
var myTotal = 0;
var myColor = ['red','green'];

for(var e = 0; e < data.length; e++)
{
  myTotal += data[e];
}

for (var i = 0; i < data.length; i++) {
ctx.fillStyle = myColor[i];
ctx.beginPath();
ctx.moveTo(canvas.width/2,canvas.height/2);
ctx.arc(canvas.width/2,canvas.height/2,canvas.height/2,lastend,lastend+(Math.PI*2*(data[i]/myTotal)),false);
ctx.lineTo(canvas.width/2,canvas.height/2);
ctx.fill();
lastend += Math.PI*2*(data[i]/myTotal);
}


  }


    function drawChart3(dataIn){

var canvas = document.getElementById("sizes");
var ctx = canvas.getContext("2d");
var lastend = 0;
var data = dataIn;
var myTotal = 0;
var myColor = ['red','green', 'blue', 'yellow', 'orange', 'purple'];

for(var e = 0; e < data.length; e++)
{
  myTotal += data[e];
}

for (var i = 0; i < data.length; i++) {
ctx.fillStyle = myColor[i];
ctx.beginPath();
ctx.moveTo(canvas.width/2,canvas.height/2);
ctx.arc(canvas.width/2,canvas.height/2,canvas.height/2,lastend,lastend+(Math.PI*2*(data[i]/myTotal)),false);
ctx.lineTo(canvas.width/2,canvas.height/2);
ctx.fill();
lastend += Math.PI*2*(data[i]/myTotal);
}


  }









    $( document ).ready(function() {

      waitForAuth(); 
      getStats(); 

    });


  function waitForAuth() {
    if (is_auth == false) {//we want it to match
      setTimeout(waitForAuth, 50);//wait 50 millisecnds then recheck
      return;
    }

    //getUnits('all', 'undefined');
    console.log("success!")
  }

