

$( document ).ready(function() {



    



        function finalizeOrder(){

                $.ajax(backendHostUrl + '/api/v1/current_users/orders', {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        },
        method: 'POST',
        data: JSON.stringify({
                            'address':  $("#delivery_address").val() , 
                              'cell_no': $("#cell_no").val(),
                              'pack_date': $('#pack_date').val(),
                              'pack_time': $("input[name='pack_time']:checked").val(),
                              'existing_delivery_id': $("input[name='existing_delivery']:checked").val(),
                            
                               }),
        contentType : 'application/json'
    }).then(function(data){
        console.log(data);
        displayServerMessage(data)
        if(data.success == true){
            $('#order_div').slideUp()
            
                window.location.replace('order-complete')
        } else {

                $("#checkout_btn").prop('disabled', false)



        }

    });



        }


        $('.count_update_btn').click(function(event) {
            console.log(this.id)
            console.log('GG')

     });


    var createOrderBtn = $('#checkout_btn');
    // createOrderBtn.click(function(event) {
    //     createOrderBtn.hide();
    //     $.ajax(backendHostUrl + '/api/v1/current_users/orders', {
    //         headers: {
    //             'Authorization': 'Bearer ' + userIdToken
    //         },
    //         type: 'GET',
    //     }).then(function(data){
    //         console.log(data);
    //         displayServerMessage(data)
    //         if (data.success) {
    //             createOrderBtn.hide();
    //         }
    //     });
    // });


createOrderBtn.click(function(event) {

    $("#checkout_btn").prop('disabled', true)


                finalizeOrder()





     });



    var tomoz =  new Date(new Date().getTime() + 24 * 60 * 60 * 1000);


    $('#pack_date').datetimepicker({minDate:tomoz,viewMode: 'months',
                                    format: 'DD/MM/YYYY',
                                    daysOfWeekDisabled: [0, 6]})

    $("select").imagepicker()
    $("#checkout_btn").hide()

    $('#server-msg-div').slideUp("slow");
    $('.center-block').slideUp("slow");

    waitForUserAuth(initShoppingCart)

});






function initShoppingCart(){
    updateCart()
    

}


function populateTable(data){
  shopping_cart_table.clear().draw();
        if (data.total == 0) {
            console.log('Showing checkout button')
            $("#checkout_btn").hide()

        } else {
           $("#checkout_btn").show()

        }

        data.items.forEach(function(item){
            shopping_cart_table.row.add([
                item.name,
                'R'  + item.price,
                 '<input min="0" type="number" id="'+item.id+'_count" value="'+item.count+'"><button class="btn btn-info" onclick=updateItemCount('+item.id+')>Update</button>',
                '<button class="btn btn-danger" onclick=removeFromCart('+item.id+') >Remove</button>'
            ]).draw(false);
        });
        $('.loader').hide()

        $("#shopping_cart_total").text(data.total)

}

function updateCart() {

    
    $('.loader').show()
    $.ajax(backendHostUrl + '/api/v1/current_users/shopping-carts/items', {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){
        console.log('Update cart result' + data)
        populateTable(data)


        

        // Iterate over user data to display user's notes from database.
    });
}


function checkout() {

    $.ajax(backendHostUrl + '/u/checkout', {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){
        console.log(data);

    });
}



function updateItemCount(item_id) {
            $('.loader').show()
            console.log(item_id)
            var new_count = $("#"+item_id+"_count").val()
            if (new_count < 0) {
                displayClientSideMessage('error', "Cannot insert a count smaller than 0")
                   $('.loader').hide()
            } else {


           


    $.ajax(backendHostUrl + '/api/v1/current_users/shopping-carts/items/'+item_id+'/counts/'+ new_count, {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){
        console.log(data);
        if (data.success){
            checkCart()
            updateCart();
        } else {
            displayServerMessage(data)
        }
                
    });

     }
}


function removeFromCart(item_id) {
          $('.loader').show()


    $.ajax(backendHostUrl + '/api/v1/current_users/shopping-carts/items/'+item_id, {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        },
        type: 'DELETE',
    }).then(function(data){
        console.log(data);
       // populateTable(data)

        checkCart()
        updateCart();

    });
}













