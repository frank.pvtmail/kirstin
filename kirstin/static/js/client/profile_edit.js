

    $(document).ready(function() {
        $('#form_section').hide()
       
        waitForUserAuth(fetchUserInfo)
    });

    function fetchUserInfo() {
        $.ajax(backendHostUrl + '/api/v1/current_users', {
            /* Set header for the XMLHttpRequest to get data from the web server
              associated with userIdToken */
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {
            console.log(data);

            $('#name').val(data.name);
            $('#cell_no').val(data.cell_no);
            $('#email').val(data.email);
            $('#address').val(data.address);
            $('#vat_number').val(data.vat_number);
            $('#business_name').val(data.business_name);

            $('.loader').hide()
            $('#form_section').slideDown()

        });
    }

    $(function() {

        var saveInfo = $('#save-info');
        saveInfo.click(function(event) {
            $('#form_section').hide()
            $('.loader').show()
            
            event.preventDefault();
            var name = $('#name').val();
            var email = $('#email').val();
            var cell_no = $('#cell_no').val();
            var address = $('.address').val();
            var business_name = $('#business_name').val();
            var vat_number = $('#vat_number').val();

            $.ajax(backendHostUrl + '/api/v1/current_users', {
                headers: {
                    'Authorization': 'Bearer ' + userIdToken
                },
                method: 'PUT',
                data: JSON.stringify({
                    'name': name,
                    'email': email,
                    'cell_no': cell_no,
                    'address': address,
                    'business_name': business_name,
                    'vat_number': vat_number
                }),
                contentType: 'application/json'
            }).then(function(data) {

                fetchUserInfo();

                displayServerMessage(data)

            });
        });
    });
