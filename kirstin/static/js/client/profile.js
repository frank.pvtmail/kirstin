   
var myModule = {
 
  myProperty: "someValue",
 
  // object literals can contain properties and methods.
  // e.g we can define a further object for module configuration:
  myConfig: {
    useCaching: true,
    language: "en"
  },
 
  // a very basic method
  saySomething: function () {
    console.log( "Where in the world is Paul Irish today?" );
  },
 
  // output a value based on the current configuration
  reportMyConfig: function () {
    console.log( "Caching is: " + ( this.myConfig.useCaching ? "enabled" : "disabled") );
  },
 
  // override the current configuration
  updateMyConfig: function( newConfig ) {
 
    if ( typeof newConfig === "object" ) {
      this.myConfig = newConfig;
      console.log( this.myConfig.language );
    }
  }
};
 
// Outputs: Where in the world is Paul Irish today?
myModule.saySomething();
 
// Outputs: Caching is: enabled
myModule.reportMyConfig();
 
// Outputs: fr
myModule.updateMyConfig({
  language: "fr",
  useCaching: false
});
 
// Outputs: Caching is: disabled
myModule.reportMyConfig();




    $(document).ready(function() {
        waitForAuth();

        $('#server-msg-div').slideUp("slow");
        $('.center-block').slideUp("slow");

    });

    function waitForAuth() {
        if (is_auth == false) { //we want it to match
            console.log("fail")
            setTimeout(waitForAuth, 50); //wait 50 millisecnds then recheck
            return;
        }

        fetchUserInfo();
        console.log("success")

    }

    function fetchUserInfo() {
        $.ajax(backendHostUrl + '/api/v1/current_users', {
            /* Set header for the XMLHttpRequest to get data from the web server
                associated with userIdToken */
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data) {
            $('.center-block').slideDown("slow");
            $('.loader').hide()
            console.log(data);
            localStorage.setItem("user_id", data.id);
            $('#name').text(data.name);
            $('#cell_no').text(data.cell_no);
            $('#email').text(data.email);
            $('#address').text(data.address);
            $('#vat_number').text(data.vat_number);
            $('#business_name').text(data.business_name);

        });
    }

    $(function() {
        $("#pod_form").submit(function(event) {
            $('#add-pod-btn').prop("disabled", true);
            event.preventDefault();
            $('#server-msg-div').slideUp("slow");
            postPOD();
        });

    });
