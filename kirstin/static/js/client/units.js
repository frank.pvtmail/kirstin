

function fetchUnits() {
    $.ajax(backendHostUrl + '/api/v1/current_users/units', {
        /* Set header for the XMLHttpRequest to get data from the web server
      associated with userIdToken */
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){
        $('.loader').hide()
        $('#my_units_div').slideDown()
        if (data.status == "failure"){

        } else{

            data.units.forEach(function(unit){

                console.log(unit.image_url)
                var image = '<img src="https://storage.cloud.google.com'+ unit.image_url+'" class="img-rounded" alt="">'
                if (unit.image_url == null) {
                    image = ''
                }

                var color = 'black'
                if(unit.outstanding_charges > 0){
                    color = 'red'
                }


             

                    if (unit.has_delivery == true) {


                        $('#my_units_div').prepend(
                            '<div class="unit_div">\
<div class="unit-div-content">\
<div>\
<label>Name:</label> '+unit.message+' \
</div>\
<div>\
<label>Code:</label> '+ unit.unit_name+ '\
</div>\
<div>\
<label>Dropoff:</label> '+ unit.dropoff_date+ '\
</div>\
<div>\
<label>Pickup:</label> '+ unit.pickup_date+ '\
</div>\
<div>\
<label>Status:</label> '+unit.status+' \
</div>\
<div>\
<label style="color:'+color+';">Outstanding charges:</label> R'+unit.outstanding_charges+' \
</div>\
</div>\
<div class="row">\
<a class="btn btn-info" href="/units/'+unit.unit_id+'">Manage</a> \
<a class="btn btn-warning" href="/cancel_rent/'+unit.unit_id+'">End/Cancel rent</a>\
</div> \
</div>')

                    } else {
                        $('#my_units_div').prepend('<div class="unit_div"> \
<div>\
<label>Name:</label> '+unit.message+' \
</div>\
<div>\
<label>Code:</label> '+ unit.unit_name+ '\
</div>\
<div>\
<div>\
<label>Client visit:</label> '+ unit.visit_date+ '\
</div>\
<label>Status:</label> '+unit.status+' \
</div>\
<div>\
<label style="color:'+color+';">Outstanding charges:</label> R'+unit.outstanding_charges+' \
</div>\
<div class="row"> <a class="btn btn-info" href="/units/'+unit.unit_id+'">Manage</a> \
<a class="btn btn-warning" href="/cancel_rent/'+unit.unit_id+'">End/Cancel rent</a></div> \
<div class="row"><br>\
<a class="btn btn-info" href="/deliveries/'+unit.unit_id+'">Schedule delivery</a> \
</div>\
</div>')




                    }



    




            });



        }
    });
}


function runTasks() {

    fetchUnits();


}

$( document ).ready(function() {
    waitForUserAuth(runTasks)

});



