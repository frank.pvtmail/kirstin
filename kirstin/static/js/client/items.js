
function viewItem(urls, text, name){
    $("#item-description").text(text)
    $(".modal-title").text(name)
    console.log(urls)
    $('.carousel-inner').empty()
    console.log('looping')

    var i = 0
    urls.forEach(function(item){
        console.log(item)


        if (i == 0) {
            $(".carousel-inner").append('<div class="item active">\
<img src="https://storage.googleapis.com' + item + '" style="width:100%;height:300px;">\
</div>')

        } else {
            $(".carousel-inner").append('<div class="item">\
<img src="https://storage.googleapis.com' + item + '" style="width:100%;height:300px;">\
</div>')
        }

        console.log('added image')

        i +=  1
    });


}


function populateItemTable(data){
    console.log(data);
    $("#item_header").text(data.item_type);
    data.items.forEach(function(item){


        my_webstore_table.row.add([
            '<img style="width:200px;" src="https://storage.googleapis.com' + item.image_url + '" class="img-thumbnail" alt="">',
            // '<button data-toggle="modal" data-target="#myModal" class="btn btn-primary" onclick=viewItem("'+item.image_url+'")>View</button>',
            item.name,
            item.ideal_for,
            item.description,
            'R ' + item.price,
            item.quantity + ' Available',
            '<button class="btn btn-primary" onclick=addToCart('+item.id+') >Add to cart</button>'

        ]).draw(false);
       
    });


}

function getItemPerCategory(item_type) {
    my_webstore_table.clear().draw();

    $.ajax(backendHostUrl + '/api/v1/current_users/items?item_category='+item_type, {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){

        populateItemTable(data)

    });
} 

$(function(){


    $('.item-btn').on('click',function(event) {
        getItemPerCategory($(this).attr('id'))
        console.log($(this).attr('id'));
    });

});


function getAllItems() {
    my_webstore_table.clear().draw();

    $.ajax(backendHostUrl + '/api/v1/current_users/items', {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){



        populateItemTable(data)

        // Iterate over user data to display user's notes from database.
    });

}


$( document ).ready(function() {
    $('.carousel').carousel()

    waitForUserAuth(getAllItems)



});




function addToCart(item_id) {

    $.ajax(backendHostUrl + '/api/v1/current_users/shopping-carts/items/'+item_id, {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        },
        type: 'POST',
    }).then(function(data){
        console.log(data);
        if (data.success == false) {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        }
        checkCart();
        displayServerMessage(data)

    });
} 




