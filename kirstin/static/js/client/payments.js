    $(function(){


        $( document ).ready(function() {
          
            waitForAuth(); 
        });

        function waitForAuth() {
            if(is_auth == false) {//we want it to match
                setTimeout(waitForAuth, 50);//wait 50 millisecnds then recheck
                return;
            }
           
        }
    });





    $(function() {


        $('#my_payments_table tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                my_payments_table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );


        $(document).on('click', '#pay_btn', function () {
            $(this).hide()
            // your function here
            console.log($("#outstanding_amount").text())
            var amount = $("#outstanding_amount").text()
            var merch_ref = $("#merch_ref").text()
            prePareCheckout(amount, merch_ref); 
        });

        $(document).on('click', '#eft_btn', function () {
            createEFTButton();
            // your function here

        });



    });

    function prePareCheckout(amount, merch_ref) {
        $.ajax(backendHostUrl + '/api/v1/current_users/payments?a='+amount + '&merch_ref=' + merch_ref, {
            /* Set header for the XMLHttpRequest to get data from the web server
          associated with userIdToken */
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            }
        }).then(function(data){

            console.log(data)
            var JSLink = "https://oppwa.com/v1/paymentWidgets.js?checkoutId="+data.id
            var JSElement = document.createElement('script');
            JSElement.src = JSLink;

            document.getElementsByTagName('head')[0].appendChild(JSElement);
            var scriptCode = "<script src=https://oppwa.com/v1/paymentWidgets.js?checkoutId="+data.payment.id + "/>";
            //str.replace(/MICROSOFT/i, "W3Schools");
            $('#payment-form').append(scriptCode);

            var getUrl = window.location;
            var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

            console.log(baseUrl)
            console.log(getUrl)
            console.log( window.location.origin+'/outstanding-payments/'+data.user.id+'?merch_ref='+merch_ref)

            if (location.hostname === "localhost"){
                $('#payment-form').append('<form action="http://localhost:8082/outstanding-payments/'+data.user.id+'" class="paymentWidgets" data-brands="VISA MASTER AMEX"></form>');
            } else{
                $('#payment-form').append('<form action="'+ window.location.origin+'/outstanding-payments/'+data.user.id+'?merch_ref='+merch_ref+'" class="paymentWidgets" data-brands="VISA MASTER AMEX"></form>');
            }

        });
    }