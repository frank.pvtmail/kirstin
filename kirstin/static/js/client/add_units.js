
var monthly = true
var has_delivery = false




function disableDeliverySection(){
    has_delivery = false
    $(".delivery_field").each(function( index ) {
        $(this).prop( "disabled", true);
    });
}

function enableDeliverySection(){
    has_delivery = true
    $(".delivery_field").each(function( index ) {
        $(this).prop( "disabled", false);


    });
}



function initFormOptions() {
    $.ajax(backendHostUrl + '/api/v1/current_users/available-units', {

        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){

        $('#pod_sizes').children('option').remove();
        $("#delivery_address").val(data.address)
        $("#cell_no").val(data.cell_no)

        data.unit_information.forEach(function(size){
            if (size.available > 0){
                $('#pod_sizes').append('<option id='+size.size_text+ '>'+size.description+'</option>');
            } else {
                $('#pod_sizes').append('<option disabled id='+size.size_text+ '>'+size.description+' NOT AVAILABLE</option>');
            }
        });
        console.log(data)
    });
}



function runTasks() {
    //fetchUserInfo();
    $('.loader').hide()
    $("#datetimepicker4").prop('disabled', true)
    $("#end_date_div").hide()
    $( "#delivery_section" ).slideUp( "slow");

    initFormOptions();
    $("#price_div").slideUp()
    $("#sla_div").hide()

    $('#server-msg-div').slideUp("slow");

    disableDeliverySection();

}


function validateOrderSection(){
    var pod_size = $('#pod_sizes').find(":selected").attr('id')

    $("#insurance_options").css({"border-color": "rgb(204, 204, 204)"})
    $("#insurance_options").css({"box-shadow": "rgba(0, 0, 0, 0.075) 0px 1px 1px 0px inset"})
    $("#delivery_required").css({"border-color": "rgb(204, 204, 204)"})
    $("#delivery_required").css({"box-shadow": "rgba(0, 0, 0, 0.075) 0px 1px 1px 0px inset"})

 


                console.log(pod_size)
                if(pod_size == '') {
                    console.log('no pod size')
                    //showMessage('Error:', 'No pod size selected', 'alert-danger')
                    displayClientSideMessage('warning', "No pod size selected")
                    return false
                }


                    if ($("#insurance_options").val() == '') {
                    console.log("GG")
                    console.log($("#insurance_options").css("border-color"))
                    console.log($("#insurance_options").css("box-shadow"))

                    $("#insurance_options").css({"border-color": "blue", "box-shadow": "0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px blue"})
                    // displayClientSideMessage('warning', "Please specify whether you would like insurance or not")
                    ///showMessage('Error:', 'Please specify if you want to deliver your unit', 'alert-danger')
                    return false
                }

       

     

                if((pod_size == 'HC' || pod_size == 'C')  && $("#delivery_required").val().startsWith("Yes")) {
                    console.log('no pod size')
                    //showMessage('Error:', 'No pod size selected', 'alert-danger')
                    displayClientSideMessage('warning', "Deliveries not supported for containers")
                    return false
                }



                if ($("#delivery_required").val() == '') {
                    $("#delivery_required").css({"border-color": "blue", "box-shadow": "0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px blue"})

                    //displayClientSideMessage('warning', "Please specify whether you would like a delivery or not")
                    ///showMessage('Error:', 'Please specify if you want to deliver your unit', 'alert-danger')
                    return false
                }






    if (monthly == false && $('#datetimepicker4').val() == '') {
        displayClientSideMessage('warning', "Please select a date to end the rent")
        // showMessage('Error:', 'No end date selected', 'alert-danger')
        return false
    }

    return true
}




function getQuote() {
    $("#price_div").hide();
    $('.loader').show()
    $('#server-msg-div').slideUp("slow");
    var validInput = validateOrderSection()
    if  (validInput == false) {
        $('.loader').hide()
        return false
    }


    $.ajax(backendHostUrl + '/api/v1/current_users/units/quotes?monthly='+ monthly + '&size=' +$('#pod_sizes').find(":selected").attr('id') + '&end_date=' + $('#datetimepicker4').val(), {
        /* Set header for the XMLHttpRequest to get data from the web server
                              associated with userIdToken */
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        }
    }).then(function(data){
        console.log(data)
         $('.loader').hide()
        $("#price_div").slideDown()
        if(monthly){
            $("#price_details").text("(price for this month (pro rata)):")
        } else {
            $("#price_details").text("(pro rata price for this month (R "+ data.pro_rata_this_month +") and " + data.num_months + " x R"+ data.price_monthly+" for months thereafter):")


        }

        $("#price_today").val(data.price_today)
        $("#price_monthly").val(data.price_monthly)
        if (has_delivery){
            $("#delivery_price_div").slideDown()
            $("#delivery_price").val(data.delivery_price)
        } else {
            $("#delivery_price_div").hide()
            $("#delivery_price").val(data.delivery_price)

        }
    });

}




function finalizeOrder(){
    var pod_size = $('#pod_sizes').find(":selected").attr('id');
    var noteField = $('#note-content');
    var note = noteField.val();
    console.log(note)
    console.log(pod_size)

    /* Send note data to backend, storing in database with existing data
                associated with userIdToken */
    $.ajax(backendHostUrl + '/api/v1/current_users/units', {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        },
        method: 'POST',
        data: JSON.stringify({'address':  $("#delivery_address").val() , 
                              'has_delivery': has_delivery,
                              'cell_no': $("#cell_no").val(),
                              'pack_date': $('#pack_date').val(),
                              'pack_time': $("input[name='pack_time']:checked").val(),
                              'quoted_price':$('#price_today').val(),
                              'delivery_price': $('#delivery_price').val(),
                              'pickup_date':$('#pickup_date').val(),
                              'dropoff_date':$('#dropoff_date').val(),
                              'dropoff_time': $("input[name='dropoff_time']:checked").val(),
                              'pickup_time': $("input[name='pickup_time']:checked").val(),
                              'unit_type' : 0,'size' : pod_size ,'type_name' : "POD",
                              'message': note , 'monthly' : monthly,
                              'end_date' : $('#datetimepicker4').val(),
                              'existing_delivery': $("input[name='existing_delivery']:checked").val(),
                              'delivery_note': $("#delivery-note-content").val(),
                              'insurance_options': $('#insurance_options').find(":selected").attr('id') }),
        contentType : 'application/json'
    }).then(function(data){
        console.log(data);
        displayServerMessage(data)
        if(data.success == true){
            $('#order_div').slideUp()
            
            window.location.replace('order-complete')
        } else {
            $('#add-pod-btn').prop( "disabled", false);
            initFormOptions();
            getQuote();

        }

    });
}

    function acceptQuote() {
            finalizeOrder();
            $('#order_div').slideUp()
            $('#server-msg-div').slideUp("slow");

    }




$(function(){



    $( "#pod_form" ).submit(function( event ) {
        $('#add-pod-btn').prop( "disabled", true);
        $('#acceptQuoteBtn').prop( "disabled", false);
        event.preventDefault();
        var pod_size = $('#pod_sizes').find(":selected").attr('id')

        var validInput = validateOrderSection()
        if  (validInput == false) {
            return false
        }

        $.ajax(backendHostUrl + '/api/v1/current_users/units/validations', {
            headers: {
                'Authorization': 'Bearer ' + userIdToken
            },
            method: 'POST',
            data: JSON.stringify({'address':  $("#delivery_address").val() , 
                                  'pickup_date':$('#pickup_date').val(),
                                  'dropoff_date':$('#dropoff_date').val(),
                                  'cell_no': $("#cell_no").val(),
                                  'dropoff_time': $("input[name='dropoff_time']:checked").val(),
                                  'pickup_time': $("input[name='pickup_time']:checked").val(),
                                  'size' : pod_size ,
                                  'end_date' : $('#datetimepicker4').val(),
                                  'existing_delivery': $("input[name='existing_delivery']:checked").val(),
                                  'has_delivery': has_delivery,
                                 }),
            contentType : 'application/json'
        }).then(function(data){
            console.log("Data from data validation of info")
            console.log(data)

            if (data.success) {
                $("#cell_no").val(data.cell_no)
                if(has_delivery) {
                    $("#delivery_price").val(data.delivery_price)
                    $("#delivery_price_quoted").text(data.delivery_price)
                    $("#distance").text(data.distance)
                    $("#base_price").text(data.base_price)
                    $("#minimum_distance").text(data.min_distance)
                    $("#cost_per_km").text(data.per_km_cost)
                    $("#myModal").modal();

                } else {
                    acceptQuote()

                }

            } else {
                 $('#add-pod-btn').prop( "disabled", false);
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                displayServerMessage(data)
                initFormOptions();
               
            }

        });

    });



        $('#myModal').on('hidden.bs.modal', function () {
            // do something…
            console.log('hiding modal')
            $('#add-pod-btn').prop( "disabled", false);
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            initFormOptions();

        })


        $("#acceptQuoteBtn").click(function(){
            $('#acceptQuoteBtn').prop( "disabled", true);
            acceptQuote()
    
        });

        $("#rejectQuoteBtn").click(function(){
              $('#add-pod-btn').prop( "disabled", false);
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                initFormOptions();
    
        });


          $(".nav-tabs a").click(function(){
            console.log(this)
            $(this).tab('show');
          });

  
           $(".next_btns").click(function(){
                console.log("next btn")
                console.log(this.id)
                if (validateOrderSection(1)) {
                    var menuId = this.id.slice(0, this.id.indexOf("_"))
                    $('li').removeClass('active')
                    $('#' + menuId + '_li').addClass('active')
                    $('<a href=#'+menuId+'>MenuItem</a>').tab('show');
                }
           
            
          });



    $('input:radio[name="payment_options"]').change(
        function(){
            console.log($("input[name='drop_off_time']:checked").val())

            if (this.checked) {
                $("#price_div").slideDown()
                if (this.value == 'on' && this.id == 'pay_once') {
                    $("#end_date_div").slideDown()
                    $("#price_monthly_div").hide()
                    $("#price_today_div").show()
                    monthly = false
                    getQuote()
                    $("#datetimepicker4").prop('disabled', false)

                    console.log(this.id)
                    localStorage.setItem("payment_option", "onceoff" );

                } else {
                    monthly = true
                    getQuote()
                    $("#price_monthly_div").show()
                    $("#price_today_div").show()
                    $("#datetimepicker4").prop('disabled', true)
                    $("#end_date_div").slideUp()
                    localStorage.setItem("payment_option", "monthly" );
                }

            }
        });


    $(document).on('change', '#pod_sizes', function(e) {

        value = this.options[e.target.selectedIndex].text
   

        if (value.startsWith('HC') || value.startsWith('C')) {

            $( "#collection_section" ).slideDown( "slow");

            disableDeliverySection();
            $( "#delivery_section" ).slideUp( "slow");

            
        } else {
             $( "#collection_section" ).slideUp( "slow");

            enableDeliverySection();
            $( "#delivery_section" ).slideDown( "slow");
            
        }
    });

    $(document).on('change', '#delivery_required', function(e) {

        value = this.options[e.target.selectedIndex].text
    
        if (value.startsWith('Yes')) {
            $( "#collection_section" ).slideUp( "slow");

            enableDeliverySection();
            $( "#delivery_section" ).slideDown( "slow");
        } else {
            $( "#collection_section" ).slideDown( "slow");

            disableDeliverySection();
            $( "#delivery_section" ).slideUp( "slow");
        }
    });

    var tomoz =  new Date(new Date().getTime() + 24 * 60 * 60 * 1000);


    $('#dropoff_date').datetimepicker({minDate:tomoz,viewMode: 'months',
                                       format: 'DD/MM/YYYY',
                                       daysOfWeekDisabled: [0, 6]})


    $('#pack_date').datetimepicker({minDate:tomoz,viewMode: 'months',
                                    format: 'DD/MM/YYYY',
                                    daysOfWeekDisabled: [0, 6]})


    $('#pickup_date').datetimepicker({minDate:tomoz,viewMode: 'months',
                                      format: 'DD/MM/YYYY', daysOfWeekDisabled: [0, 6]})


    $('#dropoff_time').datetimepicker({ 
        stepping:30,
        format:'LT'
    })
    $('#pickup_time').datetimepicker({ 
        stepping:30,
        format:'LT'
    })


    $('#datetimepicker4').datetimepicker(
        {
            minDate:new Date(),
            viewMode: 'years',
            format: 'MM/YYYY',
            stepping: 30,
            showTodayButton: true,
        });

    $("#datetimepicker4").on("dp.change", function(e) {
        getQuote();
    });


});


$( document ).ready(function() {



    waitForUserAuth(runTasks)



});



