// Copyright 2016, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//podit-169717

var is_auth = false
var userIdToken = null;
//var backendHostUrl = 'https://podit-169717.appspot.com';
//https://backend-dot-[PROJECT_ID].appspot.com


   


 function checkCharges(data){

        var label = ''
        if(data.charges > 0) {

          label = '&nbsp;<span class="label label-danger">'+data.charges+'</span>'

        }
        $('#payments_link').append(label)


  }


function AjaxPOST(url, json_data, onsuccessCall){
     $.ajax(backendHostUrl + url, {
        headers: {
            'Authorization': 'Bearer ' + userIdToken
        },
        method: 'POST',
        data: JSON.stringify(json_data),
        contentType : 'application/json'
    }).then(function(data){
        console.log(data);
        displayServerMessage(data)

        if (data.charges){
           checkCharges(data)
        }
        if (data.success == true){
          onsuccessCall();
        }


    });

}

 function displayClientSideMessage(status, message, div_id=null) {
  var server_msg = $('#server-msg-div')
     $('#server_msg').text(message);
                if(status == 'success'){
                    $('.alert').addClass('alert-success')
                    $('.alert').removeClass('alert-danger')
                    $('.alert').removeClass('alert-warning')
                    $('#server_msg_header').text('Success!')
                } else if(status == 'warning'){ 
                  $('.alert').addClass('alert-warning')
                  $('.alert').removeClass('alert-success')
                  $('.alert').removeClass('alert-danger')

                    $('#server_msg_header').text('Info')

                } else {
                  $('.alert').addClass('alert-danger')
                  $('.alert').removeClass('alert-success')
                   $('.alert').removeClass('alert-warning')
                    $('#server_msg_header').text('Error!')
                }
                
                if (div_id) {
                  $("#"+ div_id).append(server_msg)

                }
                $('#server-msg-div').slideUp("slow");
                $('#server-msg-div').slideDown("slow");

 }

  function displayServerMessage(data) {
    
                    $('#server_msg').text(data.message);
                if(data.success == true){
                    $('.alert').addClass('alert-success')
                     $('.alert').removeClass('alert-warning')
                    $('.alert').removeClass('alert-danger')
                    $('#server_msg_header').text('Success!')
                } else {
                  $('.alert').addClass('alert-danger')
                   $('.alert').removeClass('alert-warning')
                  $('.alert').removeClass('alert-success')
                    $('#server_msg_header').text('Error!')
                }

                if(data.extra == true) {
                  console.log(data)
                  console.log(data.extra_content)
                
                  $('#server_msg').append(data.extra_content);
                }
                // Refresh notebook display.
                console.log(data)
                
                $('#server-msg-div').slideUp("slow");
                $('#server-msg-div').slideDown("slow");
  }

  function displayServerMessageFromFlash(message, category) {
    
                $('#server_msg').text(message);
                if(category != 'error'){
                    $('.alert').addClass('alert-success')
                    $('.alert').removeClass('alert-danger')
                    $('#server_msg_header').text('Success!')
                } else {
                    $('.alert').addClass('alert-danger')
                    $('.alert').removeClass('alert-success')
                    $('#server_msg_header').text('Error!')
                }
                // Refresh notebook display.
                               
                $('#server-msg-div').slideUp("slow");
                $('#server-msg-div').slideDown("slow");
  }







    function checkUser(){
    console.log('Check user client')
     //$("#server-msg-div").hide()


      $.ajax(backendHostUrl + '/api/v1/current_users', {
        headers: {
          'Authorization': 'Bearer ' + userIdToken
        },
        method: 'POST',
        data: JSON.stringify({'message': 'incoming user'}),
        contentType : 'application/json'
      }).then(function(data){
        
         loginUser(data.user_dict.id);

         checkCharges(data)

      


        if (data.profile_complete == false){
             Messenger().post({
                    message: 'Please complete profile',
                    type: 'error',
                    showCloseButton: true
                });

          if (window.location.href.indexOf("profile/edit") != -1){
              console.log('on payments page')
          } else {
              window.location.href = "/profile/edit"
          }

        }
          console.log('appending notification')
         
          $("#name_container").append('<span>'+data.user_dict.name+'</span>');
          $("#email_container").append('<span>'+data.user_dict.email+'</span>');
          $("#user_info").slideDown()
          checkCart();
          console.log(data)

      });


  }



