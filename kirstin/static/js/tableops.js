/*var t = $('#myitems_table').DataTable({
   responsive: true,
   "language": {
    "search": "Filter:"
},
"bPaginate": false,
"bLengthChange": false,
"bFilter": true,
"bInfo": false,
"bAutoWidth": false,
"createdRow": function ( row, data, index ) {
    if(data[1] != 'N/A'){

    var due_date = new Date(data[1]);
    due_date.setHours(due_date.getHours() + 2);
    var humanized_time  = showRemaining(due_date);
    if (humanized_time.endsWith("ago")) { 
        $('td', row).css('background-color', 'Pink');
    } else {
        var days = humanized_time.indexOf("days"); 
        var hours = humanized_time.indexOf("hrs");
        var mins = humanized_time.substring(humanized_time.indexOf("m") - 3, humanized_time.indexOf("m"))
        if (days == -1 && hours == -1 && parseInt(mins) <= 30) { 
            $('td', row).css('background-color', 'Bisque');
        }
    }
    }

},
"aoColumnDefs": [
{ "width": "1%", "targets": [-2] },
{ "width": "20%", "targets": [1] },
{ "width": "10%", "targets": [0] },
{ "width": "25%", "targets": [-3,-4] },
{ "type": "display", "targets": [-4,-3,-2,-1,1] },
{"className": "dt-center", "targets": "_all"},
{"mRender": function ( data, type, row ) {
    if(type == 'display') {
        if(data != 'N/A'){

        var due_date = new Date(data);
        due_date.setHours(due_date.getHours() + 2);
        var renderedData = showRemaining(due_date);
        var formatted = reformatDate(due_date);
        return  '<span data-toggle="tooltip" title="' + formatted + '">'+ formatted + '<br>'+ renderedData + '</span>';
        } else {
            return 'Not specified';
        }
    
    }
    return data;
},
"aTargets": [ 1 ]
},
{"mRender" : function ( data, type, row ) {
    if(type == 'display'){
        var statusicon = '';
        if (data == 'PENDING PICKUP') {
            statusicon = '<i title="'+data+'" class="a fa fa-car" style="color:green;"></i>';
        } else if (data == 'PENDING DROPOFF'){
            statusicon = '<i title="'+data+'" class="b fa fa-car" style="color:orange;"></i>';
        } else if (data == 'COMPLETE'){
            statusicon = '<i title="'+data+'" class="fa fa-car" style="color:blue;"></i>';
        } else {
            statusicon = '<i title="'+data+'" class="fa fa-car" style="color:red;"></i>';
        }
        return statusicon;
    }
    return  data;},
    "aTargets": [ -2 ]
    },
            {"mRender": function ( data, type, row ) {
                if(type == 'display'){
                    if(data.length > 30){
                        return '<span title="'+data+'">'+data.substr( 0,27 )+'...</span>' 
                    }
                }
                return data;
            },
            "aTargets": [ -3,-4 ]
        }
        ]
    });
*/


/*
function updateTable(filter, n, cursor, city){
    var nextpage = '';
    disableAllButtons();
    $.ajax({
        url:'/test?filter='+filter+'&n='+String(n)+'&p='+cursor+'&city='+city ,
        type: 'GET',
        success: function(response) {
            var deliveries = response.deliveries;
            nextpage = response.next_page;
            total = response.total;
            localStorage.setItem("cursor", nextpage);
            for (i = 0; i < deliveries.length; i++) { 
                if(deliveries[i].address_to == 'None'){
                    console.log('ommitting entry');
                     records_loaded += 1;  
                } else {
                    records_loaded += 1;    
                t.row.add([
                    deliveries[i].contact_name_from ,
                    deliveries[i].pickup_by,
                    '<a href="/drivers/' + deliveries[i].driver_ds_id + '">' + deliveries[i].driver_name + '</a>',
                    deliveries[i].address_from,
                    deliveries[i].address_to,
                    deliveries[i].status,
                    deliveries[i].ds_id
                    ]).draw(false);
                }
            }
            enableAllButtons();
            if (parseInt(total) < 70) {
                $('.loadall').prop('disabled', false);
            } else {
                $('.loadall').prop('disabled', true);
            }
            console.log(nextpage);
            if(nextpage == null){
                console.log('disable button');
                $('.nextpage').prop('disabled', true);
                $('.loadall').prop('disabled', true);
            }
        },
        error: function(error) {
            console.log(error);
        }
    });

}*/









