var is_auth = false;
var userIdToken = null;


var backendHostUrl = 'https://zarprojectx.appspot.com';
var clientAppUrl = 'https://'+ window.location.hostname;
//https://backend-dot-[PROJECT_ID].appspot.com

if (location.hostname === "localhost"){
    backendHostUrl = 'http://localhost:8081';
    clientAppUrl = 'http://localhost:8080';
}

    function loginUser(id){
      $.ajax(clientAppUrl + "/login/"+ id, {
      
    }).then(function(data){
      console.log(data)
      $("#menu_side").slideDown()
      $("#transactions_link").removeClass("isDisabled")
      $("#my_orders").removeClass("isDisabled")
      

        });
    }

    function logoutUser(){
      $.ajax(clientAppUrl + "/logout", {
      
    }).then(function(data){
      console.log(data)
      


          // Iterate over user data to display user's notes from database.
        });
    }


function waitForUserAuth(callOnAuth) {
    if (is_auth == false) {//we want it to match
      setTimeout(function (){
            waitForUserAuth(callOnAuth);
      }, 50);//wait 50 millisecnds then recheck
      return;
    }
    console.log('applying function')
    callOnAuth();

    console.log("success!")
  }

$(function(){

  var config = {
    apiKey: "AIzaSyC_JA0h1OgjQ8eV4S5hCcQntBPyckqr-A0",
    authDomain: "zarprojectx.firebaseapp.com",
    databaseURL: "https://zarprojectx.firebaseio.com",
    projectId: "zarprojectx",
    storageBucket: "zarprojectx.appspot.com",
    messagingSenderId: "8224685283"
  };

function resetPassword(emailAddress){
  var auth = firebase.auth();

  auth.sendPasswordResetEmail(emailAddress).then(function() {
  // Email sent.
}).catch(function(error) {
  // An error happened.
});

}


  function configureFirebaseLogin() {

    firebase.initializeApp(config);

    // [START onAuthStateChanged]
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        $('#logged-out').hide();
        var name = user.displayName;

        var welcomeName = name ? name : user.email;

        user.getToken().then(function(idToken) {
          userIdToken = idToken;


          checkUser();
          is_auth = true


        });
        $('#user').text(welcomeName);
        $('#logged-in').show();
        $("#logged_out_menu").hide()
        $("#logged_out_menu_button").hide()
        
        $("#login_button_div").hide()

    } else {
      $('#logged-in').hide();
      $("#logged_out_menu").show()
    $('#logged-out').show();
    $("#login_button_div").show()

  }

});

}


function configureFirebaseLoginWidget() {
  var uiConfig = {
    'signInOptions': [
      // Leave the lines as is for the providers you want to offer your users.
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
      //firebase.auth.GithubAuthProvider.PROVIDER_ID,
      //firebase.auth.PhoneAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    // Terms of service url
    'tosUrl': '/',
  };

  var ui = new firebaseui.auth.AuthUI(firebase.auth());
  ui.start('#firebaseui-auth-container', uiConfig);
}




var signOutBtn =$('#sign-out');
signOutBtn.click(function(event) {
  event.preventDefault();

  firebase.auth().signOut().then(function() {
     $("#login_button_div").show()
     logoutUser();
     window.location.href = "/"

    console.log("Sign out successful");
  }, function(error) {
    console.log(error);
  });
});


configureFirebaseLogin();
configureFirebaseLoginWidget();

});