# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START app]

# TODO implement comments like the one below from Google lib
"""Verifies an ID token and returns the decoded token.

Args:
    id_token (Union[str, bytes]): The encoded token.
    request (google.auth.transport.Request): The object used to make
        HTTP requests.
    audience (str): The audience that this token is intended for. If None
        then the audience is not verified.
    certs_url (str): The URL that specifies the certificates to use to
        verify the token. This URL should return JSON in the format of
        ``{'key id': 'x509 certificate'}``.

Returns:
    Mapping[str, Any]: The decoded token.
"""

import ast
import calendar
import datetime
import json
import logging
import uuid
from functools import wraps
import handlers.client_request_handler
import handlers.admin_request_handler

import flask_cors
import google.auth.transport.requests
import google.oauth2.id_token
import requests
import requests_toolbelt.adapters.appengine
from flask import Flask, jsonify, request
from google.appengine.api import search
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext import deferred
from google.appengine.ext import ndb
from validate_email import validate_email

import lib.cloudstorage as gcs
import utils.geo as geo
from models.address import Address
from models.charge import Charge, CHARGE_TYPE
from models.item import Item, ItemCategory
from models.order import Order
from models.shopping_cart import ShoppingCart
from models.system_settings import SystemSettings, CalendarEvent
from models.unit import Unit, STATUS_TO_TEXT, POD_SIZE_HUMANIZE, POD_SIZE, AT_FACILITY, PENDING_DROPOFF, CANCELLED
from models.user import User
from services.exporter import client as exporter
from services.sender import client as communicator

SYSTEM_SETTINGS = SystemSettings.fetch()

import utils.time_utils as time_utils

PROJECT_ID = 'podit-169717'
SID_MERCHANT = "TESTPODIT"
SID_SECRET_KEY = "ZVPCWNmJWfDqQGl7yhQKmQE5b2x0WSGy0qwqXsaAXA7GwGjlmnvn7tWIR"
SID_CURRENCY = "ZAR"
SID_COUNTRY = "ZA"

# Use the App Engine Requests adapter. This makes sure that Requests uses
# URLFetch.
requests_toolbelt.adapters.appengine.monkeypatch()
HTTP_REQUEST = google.auth.transport.requests.Request()

import google.oauth2.credentials

SCOPES = ['https://www.googleapis.com/auth/calendar']
API_SERVICE_NAME = 'calendar'
API_VERSION = 'v3'
CLIENT_SECRETS_FILE = "client_secret.json"

# authentication.userId = ff808081392eb9b201392f0b6d0200a3
# authentication.password = wCJFfx6F

# authentication.entityId = Please use one of the channel IDs below for this parameter depending on the request type (i.e. once-off or recurring)

# Once-off / 3DSecure Channel ID: ff808081407bfe10014087a5dcd918ea
# Recurring Channel ID: ff808081392eb9b201392f0bfe3800a9
from models.const import *

app = Flask(__name__)
app.secret_key = '38uhsdfuh838uhsdfuh388hdjh88282'
flask_cors.CORS(app)


def is_authorized(request):
    id_token = request.headers['Authorization'].split(' ').pop()
    claims = google.oauth2.id_token.verify_firebase_token(
        id_token, HTTP_REQUEST)
    return claims


UNIT_TYPES = {
    "POD": 0,
    "SS": 1
}

TYPES_HUMANIZE = {
    0: "POD",
    1: "SS"
}


def admin_authorized(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        id_token = request.headers['Authorization'].split(' ').pop()
        firebase_token = google.oauth2.id_token.verify_firebase_token(
            id_token, HTTP_REQUEST)
        current_user = User.query(User.firebase_id == firebase_token['sub']).get()
        session_token = {"firebase_token": firebase_token, "current_admin": current_user}
        if not firebase_token:
            return 'Unauthorized', 401

        if not current_user.is_admin:
            return 'Unauthorized', 401

        return f(session_token, *args, **kwargs)

    return decorated_function


def authorized(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        id_token = request.headers['Authorization'].split(' ').pop()
        firebase_token = google.oauth2.id_token.verify_firebase_token(
            id_token, HTTP_REQUEST)
        current_user = User.query(User.firebase_id == firebase_token['sub']).get()
        session_token = {"firebase_token": firebase_token, "current_user": current_user}
        if not firebase_token:
            return 'Unauthorized', 401

        return f(session_token, *args, **kwargs)

    return decorated_function


@app.route('/a/delete_pods', methods=['GET'])
def delete_pods():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401
    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    if u.is_admin == False:
        return 'Unauthorized', 401

    ndb.delete_multi(
        Unit.query().fetch(keys_only=True)
    )
    for x in range(Charge.query().count()):
        results = Charge.query().fetch(100, keys_only=True)
        ndb.delete_multi(results)

    return jsonify({'deleted pods': 'done'})






@app.route('/prepare_checkout', methods=['GET'])
def prepare_checkout():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    amount = request.args.get('a')
    amount = float(amount)
    amount = int(round(amount, 2))
    data = [
        ('authentication.userId', PEACH_USER_ID),
        ('authentication.password', PEACH_PASSWORD),
        ('authentication.entityId', INITIAL_CHANNEL_ID),
        ('amount', str(amount)),
        ('currency', 'ZAR'),
        ('createRegistration', 'true'),
        # 'recurringType' : 'INITIAL'
        ('recurringType', 'INITIAL'),
        ('paymentType', 'DB'),
        # ('createRegistration', 'true'),
    ]

    response = requests.post('https://test.oppwa.com/v1/checkouts', data=data)
    json_data = json.loads(response.text)
    print response
    return jsonify({"payment": ast.literal_eval(response.text), "user": u.to_dictionary()})


@app.route('/prepare_checkout_outstanding', methods=['GET'])
def prepare_checkout_outstanding():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    amount = request.args.get('a')
    amount = float(amount)
    amount = int(round(amount, 2))
    data = [
        ('authentication.userId', PEACH_USER_ID),
        ('authentication.password', PEACH_PASSWORD),
        ('authentication.entityId', INITIAL_CHANNEL_ID),
        ('amount', str(amount)),
        ('currency', 'ZAR'),
        ('paymentType', 'DB'),
        ('recurringType', 'INITIAL'),
        ('createRegistration', 'true')

    ]

    response = requests.post('https://test.oppwa.com/v1/checkouts', data=data)
    json_data = json.loads(response.text)
    print response
    return jsonify({"payment": ast.literal_eval(response.text), "user": u.to_dictionary()})


@app.route('/a/units', methods=['POST'])
def view_units_by_filter():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    data = request.get_json()
    print type(data['statuses'])
    print data['statuses']
    cursor = None
    if str(0) in data['statuses'] and str(1) in data['statuses'] and str(2) in data['statuses']:
        unitq, next_curs, more = Unit.query(ndb.OR(Unit.status == 0, Unit.status == 1, Unit.status == 2),
                                            Unit.available == True).order(-Unit.key).fetch_page(10, start_cursor=cursor)
        print '3'
    elif str(0) in data['statuses'] and str(1) in data['statuses']:
        print Unit.query(ndb.OR(Unit.status == 0, Unit.status == 1)).count()
        unitq, next_curs, more = Unit.query(ndb.OR(Unit.status == 0, Unit.status == 1)).order(-Unit.key).fetch_page(10,
                                                                                                                    start_cursor=cursor)
        print '2'
    elif str(0) in data['statuses']:
        unitq, next_curs, more = Unit.query(Unit.status == 0, Unit.available == True).order(-Unit.key).fetch_page(10,
                                                                                                                  start_cursor=cursor)
        print '1'
    else:
        unitq, next_curs, more = Unit.query(Unit.available == True).fetch_page(10, start_cursor=cursor)
    # else:
    if next_curs:
        next_curs = next_curs.urlsafe()
    else:
        next_curs = 'undefined'
    units = []
    print len(unitq)
    units = create_dict_from_unit_list(unitq, units)
    print units
    return jsonify({'units': units, 'next_page': next_curs})


payment_template = {
    "unit_name": "Unit name",
    "amount": "Amount",
    "created": "Date Created",
    "owner_name": "Owner name"
}


@app.route('/export', methods=['GET'])
def export():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401
    # TODO: add 2nd cursor for the two date queries
    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    if u.is_admin == False:
        return 'Unauthorized', 401
    sd = request.args.get('sd')
    ed = request.args.get('ed')
    deferred.defer(exporter.export_payments, sd, ed, u.email)

    return jsonify({'success': 'gg'})


@app.route('/admin_units', methods=['GET'])
def list_units():
    filter_name = request.args.get('filter')
    cursor = request.args.get('p')
    query = request.args.get('query')
    sd = request.args.get('sd')
    ed = request.args.get('ed')
    print sd, ed
    print query
    if cursor == 'undefined':
        cursor = None
    else:
        cursor = Cursor(urlsafe=cursor)
    # TODO: add 2nd cursor for the two date queries
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401
    # TODO: add 2nd cursor for the two date queries
    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    if u.is_admin == False:
        return 'Unauthorized', 401
    unitq = None
    next_curs = None
    more = None
    # TODO: add 2nd cursor for the two date queries
    start_date = datetime.datetime.strptime(sd, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(ed, "%Y-%m-%d") + datetime.timedelta(days=1)
    dateq = Unit.query(ndb.AND(Unit.pickup_date >= start_date), Unit.pickup_date <= end_date).order(-Unit.pickup_date,
                                                                                                    Unit.key)
    date_do = Unit.query(ndb.AND(Unit.dropoff_date >= start_date, Unit.dropoff_date <= end_date)).order(
        -Unit.dropoff_date, Unit.key)
    # dateq = dateq.filter(ndb.AND(Unit.dropoff_date >= start_date,Unit.dropoff_date < end_date)).order(-Unit.dropoff_date, Unit.key)
    unitq_do = None
    if filter_name == 'all' and query == '':
        # TODO: add 2nd cursor for the two date queries
        unitq, next_curs, more = dateq.fetch_page(10, start_cursor=cursor)
        unitq_do, next_curs, more = date_do.fetch_page(10, start_cursor=cursor)
    elif filter_name == 'available' and query == '':
        unitq, next_curs, more = Unit.query(Unit.available == True).fetch_page(10, start_cursor=cursor)
    elif filter_name == 'occupied' and query == '':
        unitq, next_curs, more = Unit.query(Unit.available == False).fetch_page(10, start_cursor=cursor)
    else:
        print 'search with query' + query
        size_filter = 0
        if len(query) <= 3:
            return jsonify({'units': [], 'next_page': 'undefined', 'status': 'query to short'})
        if str(query).lower().find('xs') != -1:
            size_filter = 0
        elif str(query).lower().find('s') != -1:
            size_filter = 1
        elif str(query).lower().find('m') != -1:
            size_filter = 2
        elif str(query).lower().find('l') != -1:
            size_filter = 3

        matches = []
        prev_curs = None
        while True:
            unitq, next_curs, more = Unit.query(Unit.unit_size == size_filter).fetch_page(200, start_cursor=prev_curs)
            prev_curs = next_curs
            matches, found = search_list_units(query, unitq, ['unit_name'], matches)
            if next_curs == None or found == True:
                unitq = matches
                break

    units = []
    units = create_dict_from_unit_list(unitq, units)
    if unitq_do:
        units = create_dict_from_unit_list(unitq_do, units)

    units = [i for n, i in enumerate(units) if i not in units[n + 1:]]

    # deliveries = [x for x in deliveries if x.address_to]
    if next_curs:
        next_curs = next_curs.urlsafe()
    else:
        next_curs = 'undefined'
    return jsonify({'units': units, 'next_page': next_curs})

    # firebase_id = ndb.StringProperty()
    # name = ndb.StringProperty()
    # cell_no = ndb.StringProperty()
    # email = ndb.StringProperty()
    # created = ndb.datetime.datetimeProperty(auto_now_add=True)
    # address = ndb.StringProperty()
    # is_admin = ndb.BooleanProperty()
    # braintree_id = ndb.StringProperty()


def create_dict_from_unit_list(unit_list, dump_list):
    for unit in unit_list:
        owner_id = None
        owner_name = 'No owner'
        if unit.owner_key:
            owner_id = unit.owner_key.id()
            owner_name = User.get_by_id(owner_id).name
        else:
            owner_id = '#'
        dump_list.append({
            'id': unit.key.id(),
            'owner_name': owner_name,
            'message': unit.message,
            'created': unit.created,
            'type_name': unit.type_name,
            'size': POD_SIZE_HUMANIZE[unit.unit_size],
            'unit_name': unit.unit_name,
            'pickup_date': unit.pickup_date,
            'dropoff_date': unit.dropoff_date,
            'status': STATUS_TO_TEXT[unit.status],
            'owner_id': owner_id

        })
    return dump_list


def search_list_units(needle, haystack, props, matches):
    found = False
    for item in haystack:
        for prop in props:
            if str(getattr(item, prop)).lower().find(needle.lower()) != -1:
                matches.append(item)
                found = True
                break
        if found:
            break

    return matches, found


def search_list(needle, haystack, props, matches):
    for item in haystack:
        for prop in props:
            if str(getattr(item, prop)).lower().find(needle.lower()) != -1:
                matches.append(item)
                break

    return matches


@app.route('/api/v1/admins/stats')
def stats():
    return handlers.admin_request_handler.fetch_stats()


@app.route('/api/v1/admins/units', methods=['GET', 'POST'])
@admin_authorized
def admin_unit_handling(session_token):
    if request.method == 'GET':
        search_term = request.args.get('search', None)

        if search_term:
            return handlers.admin_request_handler.unit_search(search_term)
    elif request.method == 'POST':
        data = request.get_json()
        return handlers.admin_request_handler.add_units(data)


@app.route('/api/v1/admins/users')
@admin_authorized
def admin_user_handling(session_token):
    search_term = request.args.get('search', None)
    user_id = request.args.get('user_id', None)
    unpaid = request.args.get('unpaid', None)
    recently_online = request.args.get('recent', None)

    if recently_online:
        return handlers.admin_request_handler.recently_online_users()

    if unpaid:
        return handlers.admin_request_handler.unpaid_users()

    if user_id:
        return handlers.admin_request_handler.fetch_user_info(session_token)

    if search_term:
        return handlers.admin_request_handler.user_search(search_term)


@app.route('/api/v1/admins/units/prices', methods=['GET', 'POST'])
@admin_authorized
def admin_unit_prices(session_token):

    if request.method == 'POST':
        return handlers.admin_request_handler.set_unit_prices(session_token)
    else:
        return handlers.admin_request_handler.fetch_unit_prices(session_token)


@app.route('/a/units/search_dates', methods=['GET', 'POST'])
def units_search_dates():
    # this is POST
    index = Unit.get_class_search_index()
    cursor = search.Cursor()
    all_units = []  # list of dicts, containing info to be printed

    # Build the sort options, sort by firstname, then lastname

    sd = request.args.get('sd')
    ed = request.args.get('ed')

    date_list = get_dates_between_two(sd, ed)
    all_units = []
    print date_list

    for date in date_list:
        results = index.search(query=search.Query(date,
                                                  options=search.QueryOptions(
                                                      limit=1000,
                                                      cursor=search.Cursor(),
                                                      sort_options=None))
                               )

        # Populate dict with props of search results
        for scored_document in results.results:
            # Create dict containing props from search results
            results_dict = dict()
            for prop in scored_document.fields:
                results_dict[prop.name] = prop.value
            # Append this batch of fetched deliveries to other batches
            if results_dict not in all_units:
                all_units.append(results_dict)

    units = []
    for u in all_units:
        unit = Unit.get_by_id(int(u.get('unit_id', '')))
        if unit:
            units.append(unit)

    unit_dicts = [x.to_dictionary() for x in units]

    return jsonify({'units': unit_dicts, 'next_page': 'next_page'})


@app.route('/a/units/this_week', methods=['GET'])
def units_this_week():
    """
    This function returns a list of unit dicts  that have dropoff or pickups this week.
    :return: JSON list of serialized Unit objects
    """
    import models.unit as units

    start_date = datetime.datetime.now() - datetime.timedelta(days=7)
    end_date = datetime.datetime.now() + datetime.timedelta(days=7)

    u1 = Unit.query(Unit.dropoff_date >= start_date, Unit.dropoff_date <= end_date,
                    Unit.status == units.PENDING_DROPOFF).fetch()
    u2 = Unit.query(Unit.pickup_date >= start_date, Unit.pickup_date <= end_date,
                    Unit.status == units.PENDING_PICKUP).fetch()

    new_list = u1 + u2
    # for u in u1 + u2:
    #     if new_list.count(u) > 1:
    #         new_list.remove(u)

    unit_dicts = [x.to_dictionary() for x in new_list]

    return jsonify({'units': unit_dicts, 'next_page': 'next_page'})


@app.route('/a/units/search_dates_two', methods=['GET', 'POST'])
def units_search_dates2():
    """
    This function searches for units between two dates
    :return: JSON list of serialized Unit objects
    """

    sd = request.args.get('sd')
    ed = request.args.get('ed')

    start_date = datetime.datetime.strptime(sd, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(ed, "%Y-%m-%d") + datetime.timedelta(days=1)

    u1 = Unit.query(Unit.dropoff_date >= start_date, Unit.dropoff_date <= end_date).fetch()
    u2 = Unit.query(Unit.pickup_date >= start_date, Unit.pickup_date <= end_date).fetch()

    new_list = u1 + u2
    for u in u1 + u2:
        if new_list.count(u) > 1:
            new_list.remove(u)

    unit_dicts = [x.to_dictionary() for x in new_list]
    calendar_events = [x.to_event_pickup() for x in u2]
    this_month = datetime.datetime.now().replace(day=1).strftime("%Y-%m-%d")

    return jsonify(
        {'units': unit_dicts, 'next_page': 'next_page', "pickups": calendar_events, "this_month": this_month})


def get_dates_between_two(sd, ed):
    """

    :param sd: String date come in in format dd-m-yyyy e.g. 25-12-1993
    :param ed:  String date come in in format dd-m-yyyy e.g. 25-12-1993
    :return: list of date strings in format ddmmyyyy e.g. 25061993. Use in search documents
    """
    start_date = datetime.datetime.strptime(sd, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(ed, "%Y-%m-%d") + datetime.timedelta(days=1)

    delta = end_date - start_date  # timedelta

    date_list = []
    for i in range(delta.days + 1):
        date = start_date + datetime.timedelta(days=i)
        date_list.append("{}{}{}".format(date.day, date.month, date.year))
    return date_list


@app.route('/a/unit/sizes', methods=['POST', 'GET'])
def get_unit_sizes():
    return jsonify({"sizes": POD_SIZE_HUMANIZE.values()})


@app.route('/a/edit_unit', methods=['POST', 'GET'])
def admin_edit_unit():
    # Verify Firebase auth.
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    # [START create_entity]
    data = request.get_json()

    # Populates note properties according to the model,
    # with the user ID as the key name.
    print data

    unit = Unit.get_by_id(data["unit_id"])
    unit.unit_size = POD_SIZE[data['size']]
    unit.type_name = data['type_name']
    unit.unit_name = data['unit_name']
    unit.unit_type = UNIT_TYPES[unit.type_name]

    if data["dropoff_date"] != ' ':
        dod = datetime.datetime.strptime(data["dropoff_date"], "%Y-%m-%d %H:%M")
        pud = datetime.datetime.strptime(data["pickup_date"], "%Y-%m-%d %H:%M")
        unit.dropoff_date = dod
        unit.pickup_date = pud

    # print dt

    msg = ''
    if unit.dropoff_date:
        msg = 'dropoff: ' + data["dropoff_date"] + '& pickup:' + data["pickup_date"]

    # Some providers do not provide one of these so either can be used.

    # [END create_entity]
    # send_message(current_user.email, msg+'<br>Nice! You have reserved POD ' + unit.unit_name + '<br>: To change any particulars (and, if you opted to deliver it, manage deliveriy of your POD) click on Manage under My Units on your profile page.<br> Note:'+unit.message)
    # Stores note in database.
    unit.put()

    return jsonify({"success": "POD UPDATED"})


@app.route('/api/v1/current_users', methods=['POST', 'GET', 'PUT'])
@authorized
def create_user(session_token):
    if request.method == 'GET':
        return handlers.client_request_handler.fetch_user_details(session_token)
    elif request.method == 'POST':
        return handlers.client_request_handler.verify_user(session_token)
    else:
        return handlers.client_request_handler.edit_user(session_token)


@app.route('/api/v1/current_users/units', methods=['GET', 'POST', 'PUT'])
@authorized
def get_units(session_token):
    if request.method == 'GET':
        return handlers.client_request_handler.get_current_user_units(session_token)
    elif request.method == 'POST':
        return handlers.client_request_handler.reserve_unit(session_token)
    else:
        return handlers.client_request_handler.edit_unit(session_token)


@app.route('/api/v1/current_users/units/quotes', methods=['GET'])
@authorized
def quote_unit(session_token):
    return handlers.client_request_handler.unit_quote(session_token)


@app.route('/u/geo-code-address')
@authorized
def geo_code_address(session_token):
    address = request.args.get('address', None)
    coordinates = geo.get_coordinates(str(address))
    if coordinates is None:
        return jsonify(
            {"status": "error", "message": "Address not found, please provide the Street, Suburb, City and Province"})
    return jsonify({"status": True, "message": "Successfully geo-coded address!"})


@app.route('/get_units/<int:user_id>', methods=['GET'])
def get_units_by_user_id(user_id):
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    units = query_units_id(user_id)
    if len(units) == 0:
        return jsonify({"status": "failure"})
    return jsonify({"status": "success", "units": units})


@app.route('/u/release_unit/<int:unit_id>', methods=['POST', 'PUT'])
def release_unit(unit_id):
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    unit = Unit.get_by_id(unit_id)
    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    if unit.owner_key != u.key:
        return 'Unauthorized', 401

    unit.available = True
    unit.message = None
    unit.friendly_id = None
    unit.dropoff_date = None
    unit.pickup_date = None
    unit.owner_key = None
    unit.put()
    return jsonify({"success": "POD released, please check email for further instructions"})


@app.route('/a/categories', methods=['GET', 'POST'])
def admin_get_categories():
    if request.method == 'POST':
        # add new category
        data = request.get_json()
        name = data.get('name')
        description = data.get('description')
        item_category = ItemCategory()
        item_category.name = name
        item_category.description = description
        item_category.put()
        return jsonify({"status": "success", "message": "Successfully added category"})

    categories = ItemCategory.query().fetch()
    category_list = [x.name for x in categories]
    return jsonify({"categories": category_list})


@app.route('/u/items', methods=['POST', 'GET', 'PUT'])
def client_view_items_per_category():
    category = request.args.get('item_category', None)
    if not category:
        return jsonify({"status": "fail", "message": "No category in URL args"})
    query = ItemCategory.query(ItemCategory.name == category)
    if not query.count():
        return jsonify({"status": "fail", "message": "No category found for type {}".format(category)})

    items = Item.query(Item.category == query.fetch(1)[0].key).fetch()
    item_dict = [x.to_dictionary() for x in items]

    return jsonify({"items": item_dict, "item_type": "{}s".format(category.lower().title())})


@app.route('/a/items', methods=['POST', 'GET', 'PUT'])
def admin_manage_items():
    if request.method == 'PUT':
        data = request.form.to_dict()
        image_file = request.files.get('item_picture')

        file_name = '/podit-169717.appspot.com/item-pictures/{}'.format(uuid.uuid1())
        write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                         }, retry_params=None)
        write_buffer.write(image_file.stream.read())
        write_buffer.close()

        item_id = request.args.get('item_id')
        item = Item.get_by_id(int(item_id))
        item.name = data.get('name')
        item.image_urls.append(file_name)
        item.description = data.get('description')
        item.category = ItemCategory.query(ItemCategory.name == data.get('category')).fetch(1)[0].key
        item.price = float(data.get('price'))
        item.put()
        return jsonify({"status": "success", "message": "Successfully edited item"})

    if request.method == 'POST':
        # add new item with category

        data = request.form.to_dict()
        image_file = request.files.get('item_picture')

        file_name = '/podit-169717.appspot.com/item-pictures/{}'.format(uuid.uuid1())
        write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                         }, retry_params=None)
        write_buffer.write(image_file.stream.read())
        write_buffer.close()

        item = Item()
        item.image_urls.append(file_name)
        item.name = data.get('name')
        item.description = data.get('description')
        item.category = ItemCategory.query(ItemCategory.name == data.get('category')).fetch(1)[0].key
        item.price = float(data.get('price'))
        item.put()
        return jsonify({"status": "success", "message": "Successfully added item"})

    category = request.args.get('item_category', None)
    if not category:
        return jsonify({"status": "fail", "message": "No category in URL args"})
    query = ItemCategory.query(ItemCategory.name == category)
    if not query.count():
        return jsonify({"status": "fail", "message": "No category found for type {}".format(category)})

    items = Item.query(Item.category == query.fetch(1)[0].key).fetch()
    item_dict = [x.to_dictionary() for x in items]

    return jsonify({"items": item_dict, "item_type": "{}s".format(category.lower().title())})


@app.route('/u/item/<string:item_type>', methods=['POST', 'GET'])
def get_items(item_type):
    query = ItemCategory.query(ItemCategory.name == item_type)
    if not query.count():
        return jsonify({"status": "fail", "message": "No category found for type {}".format(item_type)})
    items = Item.query(Item.category == query.fetch(1)[0].key).fetch()
    item_dict = [x.to_dictionary() for x in items]

    return jsonify({"items": item_dict, "item_type": "{}s".format(item_type.lower().title())})


@app.route('/u/get_cart_items', methods=['POST', 'GET'])
def get_cart_items():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    try:
        shopping_cart = ShoppingCart.query(ShoppingCart.owner_key == u.key).fetch()[0]
        print shopping_cart.items
    except:
        logging.error('No cart for user')
        return jsonify({"status": "error"})
    return jsonify({"count": len(shopping_cart), "status": "success",
                    'items': [x.get().to_dictionary() for x in shopping_cart.items]})


@app.route('/u/add_to_cart/<int:item_id>', methods=['POST', 'GET'])
def add_to_cart(item_id):
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    item = Item.get_by_id(int(item_id))
    try:
        shopping_cart = ShoppingCart.query(ShoppingCart.owner_key == u.key).fetch()[0]
        shopping_cart.items.append(item.key)
        shopping_cart.put()
    except:
        logging.error('No cart for user')
    return jsonify({"status": "success", 'item': item.to_dictionary()})


@app.route('/u/remove_from_cart/<int:item_id>', methods=['POST', 'GET'])
def remove_from_cart(item_id):
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    u = User.query(User.firebase_id == claims['sub']).fetch()[0]
    item = Item.get_by_id(int(item_id))
    try:
        shopping_cart = ShoppingCart.query(ShoppingCart.owner_key == u.key).fetch()[0]
        shopping_cart.items.remove(item.key)
        shopping_cart.put()
    except:
        logging.error('No cart for user')
    return jsonify({"status": "success", 'item': item.to_dictionary()})


@app.route('/u/create_eft_button', methods=['POST', 'GET'])
def create_eft_button():
    import uuid
    import hashlib
    x = uuid.uuid1()
    print str(x)
    SID_REFERENCE = str(x)[:5]
    SID_CONSISTENT = SID_MERCHANT + SID_CURRENCY + SID_COUNTRY + SID_REFERENCE + '1.12' + SID_SECRET_KEY
    m = hashlib.sha512()
    m.update(SID_CONSISTENT)
    SID_CONSISTENT = m.hexdigest().upper()
    print SID_CONSISTENT
    print m
    return jsonify({"status": "success", 'message': "EFT button created",
                    "SID_CONSISTENT": SID_CONSISTENT,
                    "SID_REFERENCE": SID_REFERENCE})


@app.route('/u/checkout', methods=['POST', 'GET'])
def checkout():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    current_user = User.query(User.firebase_id == claims['sub']).fetch()[0]
    shopping_cart = ShoppingCart.query(ShoppingCart.owner_key == current_user.key).fetch()[0]
    order = Order.create_from_shopping_cart(shopping_cart)
    print order.calculate_total()
    charge = Charge()
    charge.charge_type = CHARGE_TYPE['ONCE']
    charge.item_key = order.key
    charge.owner_key = current_user.key
    charge.amount = order.calculate_total()
    charge.put()
    return jsonify({"success": "Order created", "total": order.calculate_total()})


@app.route('/u/cancel_rent')
@authorized
def cancel_rent(session_token):
    current_user = session_token.get('current_user')
    unit_id = request.args.get('unit_id')
    unit = Unit.get_by_id(int(unit_id))
    charge_query = Charge.query(Charge.owner_key == current_user.key, Charge.item_key == unit.key)
    unpaid_charge_query = Charge.query(Charge.owner_key == current_user.key, Charge.item_key == unit.key,
                                       Charge.paid == False)
    if charge_query.count() == 1 and charge_query.get().paid == False:
        # The user has 1 charge and it is unpaid
        charge = charge_query.get()
        charge.key.delete()
        unit.reset()
        return jsonify({"success": True, "total": 3})

    if unpaid_charge_query.count():
        # user has unpaid charges
        return jsonify({"success": False, "total": 3})

    unit.cancel_rent()

    communicator.send_email(current_user.email, """You have opted to cancel rent.<br>Please vacate the unit as soon as 
                                        possible as charges will only stop incurring once you have vacated the unit""")

    return jsonify({"success": True, "total": 3})


@app.route('/u/payments', methods=['GET'])
def get_payments_for_user():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401

    current_user = User.query(User.firebase_id == claims['sub']).fetch()[0]
    query = Charge.query(Charge.owner_key == current_user.key)
    if query.count():
        charges, next_curs, more = Charge.query(Charge.owner_key == current_user.key).fetch_page(200, start_cursor=None)
        return jsonify({"charges": [x.to_dictionary() for x in charges]})
    else:
        return jsonify({"status": "error"})


@app.route('/u/order_information', methods=['GET'])
def get_order_information():
    claims = is_authorized(request)
    if not claims:
        return 'Unauthorized', 401
    current_user = User.query(User.firebase_id == claims['sub']).fetch()[0]

    available_sizes = []
    for k, v in POD_SIZE.iteritems():
        if Unit.query(ndb.AND(Unit.unit_size == v, Unit.available == True)).count():
            available_sizes.append(k)

    return jsonify({"available_sizes": available_sizes, "address": current_user.address.full_name})


@app.errorhandler(500)
def server_error(e):
    # Log the error and stacktrace.
    logging.exception('An error occurred during a request.')
    return 'An internal error occurred.', 500

# [END app]
