import os
import sys
import io
import csv

from google.appengine.ext import ndb
import datetime
import models.unit
import utils.time_utils
LOGIN = 0
LOGOUT = 1


class Log(ndb.Model):
    """NDB model class for a log

    """

    created = ndb.DateTimeProperty(auto_now_add=True)
    item_key = ndb.KeyProperty(required=True)
    owner_key = ndb.KeyProperty(required=True)


class ClientLog(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    image_url = ndb.StringProperty()
    category = ndb.StringProperty(choices=["POD_DROP", "POD_PICKUP", "VIEWING", "COLLECTING"])
    note = ndb.StringProperty(required=True)
    archived = ndb.BooleanProperty(default=False)
    email = ndb.StringProperty(required=True)
    name = ndb.StringProperty(required=True)
    client_type = ndb.StringProperty(choices=['NEW', 'EXISTING'])
    unit_code = ndb.StringProperty(required=True)
    cell_no = ndb.StringProperty(required=True)

    def to_dictionary(self):

        return dict(created=utils.time_utils.to_zar_time(self.created),
                    category={"POD_DROP": "Dropped off Unit",
                              "POD_PICKUP": "Picked up Unit",
                              "VIEWING": "Viewed POD at facility",
                              "COLLECTING": "Collected items from Unit"}.get(self.category, "N/A"),
                    client_type=self.client_type.lower(),
                    unit_code=self.unit_code,
                    email=self.email,
                    note=self.note,
                    cell_no=self.cell_no,
                    name=self.name,
                    id=self.key.id(),
                    image_url=self.image_url)



class PodLog(Log):
    category = ndb.StringProperty(choices=["LOGIN", "LOGOUT", "CANCELLED", "RENTED", "UPDATED"])
    description = ndb.StringProperty()

    def to_dictionary(self):
        owner = self.owner_key.get()
        unit = self.item_key.get()

        return dict(created=utils.time_utils.to_zar_time(self.created),
                    category=self.category,
                    description=self.description,
                    user_id=self.owner_key.id(),
                    unit_id=self.item_key.id(),
                    unit_name=unit.unit_name if unit.unit_name else 'N/A',
                    user_name=owner.name if owner.name else 'N/A')


class OrderLog(Log):
    category = ndb.StringProperty(choices=["SHIPPED", "PLACED", "RETURNED"])
