import os
import sys
import io
import csv

from google.appengine.ext import ndb
import datetime


class Address(ndb.Model):
    """NDB model class for a user's Address.

    """


    created = ndb.DateTimeProperty(auto_now_add=True)
    coordinates = ndb.GeoPtProperty()
    owner_key = ndb.KeyProperty(default=None)
    name = ndb.StringProperty()
    full_name = ndb.StringProperty()


