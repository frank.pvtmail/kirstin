# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import sys
import io
import csv
import logging
from google.appengine.ext import ndb
from google.appengine.api import search
import services.user_orchestrator
import models.charge
from address import Address

location_map = {"PE": "Port Elizabeth",
                         "CPT": "Cape Town",
                         "JHB": "Johannesburg",
                         "DBN": "Durban"}

class Ad(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    owner_key = ndb.KeyProperty(required=True)
    archived = ndb.BooleanProperty(default=False)
    image_urls = ndb.StringProperty(repeated=True)
    title = ndb.StringProperty()
    price = ndb.FloatProperty()
    link = ndb.StringProperty()
    content = ndb.TextProperty()
    keywords = ndb.StringProperty()
    location = ndb.StringProperty(choices=['JHB', 'CPT', 'PE', 'DBN'])
    shipping = ndb.StringProperty(choices=['YES-BUYER', 'YES-SELLER', 'NO', 'NA'])

    @staticmethod
    def find_ads_by_owner_key(key):
        ads = Ad.query(Ad.owner_key == key).fetch()
        return ads

    def create_search_document(self):
        return search.Document(
            doc_id=self.key.kind() + str(self.key.id()),
            fields=self.populate_index_fields()
        )

    def to_dictionary(self):
        user_dict = {}
        user_dict.update({
            "id": self.key.id(),
            "title": self.title,
            "created": self.created,
            "location": self.location,
            "location_hr": {"PE": "Port Elizabeth",
                         "CPT": "Cape Town",
                         "JHB": "Johannesburg",
                         "DBN": "Durban"}.get(self.location),
            "content": self.content,
            "price": self.price,
            "shipping": {"YES-SELLER": "Yes at sellers cost",
                         "YES-BUYER": "Yes at buyers cost",
                         "NO": "No, collection only",
                         "NA": "Not applicable"}.get(self.shipping),
            "link": self.link
            })
        return user_dict



    def get_search_index(self):
        """
        Generates a search index.
        :return index:
        """
        return search.Index(name='ads')

    @classmethod
    def get_class_search_index(cls):
        """
        Generates a search index.
        :return index:
        """
        return search.Index(name='ads')


    def _post_put_hook(self, future):
        logging.info('updating search doc')
        document = self.create_search_document()
        self.get_search_index().put([document])

    @classmethod
    def _pre_delete_hook(cls, key):
        try:
            print 'delete index'
            docindex = cls.get_class_search_index()
            docindex.delete(key.kind()+str(key.id()))
        except Exception as e:
            logging.error('Error deleting doc')

    @classmethod
    def deleteAllInIndex(cls):
        """Delete all the docs in the given index."""
        docindex = cls.get_class_search_index()

        try:
            while True:
                # until no more documents, get a list of documents,
                # constraining the returned objects to contain only the doc ids,
                # extract the doc ids, and delete the docs.
                document_ids = [document.doc_id for document in docindex.get_range(ids_only=True)]
                if not document_ids:
                    break
                docindex.delete(document_ids)
        except search.DeleteError:
            logging.exception("Error removing documents:")

    def populate_index_fields(self):
        return [search.DateField(name='created', value=self.created),
                    search.TextField(name='location', value=self.location),
                search.TextField(name='title', value=self.title),
                search.TextField(name='content', value=self.content),
                search.TextField(name='keywords', value=self.keywords),
                search.TextField(name='ad_id', value=str(self.key.id()))
                ]


class Rating(ndb.Model):
    owner_key = ndb.KeyProperty()
    score = ndb.StringProperty(choices=['GOOD', 'BAD', 'NEUTRAL'])
    ad_id = ndb.StringProperty()
    submitter_key = ndb.KeyProperty()


class User(ndb.Model):

    firebase_id = ndb.StringProperty(default='')
    name = ndb.StringProperty(default='')
    archived = ndb.BooleanProperty(default=False)
    cell_no = ndb.StringProperty(default=None)
    is_driver = ndb.BooleanProperty(default=False)
    email = ndb.StringProperty(default='')
    created = ndb.DateTimeProperty(auto_now_add=True)
    address = ndb.StructuredProperty(Address, default=None)
    is_admin = ndb.BooleanProperty(default=False)
    vat_number = ndb.StringProperty(default='')
    business_name = ndb.StringProperty(default='')
    last_seen = ndb.DateTimeProperty(default=None)
    peach_reg_id = ndb.StringProperty(default=None)
    outstanding_charges = ndb.BooleanProperty(default=False)
    unpaid_charges = ndb.ComputedProperty(lambda self: self.unpaid_charges_calc())
    recurring_billing = ndb.BooleanProperty(default=False)
    shopping_cart_key = ndb.KeyProperty(default=None)
    friendly_id = ndb.ComputedProperty(lambda self: self.uuid())
    verification_code = ndb.StringProperty()
    verified = ndb.BooleanProperty(default=False)
    rating_count = ndb.IntegerProperty()

    @property
    def is_authenticated(self):
        """
        Needed by Flask-login
        :return:
        """
        return True

    def is_active(self):
        """
        Needed by Flask-login
        :return:
        """
        return True


    def is_anonymous(self):
        """
        Needed by Flask-login
        :return:
        """
        return False

    def get_id(self):
        """
        Needed by Flask-login
        :return:
        """
        return unicode(self.key.id())

    def unpaid_charges_calc(self):
        total = 0
        for c in models.charge.Charge.query(models.charge.Charge.owner_key == self.key,
                                      models.charge.Charge.paid == False,
                                      models.charge.Charge.archived == False).fetch():
            total += c.amount

        return round(total, 2)

    def uuid(self):
        return self.firebase_id[:4]

    @staticmethod
    def find_by_email(email):
        user = User.query(User.email == email.lower()).get()
        return user

    def to_dictionary(self):
        user_ratings = Rating.query(Rating.owner_key == self.key).fetch()
        total_ratings = len(user_ratings)
        positive_ratings = len([x for x in user_ratings if x.score == 'GOOD'])

        user_dict = {}
        user_dict.update({
            "email": self.email,
            "id" : self.key.id(),
            "name": self.name,
            "total_ratings": total_ratings,
            "positive_ratings": positive_ratings,
            "user_rating": (float(positive_ratings)/float(total_ratings))*100 if total_ratings else 'N/a',
            'cell_no': self.cell_no,
            'address': self.address.full_name if self.address else 'N/a',

            })
        return user_dict

    def _post_put_hook(self, future):
        logging.info('updating search doc')
        document = self.create_search_document()
        self.get_search_index().put([document])

    def create_search_document(self):
        return search.Document(
            doc_id=self.key.kind() + self.firebase_id,
            fields=self.populate_index_fields()
        )

    def get_search_index(self):
        """
        Generates a search index.
        :return index:
        """
        return search.Index(name='users')

    @classmethod
    def get_class_search_index(cls):
        """
        Generates a search index.
        :return index:
        """
        return search.Index(name='users')

    @classmethod
    def _pre_delete_hook(cls, key):
        try:
            print 'delete index'
            docindex = cls.get_class_search_index()
            docindex.delete(key.kind()+key.get().firebase_id)
        except Exception as e:
            logging.error('Error deleting doc')

    @classmethod
    def deleteAllInIndex(cls):
        """Delete all the docs in the given index."""
        docindex = cls.get_class_search_index()

        try:
            while True:
                # until no more documents, get a list of documents,
                # constraining the returned objects to contain only the doc ids,
                # extract the doc ids, and delete the docs.
                document_ids = [document.doc_id for document in docindex.get_range(ids_only=True)]
                if not document_ids:
                    break
                docindex.delete(document_ids)
        except search.DeleteError:
            logging.exception("Error removing documents:")


    @classmethod
    def update_users_search_indexes(cls):
        users = cls.query().fetch()
        for u in users:
            u.put()


    def populate_index_fields(self):
        address = ''
        if self.address:
            address = self.address.full_name
        return [search.TextField(name='cell_no', value=self.cell_no),
                search.TextField(name='email', value=self.email),
                search.TextField(name='name', value=self.name),
                search.TextField(name='address', value=address),
                search.TextField(name='last_seen', value=str(self.last_seen)),
                search.TextField(name='user_id', value=str(self.key.id()))]