import datetime
from google.appengine.api import search
from google.appengine.ext import ndb
from google.appengine.ext import deferred
from google.appengine.api import memcache
import models.address
import models.charge
import models.item
import models.unit
import models.order

MEMCACHE_PREFIX = 'X-Singleton-ID-'

PEACH_USER_ID = '8a8294185e997b82015ea167d3712759'
PEACH_PASSWORD = 'tSWdD5STaM'

INITIAL_CHANNEL_ID = '8a8294185e997b82015ea1739974275f'
RECURRING_CHANNEL_ID = '8a8294185e997b82015ea1740efe2763'

TWILIO_ACCOUNT_SID = 'AC69ea431ec8c6971a072e1af614a590f5'
TWILIO_AUTH_TOKEN = '4ebe0beddf682fc5f7ad8b2cee174c8b'
TWILIO_NUMBER = '+12563054514'
MAILJET_API_KEY = '65db900cd8fcb2b496aab102a5ff599d'
MAILJET_API_SECRET = '93e77c83d968ae749995ae714b763cdb'
MAILJET_SENDER = 'frank.pvtmail@gmail.com'


class PoorMansSettings(object):
    """

    NB: Whenever you add a value here, please also add it to production & staging config records.
    Just as NB: Whenever you add something to the production & staging configs, please also add it here.

    """

    environment = 'STAGING'

    INITIAL_CHANNEL_ID = '8a8294185e997b82015ea1739974275f'
    RECURRING_CHANNEL_ID = '8a8294185e997b82015ea1740efe2763'

    TWILIO_ACCOUNT_SID = 'ACde1693b4333e069968b9d57d201f0bee'
    TWILIO_AUTH_TOKEN = '8e0c2772d896d8be3b0d1cc8ad36d239'
    TWILIO_NUMBER = '+18782061935'
    MAILJET_API_KEY = '65db900cd8fcb2b496aab102a5ff599d'
    MAILJET_API_SECRET = '93e77c83d968ae749995ae714b763cdb'
    MAILJET_SENDER = 'frank.pvtmail@gmail.com'
    MINI_POD_PRICE = 225
    POD_PRICE = 325
    B_PRICE = 40
    LARGE_POD_PRICE = 425
    JUMBO_POD_PRICE = 500
    C_PRICE = 425
    HC_PRICE = 500
    admin_emails = ['frank.pvtmail@gmail.com']
    delivery_cost = 200
    slot_limit = 4
    booking_cutoff_days = 60

    def __init__(self):
        # TODO the below warning is polluting debug logs
        # logging.warning('Using PoorMansSettings object (fallback to SystemSettings), which is not recommended.')
        pass

    def put(self):
        pass

    @classmethod
    def get_object(cls):
        return PoorMansSettings()


class Gate(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    id = ndb.StringProperty(required=True)
    name = ndb.StringProperty()
    region = ndb.StringProperty(choices=["STRAND", "STELLENBOSCH", "PAARL"])
    base_station_key = ndb.KeyProperty()

    @classmethod
    def get_by_nold_id(cls, nid):
        return cls.query(cls.id == nid).get()

    @classmethod
    def get_by_bs_key(cls, key):
        return cls.query(cls.base_station_key == key).get()

class BaseStation(ndb.Model):

    created = ndb.DateTimeProperty(auto_now_add=True)
    name = ndb.StringProperty()
    archived = ndb.BooleanProperty(default=False)
    address = ndb.StructuredProperty(models.address.Address)

    @classmethod
    def fetch_all(cls):
        stations = cls.query().fetch()
        return stations

    @classmethod
    def fetch_unarchived(cls):
        stations = cls.query(cls.archived == False).fetch()
        return stations

    @staticmethod
    def find_by_name(name):
        bs = BaseStation.query(BaseStation.name == name).get()
        return bs


class CalendarEvent(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    archived = ndb.BooleanProperty(default=False)
    category = ndb.StringProperty(
        choices=['PICKUP', 'DROPOFF', 'DROPOFF_EVAC', 'PICKUP_EVAC', 'CLIENT_VISIT', 'CLIENT_VISIT_EVAC'])
    unit_key = ndb.KeyProperty()
    item_keys = ndb.KeyProperty(repeated=True)
    status = ndb.StringProperty(choices=['CANCELLED', 'COMPLETED', 'PENDING'], default='PENDING')
    owner_key = ndb.KeyProperty()
    delivery_id = ndb.StringProperty()
    charge_key = ndb.KeyProperty()
    updated = ndb.BooleanProperty(default=True)
    date = ndb.DateTimeProperty()
    internal_datetime = ndb.DateTimeProperty()
    event_id = ndb.StringProperty()
    address = ndb.StructuredProperty(models.address.Address)
    user_comment = ndb.TextProperty(default='')
    time_category = ndb.StringProperty(choices=['MORNING', 'AFTERNOON'])

    def to_dictionary(self):
        orders = []
        units = []
        items = ""
        for i in self.item_keys:
            if i.kind() == "Order":

                if i and i.get():
                    items += "Order ref: {} | ".format(i.get().order_ref)
                    orders.append(i.get())

            if i.kind() == "Unit":
                if i and i.get():
                    units.append(i.get())
                    items += "Unit name : {} | ".format(i.get().unit_name)

        item_dict = {
            "delivery_id": self.delivery_id,
            'id': self.key.id(),
            'orders': orders,
            'status': self.status,
            'time_category': self.time_category,
            "category": self.category,
            "category_formatted": self.category.replace("_", " "),
            'units': units,
            'date_formatted': self.date.strftime("%d %B %Y"),
            'owner_name': self.owner_key.get().name if self.owner_key else 'N/A',
            "date": self.date,
            "items_description": items,
            "address": self.address.full_name if self.address and self.address.full_name else 'Podit Stellenbosch'
        }
        return item_dict

    def _pre_put_hook(self):

        if self.time_category == 'MORNING':
            self.internal_datetime = self.date.replace(hour=8)
        else:
            self.internal_datetime = self.date.replace(hour=13)

    @classmethod
    def get_pending_calendar_events(cls, unit_id, user_id, category):
        return cls.query(cls.item_keys == ndb.Key('Unit', unit_id),
                         cls.status == 'PENDING',
                         cls.owner_key == ndb.Key('User', user_id),
                         cls.category == category,
                         cls.archived == False).get()


class OAuthCredentials(ndb.Model):
    scopes = ndb.StringProperty(repeated=True)
    token_uri = ndb.StringProperty()
    token = ndb.StringProperty()
    client_id = ndb.StringProperty()
    client_secret = ndb.StringProperty()
    refresh_token = ndb.StringProperty()


class SingletonBaseClass(ndb.Expando):
    created = ndb.DateTimeProperty(auto_now_add=True)
    oauth_credentials = ndb.StructuredProperty(OAuthCredentials, default=None)

    def __cmp__(self, other):
        if self.created > other.created:
            return 1
        elif self.created < other.created:
            return -1
        else:
            return 0

    def _pre_put_hook(self):
        if not self.key:
            # Means we are saving a new record.
            print ndb.Query(kind=self._class_name()).count()
            if ndb.Query(kind=self._class_name()).count() > 0:
                raise ValueError('Attempted creation of a duplicate entry for a SingletonBaseClass subclass.')

    def _post_put_hook(self, future):
        memcache.add(key=MEMCACHE_PREFIX + self._class_name(), value=self.key.id(), time=3600 * 24)
        # Also, assuming a mistake could have occurred and a duplicate has been created:
        new_elements = sorted(ndb.Query(kind=self._class_name()).fetch())
        original = new_elements.pop(0)  # find the original (oldest) instance.
        ndb.delete_multi([x.key for x in new_elements])  # Delete the others.

    @classmethod
    def _pre_delete_hook(cls, key):
        memcache.delete(key=MEMCACHE_PREFIX + cls._class_name())

    @classmethod
    def get_object(cls):
        """
        Returns either the previously existing singleton or None.
        :return:
        """
        obj_id = memcache.get(MEMCACHE_PREFIX + cls._class_name())
        if not obj_id:
            try:
                obj = cls.query().fetch(1)[0]
                memcache.add(key=MEMCACHE_PREFIX + cls._class_name(), value=obj.key.id(), time=3600 * 24)
            except IndexError:
                obj = None
        else:
            obj = cls.get_by_id(int(obj_id))
        return obj


class PoditSettings(ndb.Expando):
    created = ndb.DateTimeProperty(auto_now_add=True)
    oauth_credentials = ndb.StructuredProperty(OAuthCredentials, default=None)

    @classmethod
    def fetch(cls):
        """
        Retrieves SystemSettings object.
        If that fails, returns the PoorMansSettings object instead.

        :rtype: object
        :return:
        """
        obj = cls.query().get()
        if not obj:
            obj = PoorMansSettings.get_object()
        return obj  # type: PoorMansSettings


class SystemSettings(SingletonBaseClass):

    @classmethod
    def fetch(cls):
        """
        Retrieves SystemSettings object.
        If that fails, returns the PoorMansSettings object instead.

        :rtype: object
        :return:
        """
        obj = cls.get_object()
        if not obj:
            obj = PoorMansSettings.get_object()
        return obj  # type: PoorMansSettings
