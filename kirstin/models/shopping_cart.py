import os
import sys
import io
import csv

from google.appengine.ext import ndb
import datetime
import models.item


class ShoppingCart(ndb.Model):
    """NDB model class for a user's Shopping cart.

    """

    created = ndb.DateTimeProperty(auto_now_add=True)
    owner_key = ndb.KeyProperty()
    items = ndb.KeyProperty(repeated=True)

    def calculate_total(self):
        total = 0
        for key in self.items:
            item = key.get()
            if item:
                total += item.price
        return round(total, 2)

