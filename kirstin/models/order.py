import os
import sys
import io
import csv
import string
import random
import logging
from google.appengine.ext import ndb
import datetime
import models.item
import models.shopping_cart
import models.log


class Order(ndb.Model):
    """NDB model class for a user's Order

    """

    created = ndb.DateTimeProperty(auto_now_add=True)
    owner_key = ndb.KeyProperty(required=True)
    archived = ndb.BooleanProperty(default=False)
    items = ndb.KeyProperty(repeated=True)
    shop_items = ndb.StructuredProperty(models.item.Item, repeated=True)
    paid = ndb.BooleanProperty(default=False)
    order_ref = ndb.StringProperty(required=True, default=None)
    status = ndb.StringProperty(choices=["AT_HQ", "SHIPPED", "RETURNED", "CANCELLED", "COMPLETED"], default="AT_HQ")

    def __len__(self):
        return len(self.items)

    def _pre_put_hook(self):
        logging.info('updating search doc')
        if not self.order_ref:
            self.gen_name()

    def ship(self):
        if not self.paid:
            return False
        self.status = "SHIPPED"
        self.put()
        order_log = models.log.OrderLog()
        order_log.category = "SHIPPED"
        order_log.owner_key = self.owner_key
        order_log.item_key = self.key
        order_log.put()

        for i in self.items:
            item = i.get()
            item.shipped_count += 1
            item.put()
        return True

    @staticmethod
    def get_by_order_ref(order_ref):
        return Order.query(Order.order_ref == order_ref).get()

    def gen_name(self):

        banned_names = ['FUCK', 'CUNT', 'POES', 'FVCK', 'VAGJ', 'VAGG', 'DICK', 'FOKK', 'FOCK', 'SHIT', 'SHTT', 'COCK']
        logging.info('generating name')

        order_ref = Order.generate_order_ref()
        while Order.query(Order.order_ref == order_ref).get() or order_ref in banned_names:
            order_ref = Order.generate_order_ref()

        logging.info('done generating name')
        self.order_ref = order_ref
        self.put()

    @staticmethod
    def generate_order_ref():
        name = ''
        for i in range(0, 4):
            rng = random.randint(0, 9)
            if rng < 5:
                name += str(random.randint(0, 9))
            else:
                name += random.choice(string.letters).upper()

        return name

    def to_dictionary(self):
        owner_email = 'N/A'
        owner_name = 'N/A'
        item_string = 'N/A'
        item_dicts = list()
        total = 0
        alt_total = 0

        for key in self.items:
            try:
                if key:
                    item = key.get()
                    total += item.price
                    item_dicts.append(item.to_dictionary())
                    if item:
                        item_string += "name: {} price: {} description: {} ".format(item.name, item.price, item.description)
            except Exception as e:
                logging.error(e)

        if self.owner_key:
            owner = self.owner_key.get()
            owner_name = owner.name
            owner_email = owner.email

        return dict(owner_key=self.owner_key,
                    items=item_string,
                    item_dicts=item_dicts,
                    total_charge=total,
                    total=self.calculate_total(),
                    created=self.created,
                    id=self.key.id(),
                    item_count=len(self),
                    owner_name=owner_name,
                    status=self.status,
                    owner_email=owner_email,
                    order_ref=self.order_ref,
                    paid=['NOT PAID', 'PAID'][self.paid])

    def calculate_total(self):
        total = 0
        try:
            for item in self.shop_items:
                total += item.price
        except:
            total = 0
            for key in self.items:
                item = key.get()

                total += item.price

        return round(total, 2)

    @classmethod
    def create_from_shopping_cart(cls, cart):
        order = cls()
        for ik in cart.items:
            item = ik.get()
            item_count = cart.items.count(ik)
            if item_count > item.quantity:
                return None, "No stock left of {}, please remove it from your cart and try checking out again".format(item.name)

            order.shop_items.append(item)

        if len(cart.items):
            new_list = cart.items[:]
            order.items = new_list
            order.owner_key = cart.owner_key
            cart.items = []
            cart.put()
            order.put()
            return order, "Successfully created order"

        logging.info("No shopping cart items found")

        return None, "Cart has no itemsin"
