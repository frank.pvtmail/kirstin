
import os
import sys
import io
import csv

from google.appengine.ext import ndb

import random
import string
import logging
from google.appengine.api import search


from models.address import Address
import models.charge
import models.system_settings


AT_FACILITY = 0
PENDING_DROPOFF = 1
PENDING_PICKUP = 2
CANCELLED = 3

STATUS_TO_TEXT = {
    AT_FACILITY: "AT FACILITY",
    PENDING_DROPOFF: "PENDING DROPOFF",
    PENDING_PICKUP: "PENDING PICKUP",
    CANCELLED: "CANCELLED"
}

POD_SIZE_DESCR = {
    "HC - Half container": ["HC", 5],
    "C - Container": ["C", 4],
    "JP - Jumbo Pod": ["JP", 3],
    "LP - Large Pod": ["LP", 2],
    "SP - Standard Pod": ["SP", 1],
    "MP - Mini Pod": ["MP", 0],
    "B - Hinged lid security box": ["B", 6]
}

POD_SIZE = {
    "MP": 0,
    "SP": 1,
    "LP": 2,
    "JP": 3,
    "C": 4,
    "HC": 5,
    "B":6
}

POD_SIZE_HUMANIZE = {
    0: "MP",
    1: "SP",
    2: "LP",
    3: "JP",
    4: "C",
    5: "HC",
    6: "B"
}

TYPES_HUMANIZE = {
    0: "POD",
    1: "SS"
}


class Unit(ndb.Model):
    archived = ndb.BooleanProperty(default=False)
    friendly_id = ndb.StringProperty()
    last_updated = ndb.DateTimeProperty(auto_now=True)
    last_order_date = ndb.DateTimeProperty(default=None)
    unit_size = ndb.IntegerProperty(choices=POD_SIZE_HUMANIZE.keys())
    price = ndb.FloatProperty(default=0.0)
    base_station_key = ndb.KeyProperty(default=None)
    owner_key = ndb.KeyProperty()
    available = ndb.BooleanProperty()
    on_hold = ndb.BooleanProperty()
    message = ndb.TextProperty()
    unit_picture = ndb.StringProperty(default=None)
    unit_name = ndb.TextProperty(indexed=True)
    position_number = ndb.StringProperty(default=None)
    status = ndb.IntegerProperty()
    term_end = ndb.DateTimeProperty(default=None)
    paid_till = ndb.DateTimeProperty(default=None)
    unit_type = ndb.IntegerProperty()
    type_name = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)
    pickup_date = ndb.DateTimeProperty(default=None)
    dropoff_date = ndb.DateTimeProperty(default=None)
    locations = ndb.StringProperty(choices=['PODIT', 'CLIENT_PREMISES'], repeated=True)
    client_address = ndb.StructuredProperty(Address, default=None)
    logged_in = ndb.BooleanProperty(default=True)
    removal_uid = ndb.StringProperty(default=None)


    @classmethod
    def get_by_name(cls, name):
        return cls.query(cls.unit_name == name).get()

    @staticmethod
    def gen_postfix(letters, numbers):
        name = ''
        for i in range(0, numbers):
            name += str(random.randint(0, 9))
        for y in range(0, letters):
            name += random.choice(string.letters).upper()
        return name



    def cancel_calendar_events(self):
        """
        This function deletes calendar events for the unit
        :return:
        """
        calendar_events = models.system_settings.CalendarEvent.query(models.system_settings.CalendarEvent.item_keys == self.key,
                                                                     models.system_settings.CalendarEvent.owner_key == self.owner_key,
                                                                     models.system_settings.CalendarEvent.status == 'PENDING').fetch()

        for e in calendar_events:
            if len(e.item_keys) == 1:
                if e.charge_key and e.charge_key.get():
                    charge = e.charge_key.get()
                    charge.archived = True
                    charge.put()
                e.status = 'CANCELLED'
            else:
                e.item_keys.remove(self.key)
            e.put()


    def cancel_rent(self):
        """
        This function marks the unit for vacation and deletes any calendar events for the unit
        :return:
        """
        self.cancel_calendar_events()

        self.status = CANCELLED
        self.put()

    def reset(self):
        """
        This function resets the unit and deletes any calendar events for the unit
        :return:
        """
        self.cancel_calendar_events()

        self.available = True
        self.paid_till = None
        self.archived = False
        self.last_order_date = None
        self.logged_in = True
        self.dropoff_date = None
        self.pickup_date = None
        self.unit_picture = None
        self.status = AT_FACILITY
        self.price = 0
        self.owner_key = None
        self.friendly_id = None
        self.message = None
        self.term_end = None
        self.put()

    def gen_name(self):

        usize = POD_SIZE_HUMANIZE[self.unit_size]

        banned_names = ['FUCK', 'CUNT', 'POES', 'FVCK', 'VAGJ', 'VAGG', 'DICK', 'FOKK', 'FOCK', 'SHIT', 'SHTT', 'COCK']
        logging.info('generating name')
        while True:
            unit_name = usize.upper() + '|' + Unit.gen_postfix(letters=4, numbers=0)

            count = Unit.query(Unit.unit_name == unit_name).count()
            if count == 0 and unit_name not in banned_names:
                break
        logging.info('done generating name')
        self.unit_name = unit_name
        self.put()

    @classmethod
    def update_units_search_indexes(cls):
        units = cls.query().fetch()
        for u in units:
            u.put()


    @staticmethod
    def get_delivery_cost():
        settings = models.system_settings.PoditSettings.fetch()
        return float(settings.delivery_cost)

    def to_event_pickup(self):
        unit_dict = {
            'title': self.unit_name,
            'start': self.pickup_date.strftime("%Y-%m-%dT%H:%M:%S"),
            'url': "http://admin.podit.co.za/unit/{}".format(self.key.id())
        }

        return unit_dict

    def _post_put_hook(self, future):
        logging.info('updating search doc')
        document = self.create_search_document()
        self.get_search_index().put([document])

    def create_search_document(self):
        return search.Document(
            doc_id=self.key.kind() + self.unit_name,
            fields=self.populate_index_fields()
        )

    def get_search_index(self):
        """
        Generates a search index.
        :return index:
        """
        return search.Index(name='units')

    @classmethod
    def get_class_search_index(cls):
        """
        Generates a search index.
        :return index:
        """
        return search.Index(name='units')

    @classmethod
    def _pre_delete_hook(cls, key):
        try:
            calendar_events = models.system_settings.CalendarEvent.query(models.system_settings.CalendarEvent.item_keys == key)
            for e in calendar_events:
                e.unit_key = None
                e.put()

            docindex = cls.get_class_search_index()
            docindex.delete(key.kind() + key.get().unit_name)
        except Exception as e:
            logging.error('Error deleting doc')


    @classmethod
    def deleteAllInIndex(cls):
        """Delete all the docs in the given index."""
        docindex = cls.get_class_search_index()

        try:
            while True:
                # until no more documents, get a list of documents,
                # constraining the returned objects to contain only the doc ids,
                # extract the doc ids, and delete the docs.
                document_ids = [document.doc_id for document in docindex.get_range(ids_only=True)]
                if not document_ids:
                    break
                docindex.delete(document_ids)
        except search.DeleteError:
            logging.exception("Error removing documents:")

    def populate_index_fields(self):
        owner_name = ''
        drop_off = ''
        pickup = ''
        if self.owner_key:
            owner_name = self.owner_key.get().name
        if self.dropoff_date:
            drop_off = "{}{}{}".format(self.dropoff_date.day, self.dropoff_date.month, self.dropoff_date.year)
        if self.pickup_date:
            pickup = "{}{}{}".format(self.pickup_date.day, self.pickup_date.month, self.pickup_date.year)

        return [search.TextField(name='unit_name', value=self.unit_name),
                search.TextField(name='drop_off', value=drop_off),
                search.TextField(name='pickup', value=pickup),
                search.TextField(name='owner_name', value=owner_name),
                search.TextField(name='unit_id', value=str(self.key.id()))]


