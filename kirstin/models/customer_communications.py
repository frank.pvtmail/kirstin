import os
import sys
import io
import csv

from google.appengine.ext import ndb
import datetime


class CustomerCommunication(ndb.Model):

    created = ndb.DateTimeProperty(auto_now_add=True)
    medium = ndb.StringProperty(choices=["SMS", "EMAIL"])
    content = ndb.TextProperty()
    service_uid = ndb.StringProperty(required=True)

    status = ndb.StringProperty(choices=["FAILED", "SUCCESS"])
    owner_key = ndb.KeyProperty(default=None)



