import os
import sys
import io
import csv

from google.appengine.ext import ndb
import datetime


class ContentPart(ndb.Model):
    category = ndb.StringProperty(choices=["TEXT", "LINK", "SUBTITLE", "TITLE", "IMAGE", "BOLD_TEXT"])
    text = ndb.StringProperty()


class BlogPost(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    content_part_keys = ndb.KeyProperty(repeated=True)
    archived = ndb.BooleanProperty(default=False)
    reply_keys = ndb.KeyProperty(repeated=True)


class Comment(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    text = ndb.StringProperty()
    owner_name = ndb.StringProperty(default='N/A')
    reply_keys = ndb.KeyProperty(repeated=True)


class GalleryImage(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    url = ndb.StringProperty()
    tags = ndb.StringProperty()
    archived = ndb.BooleanProperty(default=False)
    description = ndb.StringProperty()

