import os
import sys
import io
import csv

from google.appengine.ext import ndb
# import google.auth.transport.requests
# import google.oauth2.id_token
# from handlers.db_tools import fetch_curs
# import requests_toolbelt.adapters.appengine
import datetime
# from models.unit import Unit

class Payment(ndb.Model):
    """NDB model class for a user's Payment.

    """


    created = ndb.DateTimeProperty(auto_now_add=True)
    item_key = ndb.KeyProperty()
    owner_key = ndb.KeyProperty(default=None)
    amount = ndb.FloatProperty()
    peach_unique_id = ndb.StringProperty()
    

    def to_dictionary(self):
        owner_name = ''
        owner_id = ''
        unit_name = ''
        unit_id = ''

        payment_dict = {}
        try:
            if self.owner_key:
                owner_name = self.owner_key.get().name
                owner_id = self.owner_key.id()

            if self.item_key:
                u = self.item_key.get()
                unit_name = u.unit_name
                unit_id = u.key.id()
        except:
            return payment_dict
            
        payment_dict.update({
            "unit_name" : unit_name,
            "unit_id" : unit_id,
            "amount" : self.amount,
            "created"  : self.created + datetime.timedelta(hours=2),
            "owner_id" : owner_id,
            "owner_name" : owner_name
            })
        return payment_dict



