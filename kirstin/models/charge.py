import os
import sys
import io
import csv
import logging

from google.appengine.ext import ndb
import datetime
import models.unit
import models.order
import models.system_settings
import models.user

CHARGE_TYPE = {
    'ONCE': 0,
    'MONTHLY': 1
}

CHARGE_TYPE_ONCE = 0
CHARGE_TYPE_MONTHLY = 1
ITEM_CATEGORY_DELIVERY = 0
ITEM_CATEGORY_UNIT = 1
ITEM_CATEGORY_ORDER = 2
ITEM_CATEGORY_LABOR = 3


class Quote(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    details = ndb.StringProperty()
    amount = ndb.FloatProperty()


class EFTPayment(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    owner_key = ndb.KeyProperty()
    charge_keys = ndb.KeyProperty(repeated=True)
    secure_eft_payment_key = ndb.StringProperty(required=True)

    @staticmethod
    def find_by_ref(code):
        eft = EFTPayment.query(EFTPayment.secure_eft_payment_key == code).get()
        return eft

    def to_dictionary(self):
        payment_dict = dict()
        details = ""
        eft_id = ""
        merchant_ref = ""
        for key in self.charge_keys:
            charge = key.get()
            eft_id = charge.peach_external_eft_id if charge.peach_external_eft_id else self.secure_eft_payment_key
            merchant_ref = charge.merchant_ref
            if charge.item_category == ITEM_CATEGORY_UNIT:
                details += " Unit charge of {} details {} |".format(charge.amount, charge.details)
            elif charge.item_category == ITEM_CATEGORY_DELIVERY:
                details += " Delivery charge of {} | details {}".format(charge.amount, charge.details)

            elif charge.item_category == ITEM_CATEGORY_ORDER:
                details += " Order charge of {} | details {}".format(charge.amount, charge.details)
            else:
                details += " Unknown charge"

        payment_dict.update({
            "owner_name": self.owner_key.get().name if self.owner_key else 'N/A',
            "total": sum([x.get().amount for x in self.charge_keys]),
            "charge_dicts": [x.get().to_dictionary() for x in self.charge_keys],
            "id": self.key.id(),
            "holder": 'N/A',
            "details": details,
            "payment_type": "EFT",
            "unique_id": eft_id,
            "merchant_ref": merchant_ref,
            "created": self.created + datetime.timedelta(hours=2) if self.created else 'N/A'

        })
        return payment_dict


class CreditCardPayment(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    owner_key = ndb.KeyProperty()
    card_holder = ndb.StringProperty(default=None)
    charge_keys = ndb.KeyProperty(repeated=True)
    peach_unique_id = ndb.StringProperty(required=True)

    @staticmethod
    def find_by_ref(code):
        cc = CreditCardPayment.query(CreditCardPayment.peach_unique_id == code).get()
        return cc

    def to_dictionary(self):
        payment_dict = dict()
        details = ""
        merchant_ref = ""

        for key in self.charge_keys:
            charge = key.get()
            merchant_ref = charge.merchant_ref

            if charge.item_category == ITEM_CATEGORY_UNIT:
                details += " Unit charge of {} details {} |".format(charge.amount, charge.details)
            elif charge.item_category == ITEM_CATEGORY_DELIVERY:
                details += " Delivery charge of {} | details {}".format(charge.amount, charge.details)
            elif charge.item_category == ITEM_CATEGORY_ORDER:
                details += " Order charge of {} | details {}".format(charge.amount, charge.details)
            else:
                details += "Unknown charge"

        payment_dict.update({
            "owner_name": self.owner_key.get().name if self.owner_key else 'N/A',
            "total": sum([x.get().amount for x in self.charge_keys]),
            "charge_dicts": [x.get().to_dictionary() for x in self.charge_keys],
            "id": self.key.id(),
            "details": details,
            "owner_id": self.owner_key.id(),
            "holder": self.card_holder,
            "merchant_ref": merchant_ref,
            "payment_type": "CreditCard",
            "unique_id": self.peach_unique_id,
            "created": self.created + datetime.timedelta(hours=2) if self.created else 'N/A'

        })
        return payment_dict


class Promo(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    code = ndb.StringProperty(required=True)
    archived = ndb.BooleanProperty(default=False)
    discount_percent = ndb.FloatProperty()

    @staticmethod
    def find_by_code(code):
        promo = Promo.query(Promo.code == code, Promo.archived == False).get()
        return promo



class Charge(ndb.Model):
    """NDB model class for a user's Charge.

    """

    created = ndb.DateTimeProperty(auto_now_add=True)
    item_key = ndb.KeyProperty(default=None)
    promo_key = ndb.KeyProperty()
    archived = ndb.BooleanProperty(default=False)
    owner_key = ndb.KeyProperty(default=None)
    merchant_ref = ndb.StringProperty()
    card_holder = ndb.StringProperty(default=None)
    internal_note = ndb.StringProperty()
    item_category = ndb.IntegerProperty()
    comment = ndb.StringProperty(default='')
    paid = ndb.BooleanProperty(default=False)
    amount = ndb.FloatProperty(default=0.0)
    charge_type = ndb.IntegerProperty(default=CHARGE_TYPE_MONTHLY)
    pay_date = ndb.DateTimeProperty(default=None)
    peach_unique_id = ndb.StringProperty(default=None)  # Used for credit card payments
    secure_eft_payment_key = ndb.StringProperty(default=None)  # Used for EFT payments
    term_end = ndb.DateTimeProperty()
    details = ndb.TextProperty()
    peach_external_eft_id = ndb.StringProperty(default='')

    def _post_put_hook(self, future):
        user = self.owner_key.get()
        # Update user unpaid charges and update calendar events to detect a change
        for ce in models.system_settings.CalendarEvent.query(
            models.system_settings.CalendarEvent.owner_key == user.key,
            models.system_settings.CalendarEvent.archived == False
        ).fetch():
            ce.updated = True
            ce.put()

        user.put()

    def to_dictionary(self):
        owner_name = ''
        owner_id = ''
        item_name = ''
        unit_id = ''
        term = ''
        pay_date = ''
        if self.pay_date:
            pay_date = self.pay_date + datetime.timedelta(hours=2)

        payment_dict = {}
        try:
            if self.owner_key:
                owner_name = self.owner_key.get().name
                owner_id = self.owner_key.id()

            if self.item_key:

                if self.item_category == ITEM_CATEGORY_UNIT:
                    u = self.item_key.get()

                    if self.charge_type == CHARGE_TYPE_ONCE:
                        term = "ONCE OFF"
                    else:
                        term = "MONTHLY"
                    paid_till = self.item_key.get().paid_till
                    item_name = u.unit_name
                    unit_id = u.key.id()

                elif self.item_category == ITEM_CATEGORY_DELIVERY:
                    item_name = 'Delivery'
                    term = 'ONCE'
                elif self.item_key.kind() == 'Order':
                    u = self.item_key.get()
                    item_name = 'Order'

                    unit_id = u.key.id()
                else:
                    item_name = 'Delivery'
                    term = 'ONCE'
            else:
                item_name = 'Unknown'


        except Exception as e:
            logging.error('Cant serialize charge {} {}'.format(self.key.id(), e))
            return None

        status = "UNPAID"
        if self.paid:
            status = "PAID"
        elif self.archived:
            status = "CANCELLED"

        payment_dict.update({
            "eft_id": self.peach_external_eft_id,
            "id": self.key.id(),
            "peach_eft_key": self.secure_eft_payment_key,
            "peach_id": self.peach_unique_id,
            "unit_name": item_name,
            "term": term,
            "comment": self.comment,
            "unit_id": unit_id,
            "pay_date": pay_date,
            "amount": self.amount,
            "created": self.created + datetime.timedelta(hours=2),
            "owner_id": owner_id,
            "owner_name": owner_name,
            "details": self.details,
            'merchant_ref': self.merchant_ref,
            "status": status,
            "cancelled":self.archived,
            "archived": 'CANCELLED AND ARCHIVED - NO PAYMENT NEEDED' if self.archived else 'NOT CANCELLED'
        })
        return payment_dict
