import os
import sys
import io
import csv

from google.appengine.ext import ndb
import datetime

ITEM_TYPE = {
    'ROPE' : 0,
    'LOCK' : 1
}
ITEM_HUMANIZE = {
     0 : 'ROPE',
     1 : 'LOCK'
}

class ItemCategory(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    name = ndb.StringProperty()
    description = ndb.StringProperty()
    archived = ndb.BooleanProperty(default=False)



    @classmethod
    def get_by_name(cls, name):
        if name:
            return cls.query(cls.name == name).get()
        else:
            return None


class Item(ndb.Model):

    created = ndb.DateTimeProperty(auto_now_add=True)
    name = ndb.StringProperty()
    archived = ndb.BooleanProperty(default=False)
    category = ndb.KeyProperty()
    usage = ndb.StringProperty(default='')
    price = ndb.FloatProperty()
    image_url = ndb.StringProperty()
    #image_urls = ndb.StringProperty(repeated=True)
    description = ndb.TextProperty()
    quantity = ndb.IntegerProperty(default=0)
    sold_count = ndb.IntegerProperty(default=0)
    shipped_count = ndb.IntegerProperty(default=0)

    def to_dictionary(self):

            item_dict = {
                'name': self.name,
                'sold_count': self.sold_count,
                'shipped_count': self.shipped_count,
                'price': "{:0.2f}".format(round(self.price, 2)),
                'id': self.key.id(),
                'ideal_for': self.usage,
                "image_url": self.image_url,
                "description": self.description,
                "quantity": self.quantity,
                "archived": ["NOT ARCHIVED", "ARCHIVED - NOT FOR SALE"][self.archived]
            }
            return item_dict

    def to_dictionary_cart(self):

            item_dict = {

                'name' : self.name,
                'price' : self.price
            }
            return item_dict