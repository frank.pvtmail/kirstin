#!/bin/bash
osascript -e 'tell app "Terminal"
   do script "cd dev/podit; yes | gcloud app deploy app.yaml --project=podit-staging --version=1-0-0; exit"
   do script "cd dev/podit; yes | gcloud app deploy admin.yaml --project=podit-staging --version=1-0-0; exit"
   do script "cd dev/podit; yes | gcloud app deploy client.yaml --project=podit-staging --version=1-0-0;  exit"
   do script "cd dev/podit; yes | gcloud app deploy cronworker.yaml --project=podit-staging --version=1-0-0; printf '\7';  exit"
end tell'

#yes | gcloud app deploy reporter.yaml app.yaml client.yaml cron.yaml index.yaml public_api.yaml services.yaml admin.yaml --project=wum-staging --version=test