import ast
import calendar
import datetime
import json
import logging
import uuid
from functools import wraps
import handlers.client_request_handler
import handlers.admin_request_handler

import flask_cors
import google.auth.transport.requests
import google.oauth2.id_token
import requests
import requests_toolbelt.adapters.appengine
from flask import Flask, jsonify, request
from google.appengine.api import search
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext import deferred
from google.appengine.ext import ndb
from validate_email import validate_email

import lib.cloudstorage as gcs
import utils.geo as geo
from models.address import Address
from models.charge import Charge, CHARGE_TYPE
from models.item import Item, ItemCategory
from models.order import Order
import models.shopping_cart
from models.system_settings import SystemSettings, CalendarEvent, BaseStation
from models.unit import Unit, STATUS_TO_TEXT, POD_SIZE_HUMANIZE, POD_SIZE, AT_FACILITY, PENDING_DROPOFF, CANCELLED, \
    POD_SIZE_DESCR
from models.user import User
import services.exporter
from services.sender import client as communicator
import models.log

import utils.time_utils as time_utils

PROJECT_ID = 'podit-169717'
SID_MERCHANT = "TESTPODIT"
SID_SECRET_KEY = "ZVPCWNmJWfDqQGl7yhQKmQE5b2x0WSGy0qwqXsaAXA7GwGjlmnvn7tWIR"
SID_CURRENCY = "ZAR"
SID_COUNTRY = "ZA"

# Use the App Engine Requests adapter. This makes sure that Requests uses
# URLFetch.
requests_toolbelt.adapters.appengine.monkeypatch()
HTTP_REQUEST = google.auth.transport.requests.Request()

import google.oauth2.credentials

SCOPES = ['https://www.googleapis.com/auth/calendar']
API_SERVICE_NAME = 'calendar'
API_VERSION = 'v3'
CLIENT_SECRETS_FILE = "client_secret.json"

# authentication.userId = ff808081392eb9b201392f0b6d0200a3
# authentication.password = wCJFfx6F

# authentication.entityId = Please use one of the channel IDs below for this parameter depending on the request type (i.e. once-off or recurring)

# Once-off / 3DSecure Channel ID: ff808081407bfe10014087a5dcd918ea
# Recurring Channel ID: ff808081392eb9b201392f0bfe3800a9
from models.const import *

app = Flask(__name__)
app.secret_key = '38uhsdfuh838uhsdfuh388hdjh88282'
flask_cors.CORS(app)


def is_authorized(request):
    id_token = request.headers['Authorization'].split(' ').pop()
    claims = google.oauth2.id_token.verify_firebase_token(
        id_token, HTTP_REQUEST)
    return claims


UNIT_TYPES = {
    "POD": 0,
    "SS": 1
}

TYPES_HUMANIZE = {
    0: "POD",
    1: "SS"
}


def admin_authorized(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        id_token = request.headers['Authorization'].split(' ').pop()
        firebase_token = google.oauth2.id_token.verify_firebase_token(
            id_token, HTTP_REQUEST)
        current_user = User.query(User.firebase_id == firebase_token['sub']).get()
        session_token = {"firebase_token": firebase_token, "current_admin": current_user}
        if not firebase_token:
            return 'Unauthorized', 401

        if not current_user.is_admin:
            return 'Unauthorized', 401

        return f(session_token, *args, **kwargs)

    return decorated_function


def authorized(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        id_token = request.headers['Authorization'].split(' ').pop()
        firebase_token = google.oauth2.id_token.verify_firebase_token(
            id_token, HTTP_REQUEST)
        current_user = User.query(User.firebase_id == firebase_token['sub']).get()
        session_token = {"firebase_token": firebase_token, "current_user": current_user}
        if not firebase_token:
            return 'Unauthorized', 401

        return f(session_token, *args, **kwargs)

    return decorated_function


@app.route('/api/v1/current_users/payments', methods=['GET'])
@authorized
def prepare_checkout_outstanding(session_token):
    user = session_token.get('current_user')
    surname = ''
    firstname = ''
    street1 = ""
    address_obj = user.address

    if address_obj:
        for part in address_obj.full_name.split(" ")[:3]:
            street1 += part.strip(",") + " "

    if user.name:
        firstname = user.name.split(" ")[0]
        for part in user.name.split(" ")[1:]:
            surname += part + " "
    amount = request.args.get('a', None)
    if not amount:
        return jsonify(dict(success=False, message="No amount to charge"))

    amount = float(amount)

    data = [
        ('authentication.userId', PEACH_LIVE_USER_ID),
        ('authentication.password', PEACH_LIVE_PASSWORD),
        ('authentication.entityId', INITIAL_CHANNEL_ID_LIVE),
        ('amount', "{:.2f}".format(amount)),
        ('currency', 'ZAR'),
        ('paymentType', 'DB'),
        ('recurringType', 'INITIAL'),
        ('createRegistration', 'true'),
        ('merchantTransactionId', request.args.get('merch_ref', 'NOT PROVIDED')),
        ('customer.givenName', firstname),
        ('customer.surname', surname),
        ('customer.email', user.email),
        ('shipping.street1', street1),
        ('shipping.city', "Stellenbosch"),
        ('shipping.country', 'ZA')

    ]

    print 'sending data to peach: {}'.format(data)

    response = requests.post('https://oppwa.com/v1/checkouts', data=data)
    json_data = json.loads(response.text)
    print response
    print json_data
    return jsonify({"payment": ast.literal_eval(response.text), "user": user.to_dictionary()})


@app.route('/api/v1/admins/privileges', methods=['POST'])
@admin_authorized
def admin_user_set(session_token):
    return handlers.admin_request_handler.set_as_admin(session_token)


@app.route('/api/v1/admins/exporter', methods=['GET'])
@admin_authorized
def exporter(session_token):
    print session_token
    return handlers.admin_request_handler.export_entities(session_token)


@app.route('/api/v1/admins/stats')
def stats():
    return handlers.admin_request_handler.fetch_stats()


@app.route('/api/v1/admins/units', methods=['GET', 'POST'])
@admin_authorized
def admin_unit_handling(session_token):
    if request.method == 'GET':

        if request.args.get('search', None):
            return handlers.admin_request_handler.unit_search(request.args.get('search', None))

        filters = dict()

        for k, v in request.args.iteritems():
            if 'all' in request.args.keys():
                break
            if v == 'true':
                v = True
            if v == 'false':
                v = False
            if k == 'unit_size':
                v = POD_SIZE.get(v)

            if k != 'p':
                filters.update({k: v})

        return handlers.admin_request_handler.units_by_filter(filters)

    elif request.method == 'POST':
        data = request.get_json()
        return handlers.admin_request_handler.add_units(data)
    else:
        return 'Unknown request method'


@app.route('/api/v1/admins/users')
@admin_authorized
def admin_user_handling(session_token):
    search_term = request.args.get('search', None)
    user_id = request.args.get('user_id', None)
    unpaid = request.args.get('unpaid', None)
    archived = request.args.get('archived', None)
    recently_online = request.args.get('recent', None)

    if recently_online:
        return handlers.admin_request_handler.recently_online_users()

    if unpaid:
        return handlers.admin_request_handler.unpaid_users()

    if user_id:
        return handlers.admin_request_handler.fetch_user_info(session_token)

    if search_term:
        return handlers.admin_request_handler.user_search(search_term)

    return jsonify({'next_page': 'None', 'users': []})


@app.route('/api/v1/admins/units/prices', methods=['GET', 'POST'])
@admin_authorized
def admin_unit_prices(session_token):
    if request.method == 'POST':
        return handlers.admin_request_handler.set_unit_prices(session_token)
    else:
        return handlers.admin_request_handler.fetch_unit_prices(session_token)


@app.route('/api/v1/admins/units/sizes', methods=['POST', 'GET'])
def get_unit_sizes():
    return handlers.admin_request_handler.fetch_unit_sizes()


@app.route('/a/categories', methods=['GET', 'POST'])
def admin_get_categories():
    if request.method == 'POST':
        # add new category
        data = request.get_json()
        name = data.get('name')
        if ItemCategory.get_by_name(name):
            return jsonify({"status": "error", "message": "Item category already exists"})
        description = data.get('description')
        item_category = ItemCategory()
        item_category.name = name
        item_category.description = description
        item_category.put()
        return jsonify({"status": "success", "message": "Successfully added category"})

    categories = ItemCategory.query(ItemCategory.archived == False).fetch()
    category_list = [x.name for x in categories]
    return jsonify({"categories": category_list})


@app.route('/a/items', methods=['POST', 'GET', 'PUT', 'DELETE'])
def admin_manage_items():
    if request.method == 'DELETE':
        return jsonify({"status": "success", "message": "Successfully DELETED item"})

    if request.method == 'PUT':
        data = request.form.to_dict()
        item_id = request.args.get('item_id')
        item = Item.get_by_id(int(item_id))
        image_file = request.files.get('item_picture', None)

        if image_file:
            file_name = '/podit-169717.appspot.com/item-pictures/{}'.format(uuid.uuid1())
            write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                    options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                             }, retry_params=None)
            write_buffer.write(image_file.stream.read())
            write_buffer.close()
            item.image_url = file_name
            #item.image_urls.append(file_name)

        item.name = data.get('name')
        item.usage = data.get('usage')
        item.description = data.get('description')
        item.category = ItemCategory.query(ItemCategory.name == data.get('category')).fetch(1)[0].key
        item.price = float(data.get('price'))
        item.put()
        return jsonify({"status": "success", "message": "Successfully edited item"})

    if request.method == 'POST':
        # add new item with category

        data = request.form.to_dict()
        image_file = request.files.get('item_picture', None)

        return handlers.admin_request_handler.add_item(data, image_file)

    category = request.args.get('item_category', None)
    if not category:
        return jsonify({"status": "fail", "message": "No category in URL args"})
    query = ItemCategory.query(ItemCategory.name == category)
    if not query.count():
        return jsonify({"status": "fail", "message": "No category found for type {}".format(category)})

    items = Item.query(Item.category == query.fetch(1)[0].key).fetch()
    item_dict = [x.to_dictionary() for x in items]

    return jsonify({"items": item_dict, "item_type": "{}s".format(category.lower().title())})


# ENDPOINTS FOR USERS

@app.route('/api/v1/current_users', methods=['POST', 'GET', 'PUT'])
@authorized
def create_user(session_token):
    if request.method == 'GET':
        return handlers.client_request_handler.fetch_user_details(session_token)
    elif request.method == 'POST':
        return handlers.client_request_handler.verify_user(session_token, request.args.get('admin', None))
    else:
        return handlers.client_request_handler.edit_user(session_token)


@app.route('/api/v1/current_users/geo-code-address')
@authorized
def geo_code_address(session_token):
    address = request.args.get('address', None)
    coordinates = geo.get_coordinates(str(address))
    is_within_radius = geo.is_within_circle_from_hub(coordinates[0], coordinates[1])
    if coordinates is None:
        return jsonify(
            {"success": False, "message": "Address not found, please provide the Street, Suburb, City and Province"})

    if not is_within_radius:
        return jsonify(
            {"success": False, "message": "Address not within 20KM from PODit HQ"})

    full_name = geo.get_full_address(str(address))
    return jsonify({"success": True, "message": "Successfully geo-coded address!", "full_name": full_name})


# ENDPOINTS REGARDING UNITS
# ENDPOINTS REGARDING UNITS
# ENDPOINTS REGARDING UNITS

@app.route('/api/v1/current_users/units', methods=['GET', 'POST', 'PUT'])
@authorized
def get_units(session_token):
    if request.method == 'GET':
        return handlers.client_request_handler.get_current_user_units(session_token)
    elif request.method == 'POST':
        return handlers.client_request_handler.reserve_unit(session_token)
    else:
        return handlers.client_request_handler.edit_unit(session_token)


@app.route('/api/v1/current_users/orders', methods=['POST'])
@authorized
def create_order(session_token):
    return handlers.client_request_handler.create_order(session_token)


@app.route('/api/v1/current_users/orders/<int:order_id>', methods=['DELETE', 'POST'])
@authorized
def cancel_order(session_token, order_id):
    if request.method == "DELETE":
        return handlers.client_request_handler.cancel_order(int(order_id))
    else:
        return jsonify(dict(sucess=False, message="Unsupported"))


@app.route('/api/v1/current_users/units/cancelations', methods=['POST'])
@authorized
def cancel_rent(session_token):
    return handlers.client_request_handler.cancel_rent_handler(session_token)


@app.route('/api/v1/current_users/available-units', methods=['GET'])
@authorized
def get_order_information(session_token):
    base_station = BaseStation.find_by_name((request.args.get('base_station')))

    unit_information = []
    for k, v in POD_SIZE_DESCR.iteritems():
        params = [ndb.AND(Unit.unit_size == v[1],
                          Unit.available == True,
                          Unit.archived == False)]
        if base_station:
            params.append(ndb.AND(Unit.base_station_key == base_station.key))

        available_count = Unit.query(*params).count()
        unit_information.append({"description": k, "size_text": v[0], "available": available_count})

    return jsonify({"unit_information": unit_information,
                    "address": session_token.get('current_user').address.full_name if session_token.get(
                        'current_user').address else '',
                    'cell_no': session_token.get('current_user').cell_no if session_token.get(
                        'current_user').cell_no else ''})


@app.route('/api/v1/current_users/units/quotes', methods=['GET'])
@authorized
def quote_unit(session_token):
    return handlers.client_request_handler.unit_quote(session_token)


@app.route('/api/v1/current_users/units/validations', methods=['POST'])
@authorized
def validate_pod(session_token):
    return handlers.client_request_handler.validate_unit_reservation_data(session_token)


@app.route('/api/v1/current_users/units/delivery-validations', methods=['POST'])
@authorized
def delivery_validation(session_token):
    return handlers.client_request_handler.delivery_data_validation(session_token)

# ENDPOINTS REGARDING ITEMS
# ENDPOINTS REGARDING ITEMS
# ENDPOINTS REGARDING ITEMS

@app.route('/api/v1/current_users/items', methods=['POST', 'GET', 'PUT'])
@authorized
def client_view_items_per_category(session_token):
    return handlers.client_request_handler.get_items_per_category()


@app.route('/api/v1/current_users/shopping-carts/items/<int:item_id>/counts/<int:count>', methods=['GET'])
@authorized
def update_item_counts(session_token, item_id, count):
    return handlers.client_request_handler.update_item_count(session_token, item_id, count)


@app.route('/api/v1/current_users/shopping-carts/items', methods=['GET'])
@authorized
def get_cart_items(session_token):
    return handlers.client_request_handler.inspect_client_cart(session_token)


@app.route('/api/v1/current_users/shopping-carts/items/<int:item_id>', methods=['POST', 'DELETE'])
@authorized
def client_cart_management(session_token, item_id):
    if request.method == 'DELETE':
        return handlers.client_request_handler.remove_from_cart(item_id, session_token)
    else:
        return handlers.client_request_handler.add_to_cart(item_id, session_token)


@app.errorhandler(500)
def server_error(e):
    # Log the error and stacktrace.
    logging.exception('An error occurred during a request.')
    return 'An internal error occurred.', 500

# [END app]
