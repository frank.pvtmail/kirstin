#!flask/bin/python

import logging
import traceback

from flask import Flask, request, make_response, jsonify, current_app, g

app = Flask('podit_internal_services')


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'status': 'error', 'message': 'Not found'}), 404)

