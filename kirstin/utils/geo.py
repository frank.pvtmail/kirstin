import importlib
importlib.import_module('math')  # For some reason, just import math was failing on local (dev) server.
import logging
from google.appengine.ext import ndb  # Google Datastore
import urllib
import math
import requests
class GeoPostCode2(ndb.Model):
    iso = ndb.StringProperty()
    country = ndb.StringProperty(default='ZA')
    gpc_id = ndb.StringProperty()
    region1 = ndb.StringProperty()
    region2 = ndb.StringProperty()
    region3 = ndb.StringProperty()
    region4 = ndb.StringProperty()
    locality = ndb.StringProperty()
    postcode = ndb.StringProperty()
    suburb = ndb.StringProperty()
    coords = ndb.GeoPtProperty()
    elevation = ndb.FloatProperty()
    hasc = ndb.StringProperty()
    is_covered = ndb.BooleanProperty(default=False)

geocoder = 'https://maps.googleapis.com/maps/api/geocode/json?'
matrix = 'https://maps.googleapis.com/maps/api/distancematrix/json?'
config = '&mode=driving&language=en-US&key='
key = 'AIzaSyDfYp_oEk-zsXsY81506Tw2M8ns5qKc1TY'

departments = ['CPT', 'JHB', 'DBN']

def str_latlng(latlng):
    """
    performs basic conversion.
    :param latlng:
    :return str(latlng):
    """
    return "latlng={}&key=".format(str(latlng))


def str_address(location):
    return "address={}&key=".format(urllib.quote_plus(location.encode('utf-8')))


def location_url(a, b):
    """
    forms the location part of the URL used in the get_estimate() function.
    :param a:
    :param b:
    :return:
    """
    if a and b:
        url = 'origins=' + str(a) + '&destinations=' + str(b) + config + key
    else:
        url = None
    return url


def get_matrix_response(a, b):
    """
    uses the Google Matrix API to query several details related to travelling from a to b
    :param a:
    :param b:
    :return:
    """
    url = matrix + location_url(a, b)
    r = requests.get(url)
    logging.info(url)
    logging.info("DistMatResponse -> " + str(r.json()))
    return r.json()


def status_OK(data):
    return True  # data['rows']['status'] == 'OK' #TODO: Status codes are set per element.


def get_geocoder_response(location, is_address=False):
    """
    uses the Google Geocoder API to query the address of the given latlng
    :param latlng:
    :return:
    """
    url = geocoder + str_latlng(location) + key if not is_address else geocoder + str_address(location) + key
    r = requests.get(url)
    return r.json()


# -------------------------Outward Facing Methods----------------------------------


def get_time(a, b):
    """
    returns the time it would take to drive from point a to b
    :param a:
    :param b:
    :return dur in sec:
    """
    data = get_matrix_response(a, b)
    if data and status_OK(data):
        return data['rows'][0]['elements'][0]['duration']['value']
    else:
        return None


def get_distance(a, b):
    """
    returns the driving distance between points a and b
    :param a:
    :param b:
    :return dist in m:
    """
    data = get_matrix_response(a, b)
    if data and status_OK(data):
        return data['rows'][0]['elements'][0]['distance']['value']
    else:
        return None


def get_address(latlng):
    """
    returns the approximate address which corresponds to the coordinates being passed in.
    :param latlng:
    :return:
    """
    data = get_geocoder_response(latlng)
    if data and status_OK(data):
        return data['results'][0]['formatted_address']
    else:
        return None


def get_full_address(address):
    data = get_geocoder_response(address, is_address=True)
    if data and status_OK(data):
        logging.debug(str(data))
        try:
            result = data['results'][0]['formatted_address']
            return result
        except:
            pass
    else:
        return None


def get_coordinates(address):
    """
    returns the approximate address which corresponds to the coordinate baing passed in.
    :param latlng:
    :return:
    """
    data = get_geocoder_response(address, is_address=True)
    if data and status_OK(data):
        logging.debug(str(data))
        try:
            result = data['results'][0]['geometry']['location']
            return result['lat'],result['lng']
        except Exception as e:
            logging.info('Failed to geo code {}'.format(e))
            return None
    else:
        return None


def get_time_and_distance(a, b):
    """
    serves to save up on api calls, replacing the above get_time and get_distance methods.
    :param a:
    :param b:
    :return dur in sec. and dist in m.:
    """
    try:
        data = get_matrix_response(a, b)
        logging.debug(data)
        if data and status_OK(data):
            try:
                dur = data['rows'][0]['elements'][0]['duration']['value']
                dist = data['rows'][0]['elements'][0]['distance']['value']
                return [dur, dist]
            except:
                logging.debug('Request failed. Data: ' + str(data))
                return None
        else:
            raise ValueError('Unsuccessful request.')
    except Exception as e:
        logging.error(str(e))
        return None


def deg2rad(deg):
    return deg * (math.pi / 180)


def is_within_circle_from_hub(lat, lng):

    lat1, lon1 = -33.930343, 18.880652
    _fr = 100

    lat2 = float(lat)
    lon2 = float(lng)
    _r = 6371
    _d_lat = deg2rad(lat2 - lat1)
    _d_lon = deg2rad(lon2 - lon1)
    a = (math.sin(_d_lat / 2) * math.sin(_d_lat / 2) +
         math.cos(deg2rad(lat1)) * math.cos(deg2rad(lat2)) *
         math.sin(_d_lon / 2) * math.sin(_d_lon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = _r * c
    return d < _fr


def is_within_circle_custom(lat, lng, city, custom_radius=30):
    if city == 'CPT':
        lat1 = conf.CPT.ring_fence_lat
        lon1 = conf.CPT.ring_fence_lon
        _fr = custom_radius
    elif city == 'DBN':
        lat1 = conf.DBN.ring_fence_lat
        lon1 = conf.DBN.ring_fence_lon
        _fr = conf.DBN.ring_fence_radius
    elif city == 'JHB':
        lat1 = conf.JHB.ring_fence_lat
        lon1 = conf.JHB.ring_fence_lon
        _fr = custom_radius
    else:
        city = get_city_from_coords(lat, lng)
        if city is not None:
            return is_within_circle(lat, lng, city)
        else:
            return None
    lat2 = float(lat)
    lon2 = float(lng)
    _r = 6371
    _d_lat = deg2rad(lat2 - lat1)
    _d_lon = deg2rad(lon2 - lon1)
    a = (math.sin(_d_lat / 2) * math.sin(_d_lat / 2) +
         math.cos(deg2rad(lat1)) * math.cos(deg2rad(lat2)) *
         math.sin(_d_lon / 2) * math.sin(_d_lon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = _r * c
    return d < _fr


def calculate_delivery_price(distance, round_trip=False):
    flat_price = conf.BASE_PRICE

    price = (float(distance) / 1000) * 12

    if price > flat_price:
        pass
    else:
        price = flat_price

    price = round(price if not round_trip else (price * 2), 2)
    # logging.info("Price calculation -> " + str(price) + " ("+str(distance)+"m)" + (" round" if round_trip else ""))

    return price


def get_city(address):
    data = get_geocoder_response(address, is_address=True)
    if data and status_OK(data):
        # result = data['results'][0]['address_components'][4]['short_name']
        address_components = data.get('results')[0].get('address_components')
        for component in address_components:
            if 'locality' in component.get('types'):
                result = component.get('short_name')
                break
            else:
                result = None
        coords = data.get('results')[0].get('geometry').get('location')
        lat, lng = coords.get('lat'), coords.get('lng')
        try:
            for department in departments:
                if is_within_circle(lat, lng, department):
                    return department
            logging.debug("Passive city mismatch. Got: {}".format(result))
            return None
        except:
            return None


def get_city_from_coords(lat, lng):
    try:
        for department in departments:
            if is_within_circle(lat, lng, department):
                return department
        return None
    except:
        return None


def str_to_ndb_geopt(lat, lng=None):
    """
    Helper method.
    :param string:
    :return GeoPt:
    """
    if lng is not None:
        return ndb.GeoPt(lat, lng)
    else:
        latlng = lat
        return ndb.GeoPt(latlng)


def get_delivery_last_known_location(delivery, format=0):
    """
    `format` determines whether this function should return a string or a GeoPtProperty.
    Should be equal to pickup coords before pickup/upon cancellation;
    Should be equal to dropoff coords after dropoff;
    Should be equal to last known driver location while in transit.
    :param delivery:
    :return:
    """
    try:
        assert isinstance(delivery, Delivery)
        if delivery.last_known_location:
            # If this order does have a "last known location" set up, just use that.
            return str(delivery.last_known_location) if not format else delivery.last_known_location
        status = delivery.get_status()
        if status == "PENDING DROPOFF":
            driver = Driver.find_by_api_key(delivery.claimed_by)
            if driver is not None:
                loc = driver.last_known_location if driver.last_known_location is not None else str(
                    delivery.coords_from)
                return (
                    str(loc) or str(delivery.coords_from)
                ) if not format else (
                    loc or delivery.coords_from
                )
            return None  # Means there's been an error
        elif status == "DELIVERED":
            return str(delivery.coords_to) if not format else delivery.coords_to
        return str(delivery.coords_from) if not format else delivery.coords_from
    except AssertionError:
        return None

def str_to_geopt(lat, lng=None):
    return ndb.GeoPt(str(lat), str(lng)) if lng else ndb.GeoPt(str(lat))
