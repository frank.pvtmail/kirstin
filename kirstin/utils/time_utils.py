
import datetime

def to_zar_time(gmt_datetime):
    if gmt_datetime:
        return gmt_datetime + datetime.timedelta(hours=2)
    else:
        return None

def za_time_now():
    return datetime.datetime.now() + datetime.timedelta(hours=2)

def get_first_of_next_month_given_date(d):
    """
    This function returns the first of the next month from the current time and date.
    :return:
    """

    next_month = d.month + 1
    next_year = d.year
    if d.month == 12:
        next_month = 1
        next_year = d.year + 1
    if next_month < 10:
        next_month = "0" + str(next_month)

    return datetime.datetime.strptime("01/"+str(next_month)+"/"+str(next_year), "%d/%m/%Y")


def get_first_of_next_month():
    """
    This function returns the first of the next month from the current time and date.
    :return:
    """
    d = datetime.date.today()
    next_month = d.month + 1
    next_year = d.year
    if d.month == 12:
        next_month = 1
        next_year = d.year + 1
    if next_month < 10:
        next_month = "0" + str(next_month)

    return datetime.datetime.strptime("01/"+str(next_month)+"/"+str(next_year), "%d/%m/%Y") 
