import service
import time
import logging

class DriverUpdates:
    @classmethod
    def _post_driver_tasks(cls, driver_obj, task_list):
        """
        Internal Method.

        Provided an instance of class Driver and a corresponding list of tasks, makes the call to Firebase servers
            to update the real-time database.

        :param driver_obj:
        :param task_list:
        :return:
        """
        time.sleep(1)
        serialized_tasks = [x.to_dictionary() for x in sorted(task_list)]
        logging.debug('Putting tasks: ' + str(serialized_tasks))
        service.fb.put('/driver-tasks', driver_obj.api_key, serialized_tasks)

    @classmethod
    def post_tasks_by_driver_id(cls, driver_id=None):
        """
        Allows to update the firebase task listing from just a list of assigned tasks.

        Takes a driver_id and queries the corresponding tasks automatically.

        :param driver_id:
        :return:
        """
        from models.driver import Driver
        from models.task import Task
        if driver_id:
            driver_obj = Driver.get_by_id(driver_id)
        else:
            raise ValueError('A driver_id must be provided.')
        if not driver_obj:
            raise ValueError('Driver not found.')
        task_list = Task.query_for_driver_app_task_list(driver_id)
        cls._post_driver_tasks(driver_obj=driver_obj, task_list=task_list)

    @classmethod
    def post_tasks_by_driver_api_key(cls, driver_api_key=None):
        """
        Allows to update the firebase task listing from just a list of assigned tasks.

        Takes a driver_api_key and queries the corresponding tasks automatically.

        :param driver_api_key:
        :return:
        """
        from models.driver import Driver
        from models.task import Task
        if driver_api_key:
            driver_obj = Driver.find_by_api_key(driver_api_key)
        else:
            raise ValueError('A driver_api_key must be provided.')
        if not driver_obj:
            raise ValueError('Driver not found.')
        task_list = Task.query_for_driver_app_task_list(driver_obj.key.id())
        cls._post_driver_tasks(driver_obj=driver_obj, task_list=task_list)

    @classmethod
    def post_task_list(cls, task_list=None):
        """
        Allows to update the firebase task listing from just a list of assigned tasks.

        Assumes the assignee is the same for all tasks and posts all tasks to the driver assigned to the task at index zero.
        :param task_list:
        :return:
        """
        from models.driver import Driver
        if task_list:
            driver_id = task_list[0].assignee_id
            driver_obj = Driver.get_by_id(driver_id)
        else:
            raise ValueError('Empty or null task_list provided.')
        if not driver_obj:
            raise ValueError('Incorrect driver ID provided, driver not found.')
        serialized_tasks = [x.to_dictionary() for x in sorted(task_list)]
        service.fb.put('/driver-tasks', driver_obj.api_key, serialized_tasks)
