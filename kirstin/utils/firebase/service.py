from lib.firebase import firebase
from google.appengine.api import memcache
from utils.geo import get_time
import logging

ETA_PADDING = 10
FIREBASE_SECRET = '1wg214niGHaketkkEEtHoD7iHkwJGXZsVS0Tm7t0'
SERVICE_ACCOUNT = 'wumdropcoreserver@wumdrop.iam.gserviceaccount.com'

fb = firebase.FirebaseApplication('https://wumdrop.firebaseio.com', None)

authentication = firebase.FirebaseAuthentication(FIREBASE_SECRET, SERVICE_ACCOUNT)
fb.authentication = authentication


def get_eta(delivery, driver):
    if not delivery or not driver:
        return None
    if delivery.is_finalized():
        return None
    next_coords = delivery.coords_from
    if delivery.time_picked_up:
        next_coords = delivery.coords_to
    eta = memcache.get('{}-{}-{}-eta'.format(
            delivery.key.id(),
            driver.key.id(),
            delivery.status))
    if eta:
        logging.info("WIN! Saved some juice by getting this stuff from memcache!")
    else:
        try:
            eta = get_time(
                '{}, {}'.format(
                    driver.last_known_location.lat,
                    driver.last_known_location.lon),
                '{}, {}'.format(
                    next_coords.lat,
                    next_coords.lon)) / 60
        except:
            eta = None

        memcache.add('{}-{}-{}-eta'.format(
            delivery.key.id(),
            driver.key.id(),
            delivery.status), eta, 60 * 3)
    return eta


def serialize_trip_data(delivery_obj, driver_obj=None):
    """
    :type delivery_obj: models.delivery.Delivery
    :type driver_obj: models.driver.Driver
    :param delivery_obj:
    :param driver_obj:
    :return:
    """

    def _get_color_code(status):
        statuses = {
            "PENDING DROPOFF": 'orange',
            "DELIVERED": 'teal',
            "PENDING PICKUP": 'yellow',
            "CANCELLED": 'red'
        }

        return statuses.get(status, 'grey')

    if not delivery_obj:
        return None
    return {
            'order': {
                'status': delivery_obj.get_status(),
                'color_code': _get_color_code(delivery_obj.get_status()),
                'location': {
                          'lat': delivery_obj.last_known_location.lat,
                          'lon': delivery_obj.last_known_location.lon
                } if delivery_obj.last_known_location else None,
            },
            'driver': {
                'name': driver_obj.firstname,
                'license_plate': driver_obj.license_plate,
                'eta': get_eta(delivery_obj, driver_obj) if driver_obj.last_known_location else None,
            } if driver_obj else None,
        }


def post_delivery_update(delivery_obj, driver_obj=None):
    logging.info('Posting update to firebase for: DELIVERY {}, DRIVER {}'.format(
        delivery_obj.key.id(), driver_obj.key.id() if driver_obj else None))
    if not delivery_obj:
        raise ValueError('Delivery cannot be None.')
    fb.put('/trips', delivery_obj.tracking_code, serialize_trip_data(delivery_obj, driver_obj))
