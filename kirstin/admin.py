import os
import sys
import io
import csv
import logging
import uuid
import datetime
import tablib
import requests
from flask import Flask, redirect, render_template, json, request, jsonify, abort, make_response, flash
# noinspection PyUnresolvedReferences
from flask_login import (
    LoginManager,
    logout_user,
    login_user,
    current_user,
    login_required
)

app = Flask(__name__)
app.secret_key = '38uhsdfuh838uhsdfuh388hdjh88282'
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.api import search
import models.unit
import models.charge
import models.user
import models.order
import models.log
import models.customer_communications
import models.item
import models.shopping_cart
from models.item import ItemCategory, Item
from models.system_settings import SystemSettings, OAuthCredentials, CalendarEvent, BaseStation
from functools import wraps

import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery
import services.unit_controller
import services.user_orchestrator
import services.sender
import services.bill_collector
import services.seven_eleven
import models.blog_post
import lib.cloudstorage as gcs
from google.auth import app_engine

SYSTEM_SETTINGS = SystemSettings.fetch()

from lib.requests_toolbelt.adapters import appengine

appengine.monkeypatch()


def is_admin_check(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):

        try:
            b = current_user
        except:
            b = None

        if b and not b.is_admin:
            flash('Please invalid, user not admin')
            return redirect('/')

        return f(*args, **kwargs)

    return decorated_function


@login_manager.user_loader
def load_user(user_id):
    return models.user.User.get_by_id(int(user_id))


SCOPES = ['https://www.googleapis.com/auth/calendar']
API_SERVICE_NAME = 'calendar'
API_VERSION = 'v3'
CLIENT_SECRETS_FILE = "client_secret.json"

POD_SIZES = {
    "XXS": 0,
    "XS": 1,
    "S": 2,
    "M": 3,
    "L": 4,
    "XL": 5
}

POD_SIZE_HUMANIZE = {
    0: "XXS",
    1: "XS",
    2: "S",
    3: "M",
    4: "L",
    5: "XL"
}


def credentials_to_dict(credentials):
    oauth_cred = OAuthCredentials()
    oauth_cred.token = credentials.token
    oauth_cred.token_uri = credentials.token_uri
    oauth_cred.refresh_token = credentials.refresh_token
    oauth_cred.client_id = credentials.client_id
    oauth_cred.client_secret = credentials.client_secret
    oauth_cred.scopes = credentials.scopes
    SYSTEM_SETTINGS.oauth_credentials = oauth_cred
    SYSTEM_SETTINGS.put()

    return True


@app.context_processor
def inject_dict_for_all_templates():
    _dict = dict()
    _dict.update(js_version=str(uuid.uuid4())[:5])
    #_dict.update(js_version="123")
    return _dict


@app.errorhandler(500)
def internal_error(e):
    logging.error(str(e))
    return redirect('/')


@app.route("/logout")
def logout():
    logout_user()
    # flash('If you have unpaid charges you will not be able to manage your Units', 'info')
    return jsonify({"success": True, "message": "Log out successful"})


@app.route("/login/<int:user_id>")
def login(user_id):
    if not current_user.is_authenticated:
        user = models.user.User.get_by_id(user_id)
        login_user(user)
    # flash('If you have unpaid charges you will not be able to manage your Units', 'info')
    return jsonify({"success": True, "message": "Log in successful"})


@app.route("/complete-event/<int:event_id>")
@is_admin_check
@login_required
def complete_event(event_id):
    event = CalendarEvent.get_by_id(event_id)
    event.status = 'COMPLETED'
    event.put()
    #        choices=['PICKUP', 'DROPOFF', 'DROPOFF_EVAC', 'PICKUP_EVAC', 'CLIENT_VISIT', 'CLIENT_VISIT_EVAC'])

    for key in event.item_keys:
        if key and key.get():
            if key.kind() == 'Unit':
                unit = key.get()
                if event.category == 'DROPOFF':
                    unit.status = models.unit.PENDING_PICKUP
                elif event.category == 'PICKUP' or event.category == "CLIENT_VISIT":
                    unit.status = models.unit.AT_FACILITY
                elif event.category == 'PICKUP_EVAC':
                    # Empty POD is collected from client and ready for another clients use
                    unit.status = models.unit.AT_FACILITY
                    unit.reset()
                elif event.category == 'CLIENT_VISIT_EVAC':
                    # Deletes pending deliveries and their charges and resets unit to default
                    unit.reset()
                else:
                    # Drop off EVAC
                    unit.status = models.unit.PENDING_PICKUP
                unit.put()

    return redirect('/deliveries')


@app.route("/cancel-order/<int:order_id>")
@is_admin_check
@login_required
def cancel_order(order_id):
    order = models.order.Order.get_by_id(order_id)
    owner = order.owner_key.get()
    status = services.seven_eleven.client_cancel_order(order_id)
    flash(status.get('message'), status.get('category'))
    return redirect('/users/{}'.format(owner.key.id()))


@app.route("/cancel-deliveries/<delivery_id>")
@is_admin_check
@login_required
def cancel_all(delivery_id):
    calendar_events = CalendarEvent.query(CalendarEvent.delivery_id == delivery_id).fetch()
    for e in calendar_events:
        if e.charge_key and e.charge_key.get():
            charge = e.charge_key.get()
            charge.archived = True
            charge.put()

        e.status = 'CANCELLED'
        e.put()

    return redirect('/deliveries')


@app.route("/")
@app.route("/units")
def all_units():
    size = request.args.get('size')
    size = request.args.get('p')

    return render_template('admin_list_units.html')


@app.route("/proforma-invoices/<int:user_id>", methods=['POST', 'GET'])
def proforma_invoices(user_id):
    user = models.user.User.get_by_id(user_id)

    charges = models.charge.Charge().query(models.charge.Charge.paid == False,
                                           models.charge.Charge.owner_key == user.key,
                                           models.charge.Charge.archived == False).fetch()

    total = sum([x.amount for x in charges])
    date = datetime.datetime.now()

    return render_template('admin_invoice_multiple_ocharges.html',
                           charges=[c.to_dictionary() for c in charges],
                           user=user,
                           total=total, date=date)


@app.route("/paid-invoices/<int:payment_id>", methods=['POST', 'GET'])
def paid_invoices(payment_id):
    payment = models.charge.EFTPayment.get_by_id(payment_id)

    if not payment:
        payment_method = "Credit card"
        payment = models.charge.CreditCardPayment.get_by_id(payment_id)
        payment_id = payment.peach_unique_id
    else:
        payment_method = "EFT"
        payment_id = payment.secure_eft_payment_key

    charges = ndb.get_multi(payment.charge_keys)
    total = sum([x.amount for x in charges])

    return render_template('admin_invoice_multiple_charges1.html',
                           payment=payment,
                           charges=[c.to_dictionary() for c in charges],
                           user=payment.owner_key.get(),
                           total=total, type=payment_method, payment_id=payment_id)


@app.route("/invoices/<int:charge_id>", methods=['POST', 'GET'])
def invoices(charge_id):
    charge = models.charge.Charge.get_by_id(charge_id)
    charges = None
    if charge.paid:
        if charge.peach_unique_id:
            # Payment was done with CC, lookup the payment record.
            payment = models.charge.CreditCardPayment.query(
                models.charge.CreditCardPayment.peach_unique_id == charge.peach_unique_id,
            ).get()

            if payment:
                payment_method = "Credit card"
                charges = ndb.get_multi(payment.charge_keys)
                total = sum([x.amount for x in charges])

                return render_template('admin_invoice_multiple_charges.html',
                                       payment=payment,
                                       charges=charges,
                                       user=payment.owner_key.get(),
                                       total=total, type=payment_method, payment_id=charge.peach_unique_id)

        else:
            payment = models.charge.EFTPayment().query(
                models.charge.EFTPayment.secure_eft_payment_key == charge.secure_eft_payment_key).get()
            # Payment was done with EFT, look up the Payment record
            if payment:
                charges = ndb.get_multi(payment.charge_keys)

                total = sum([x.amount for x in charges])
                payment_method = "EFT"

                return render_template('admin_invoice_multiple_charges.html',
                                       payment=payment,
                                       charges=charges,
                                       user=payment.owner_key.get(),
                                       total=total, type=payment_method, payment_id=charge.secure_eft_payment_key)

    if charge.item_key.kind() == "Unit":

        unit = charge.item_key.get()

        return render_template('admin_pod_invoice.html', charge=charge, user=charge.owner_key.get(), unit=unit)
    else:
        order = charge.item_key.get()
        order_dict = order.to_dictionary()
        invoice_date = (order.created + datetime.timedelta(hours=2)).strftime("%d %B %Y")

        return render_template('client_order_invoice2.html', order=order_dict, user=charge.owner_key.get(),
                               invoice_date=invoice_date)



@app.route("/unit-logs", methods=['GET'])
@login_required
def unit_logs():

    p = request.args.get('p', None)
    cursor = None
    if p:
        cursor = Cursor(urlsafe=p)

    logs, next_cursor, more = models.log.ClientLog().query(models.log.ClientLog.archived == False).fetch_page(30, start_cursor=cursor)

    next_curs = None
    if more:
        next_curs = next_cursor.urlsafe()

    return render_template('admin_unit_logs.html', logs=[l.to_dictionary() for l in logs], p=next_curs)


@app.route("/archive-unit-log/<int:log_id>", methods=['POST', 'GET'])
@login_required
def archive_log(log_id):
    log = models.log.ClientLog.get_by_id(log_id)
    log.archived = True
    log.put()
    return redirect("/unit-logs")


@app.route("/unit-logs-creation", methods=['POST', 'GET'])
@login_required
def unit_log_create():
    if request.method == 'POST':
        logging.info('creating client log with {}'.format(request.form))
        data = request.form

        image_file = request.files.get('picture', None)
        cl = models.log.ClientLog()

        if image_file:
            logging.info('Uploading file')
            file_name = '/podit-169717.appspot.com/unit-log-pictures/{}'.format(uuid.uuid1())
            write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                    options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                             }, retry_params=None)
            write_buffer.write(image_file.stream.read())
            write_buffer.close()

            cl.image_url = file_name

        cl.category = data.get('reason').upper()
        cl.name = data.get('name')
        cl.email = data.get('email')
        cl.cell_no = data.get('cell_no')
        cl.client_type = data.get('client_type').upper()
        cl.note = data.get('note')
        cl.unit_code = data.get('unit_code')
        cl.put()
        flash('Successfully added unit log', 'success')
        return redirect('/unit-logs')

    return render_template('admin_unit_log_creation.html')


@app.route("/units/toggle-listing/<int:unit_id>")
@login_required
@is_admin_check
def remove_unit(unit_id):
    unit = models.unit.Unit.get_by_id(unit_id)

    if unit.owner_key and not unit.archived:
        flash("This POD is currently occupied", "error")
        return redirect('/units')

    if not unit.archived:
        unit.archived = True
    else:
        unit.archived = False

    unit.put()
    flash("Operation complete", "success")

    return redirect('/units/{}'.format(unit_id))


@app.route("/archived-deliveries")
@login_required
@is_admin_check
def past_deliveries():
    p = request.args.get('p', None)
    cursor = None
    if p:
        cursor = Cursor(urlsafe=p)

    calendar_events, next_curs, more = CalendarEvent.query(ndb.OR(CalendarEvent.status == 'CANCELLED',
                                                                  CalendarEvent.status == 'COMPLETED')).order(
        CalendarEvent._key).fetch_page(1,
                                       start_cursor=cursor)
    if next_curs:
        next_curs = next_curs.urlsafe()
    return render_template('admin_archived_deliveries.html', calendar_events=calendar_events, next_curs=next_curs)


def _stringify_event_items(e):
    item_description = ''
    for key in e.item_keys:
        if key.kind() == "Order":
            item_description += "Shop Order <a href='https://admin.podit.co.za/orders/{}'>View</a><br>".format(
                key.id())

        if key.kind() == "Unit":
            item_description += "Unit:{} <a href='https://admin.podit.co.za/units/{}'>View</a><br>".format(
                key.get().unit_name, key.id())

    return item_description


def _compile_event_description(e, owner):
    item_description = _stringify_event_items(e)

    return ("{} {}\n"
            "{}\n"
            "<a href=https://admin.podit.co.za/complete-event/{"
            "}>Mark as done"
            "</a>\n"
            "<a href=https://admin.podit.co.za/cancel-deliveries/{}>"
            "cancel all events for this delivery"
            "</a>\n"
            "User comment: {}").format(owner.name,
                                       owner.cell_no,
                                       item_description,
                                       e.key.id(),
                                       e.delivery_id,
                                       e.user_comment)


@app.route("/deliveries")
@login_required
@is_admin_check
def all_deliveries():
    oauth_cred = SYSTEM_SETTINGS.oauth_credentials

    # Load credentials from the session.
    # credentials = google.oauth2.credentials.Credentials(
    #     **{'token': oauth_cred.token,
    #        'token_uri': oauth_cred.token_uri,
    #        'refresh_token': oauth_cred.refresh_token,
    #        'client_id': oauth_cred.client_id,
    #        'client_secret': oauth_cred.client_secret,
    #        'scopes': oauth_cred.scopes})
    credentials = app_engine.Credentials(scopes=SCOPES)
    service = googleapiclient.discovery.build(
        API_SERVICE_NAME, API_VERSION, credentials=credentials)

    # credentials_to_dict(credentials)
    def update_event(event_dict, e, owner):
        delivery_category = e.category
        if delivery_category == "DROPOFF_EVAC":
            delivery_category = "EVACUATION DROPOFF"
        if delivery_category == "PICKUP_EVAC":
            delivery_category = "EVACUATION PICKUP"

        if owner.unpaid_charges > 0.0:
            location = 'OUTSTANDING CHARGES'
        else:
            location = e.address.full_name if e.address else 'N/A'

        event_dict.update({
            'description': _compile_event_description(e, owner), 'attendees': [
                {'email': 'info@podit.co.za'}
            ],
            'summary': "{} of {} items".format(delivery_category.replace("_", " "),
                                               len(e.item_keys)),
            'location': location,
            'start': {
                'dateTime': e.internal_datetime.strftime("%Y-%m-%dT%H:%M:%S"),
                'timeZone': 'Africa/Harare',
            },
            'end': {
                'dateTime': (e.internal_datetime + datetime.timedelta(hours=1)).strftime("%Y-%m-%dT%H:%M:%S"),
                'timeZone': 'Africa/Harare',
            },
            'recurrence': [
                'RRULE:FREQ=DAILY;COUNT=1'
            ],

            'reminders': {
                'useDefault': False,
                'overrides': [
                    {'method': 'email', 'minutes': 24 * 60},
                    {'method': 'popup', 'minutes': 10},
                ],
            },
        })
        return event_dict


    # Fetch all events that are not cancelled or completed
    calendar_events = CalendarEvent.query(CalendarEvent.archived == False).order(-CalendarEvent.date).fetch()

    for e in calendar_events:

        try:
            if e.status == 'CANCELLED' or e.status == 'COMPLETED':
                e.archived = True
                if e.event_id:
                    item_description = _stringify_event_items(e)

                    event = service.events().get(calendarId='fdbjd5p94017bgrucgsova0dco@group.calendar.google.com',
                                                 eventId=e.event_id).execute()

                    event['summary'] = "{} {}".format(e.status, e.category.replace("_", " "))
                    event['description'] = "This event was for {} of {} items:\n{}".format(e.category.replace("_", " "),
                                                                                           len(e.item_keys),
                                                                                           item_description)
                    event['attendees'] = []
                    event['location'] = e.status

                    service.events().update(
                        calendarId='fdbjd5p94017bgrucgsova0dco@group.calendar.google.com',
                        eventId=event['id'], body=event).execute()

                e.put()
                continue

            event = dict()

            owner = e.owner_key.get()
            if not owner:
                logging.error("No owner for this event {}".format(e.key.id()))
                continue

            if e.event_id:
                if e.updated:
                    # We need to update this event so get the event first
                    event = service.events().get(calendarId='fdbjd5p94017bgrucgsova0dco@group.calendar.google.com',
                                                 eventId=e.event_id).execute()
                    e.updated = False
                    e.put()
                    event = update_event(event_dict=event, e=e, owner=owner)
                    service.events().update(calendarId='fdbjd5p94017bgrucgsova0dco@group.calendar.google.com',
                                            eventId=event['id'], body=event).execute()
                # Something changed with regards to time, item count or delivery notes

            else:
                event = update_event(event_dict=event, e=e, owner=owner)

                # This event is being inserted into the calendar for the first time
                event = service.events().insert(calendarId='fdbjd5p94017bgrucgsova0dco@group.calendar.google.com',
                                                body=event).execute()
                e.event_id = event['id']
                e.put()


        except Exception as e:
            logging.info(e)
            logging.info("Updating/creating G calendar event failed")
            continue

    return render_template('admin_deliveries.html')


@app.route("/units/logins", methods=['GET', 'POST'])
@login_required
@is_admin_check
def login_units():
    import models.log

    pod_log = None
    logs = models.log.PodLog.query().order(
        -models.log.PodLog.created).fetch()
    units = models.unit.Unit().query(models.unit.Unit.available == False,
                                     models.unit.Unit.paid_till > datetime.datetime.now()).order(
        -models.unit.Unit.paid_till,
        models.unit.Unit._key
    ).fetch(
        projection=[models.unit.Unit.unit_name], limit=30)

    orders = models.order.Order().query(models.order.Order.paid == True,
                                        models.order.Order.status == "AT_HQ").order(-models.order.Order.created).fetch(
        projection=[models.order.Order.order_ref], limit=30)

    if request.method == 'POST':

        unit_id = request.form.get('pod_id', None)
        unit_code = request.form.get('code', '')
        order_id = request.form.get('order_id', None)
        order_ref = request.form.get('order_ref', '')

        order = None
        unit = None

        if unit_id or unit_code:
            if unit_id:
                unit = models.unit.Unit().get_by_id(int(unit_id))
            else:
                unit = models.unit.Unit().query(models.unit.Unit.unit_name == unit_code).get()
        else:
            if not order_id:
                order = models.order.Order().query(models.order.Order.order_ref == order_ref).get()
            else:
                order = models.order.Order().get_by_id(int(order_id))

        if order:
            if not order.ship():
                flash('Client has not paid for order', 'error')
            else:
                services.sender.send_sms(order.owner_key.get().cell_no,
                                         "Your order #{} has been shipped from the PODit HQ and is on it's way to you".format(
                                             order.order_ref))
                flash('Order successfully marked as shipped', 'success')

        elif unit and unit.owner_key:
            form_category = request.form.get('category', '')
            category = form_category
            if category == "LOGIN":
                if unit.logged_in:
                    flash('First log out pod before logging back in', 'error')
                    return render_template('admin_log_unit.html', logs=logs, units=units)
                unit.logged_in = True
                services.sender.send_sms(unit.owner_key.get().cell_no,
                                         "Your pod {} has been logged into the facility".format(unit.unit_name))
            if category == "LOGOUT":
                if not unit.logged_in:
                    flash('First login pod before logging out', 'error')
                    return render_template('admin_log_unit.html', logs=logs, units=units)
                services.sender.send_sms(unit.owner_key.get().cell_no,
                                         "Your pod {} has been logged out of the facility".format(unit.unit_name))
                unit.logged_in = False

            pod_log = models.log.PodLog()
            pod_log.category = category
            pod_log.owner_key = unit.owner_key
            pod_log.item_key = unit.key
            pod_log.put()
            unit.put()
        else:
            flash('Unit or Order not found or has no owner', 'error')
        return redirect('/units/logins')

    return render_template('admin_log_unit.html', logs=logs, units=units, orders=orders)


@app.route("/archive-users/<int:user_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def user_archive(user_id):
    user = models.user.User.get_by_id(user_id)
    user.archived = True
    user.put()

    flash('Archived user!', 'success')
    return redirect('/users/{}'.format(user
                                       .key.id()))


@app.route("/edit-charges/<int:charge_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def edit_charge(charge_id):
    charge = models.charge.Charge.get_by_id(charge_id)

    if request.method == 'POST':
        charge.comment = request.form.get('admin_comment', ' ')

        if charge.amount != round(float(request.form.get('amount')), 2):
            charge.comment += " R{} to R{} on {} ".format(charge.amount, round(float(request.form.get('amount')), 2), (
                    datetime.datetime.now() + datetime.timedelta(hours=2)).strftime("%d-%m-%y at %H:%M"))
        charge.details = request.form.get('details', ' ')
        charge.amount = round(float(request.form.get('amount')), 2)
        charge.put()

    return render_template('admin_edit_charge.html', charge=charge.to_dictionary())


@app.route("/mark-unpaid-charges/<int:charge_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def unpay(charge_id):
    charge = models.charge.Charge.get_by_id(charge_id)
    owner = charge.owner_key.get()
    if not charge.paid:
        flash('Charge already unpaid', 'error')
    elif not charge.archived:

        cc_payment = models.charge.CreditCardPayment.query(
            models.charge.CreditCardPayment.peach_unique_id == charge.peach_unique_id).get()
        if cc_payment:
            cc_payment.key.delete()

        charge.paid = False
        charge.peach_unique_id = None
        charge.pay_date = None
        charge.comment += " ADMIN MARKED AS UNPAID {}".format(datetime.datetime.now())
        charge.put()

        flash('Successfully marked as unpaid!', 'success')
    else:
        flash('Charge cancelled', 'error')

    return redirect('/users/{}'.format(owner
                                       .key.id()))


@app.route("/mark-paid-charges/<int:charge_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def paid_charges(charge_id):
    charge = models.charge.Charge.get_by_id(charge_id)
    owner = charge.owner_key.get()
    if charge.paid:
        flash('Charge already paid', 'error')
    elif not charge.archived:

        charge.paid = True
        if request.args.get('eft', 'N/A') == 'on':

            charge.peach_external_eft_id = request.args.get('peach_id', 'N/A')
            charge.secure_eft_payment_key = request.args.get('peach_id', 'N/A')
            existing_eft_payment = models.charge.EFTPayment().find_by_ref(request.args.get('peach_id', 'N/A'))
            if existing_eft_payment:
                existing_eft_payment.charge_keys.append(charge.key)
                existing_eft_payment.put()
            else:
                payment = models.charge.EFTPayment()
                payment.owner_key = owner.key
                payment.secure_eft_payment_key = request.args.get('peach_id', 'N/A')
                payment.charge_keys.append(charge.key)
                payment.put()
        else:
            charge.peach_unique_id = request.args.get('peach_id', 'N/A')
            existing_cc_payment = models.charge.CreditCardPayment.find_by_ref(request.args.get('peach_id', 'N/A'))
            if existing_cc_payment:
                existing_cc_payment.charge_keys.append(charge.key)
                existing_cc_payment.put()
            else:
                charge.peach_unique_id = request.args.get('peach_id', 'N/A')
                credit_card_payment = models.charge.CreditCardPayment()
                credit_card_payment.owner_key = owner.key
                credit_card_payment.peach_unique_id = request.args.get('peach_id', 'N/A')
                credit_card_payment.charge_keys.append(charge.key)
                credit_card_payment.put()

        charge.pay_date = datetime.datetime.now()
        charge.comment += " ADMIN MARKED AS PAID on {}".format(datetime.datetime.now())
        charge.put()

        flash('Successfully marked as paid!', 'success')
    else:
        flash('Charge already cancelled', 'error')

    return redirect('/users/{}'.format(owner
                                       .key.id()))


@app.route("/cancel-charges/<int:charge_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def cancel_charge(charge_id):
    charge = models.charge.Charge.get_by_id(charge_id)
    owner = charge.owner_key.get()
    if charge.paid:
        flash('Charge already paid', 'error')
    elif not charge.archived:

        charge.archived = True

        charge.details += " ARCHIVED CHARGE - This charge was cancelled - reason: {}".format(
            request.args.get('reason', 'Admin cancellation'))
        charge.put()

        flash('Successfully cancelled charge!', 'success')
    else:
        flash('Charge already cancelled', 'error')

    return redirect('/users/{}'.format(owner
                                       .key.id()))


@app.route("/cancel-units/<int:unit_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def cancel_unit(unit_id):
    unit = models.unit.Unit.get_by_id(unit_id)
    owner = unit.owner_key.get()
    if owner:
        services.unit_controller.cancel_rent_and_schedule_return(owner,
                                                                 dict(unit_name=unit.unit_name,
                                                                      reason="Admin cancellation"), admin=True)

    pod_log = models.log.PodLog()
    pod_log.owner_key = owner.key
    pod_log.item_key = unit.key
    pod_log.description = "Unit {} with owner {} cancelled by admin".format(unit.unit_name, owner.name)
    pod_log.category = "CANCELLED"
    pod_log.put()
    flash('Reset unit and successfully cancelled and deleted charges and events for unit', 'success')
    return redirect('/units/{}'.format(unit_id))


@app.route("/units/send-gate-invite/<int:unit_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def send_gate_invite(unit_id):
    from requests.auth import HTTPBasicAuth

    unit = models.unit.Unit.get_by_id(unit_id)
    user = unit.owner_key.get() if unit.owner_key else None

    gate = models.system_settings.Gate.get_by_bs_key(unit.base_station_key)

    if gate:
        nold_data = {
            "guests": [
                {
                    "email": user.email,
                    "name": user.name,
                    'phone': user.cell_no
                }
            ],
            "devices": [
                {
                    "_id": gate.id,
                    "relay": 1
                }
            ],
            "key": {
                "type": "online"
            }
        }
        auth = HTTPBasicAuth("5e197b86-086e-4d39-9b60-bc63ae347000",
                             "7d699f74-c26f-46cb-b3a6-b3fef4a25862")
        r = requests.post(auth=auth, url='https://api.nold.io/public/invite', json=nold_data)
        flash('Successfully sent invite', 'success')

    return redirect('/units/{}'.format(unit_id))


@app.route("/units/<int:unit_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def view_unit(unit_id):
    unit = models.unit.Unit.get_by_id(unit_id)
    user = unit.owner_key.get() if unit.owner_key else None

    event_filter = request.args.get('events', 'PENDING')
    charge_filter = request.args.get('charges')

    user_search = request.args.get('user_search_term')
    user_list = list()
    if user_search:
        user_list = services.user_orchestrator.fetch_user_dicts_by_search(user_search).get('users')
        print user_list

    user_id = request.args.get('user_id')
    if user_id:
        price, price_per_month, delivery_price, months, pro_rata_this_month, quote_id = services.unit_controller.get_unit_quote(
            size=unit.unit_size, monthly=True, end_date=None)

        data = dict(monthly=True,
                    quoted_price=pro_rata_this_month,
                    message="Manual reservation",
                    pack_date=datetime.datetime.now().strftime("%d/%m/%Y"),
                    pack_time=["MORNING", "AFTERNOON"][datetime.datetime.now().hour > 10])

        new_owner = models.user.User.get_by_id(int(user_id))

        print new_owner
        print data
        services.unit_controller.reserve_unit_for_user(new_owner, data, unit_id=unit.key.id())
        flash('Successfully reserved unit', 'success')
        return redirect('/units/{}'.format(unit_id))

    eparams = list()
    cparams = list()

    eparams.append(getattr(CalendarEvent, 'item_keys') == unit.key)
    cparams.append(getattr(models.charge.Charge, 'item_key') == unit.key)

    if not event_filter == "ALL":
        eparams.append(getattr(CalendarEvent, 'status') == event_filter)

    if charge_filter:
        cparams.append(getattr(models.charge.Charge, 'paid') == (True if charge_filter == "PAID" else False))

    if request.method == "POST":
        bs = BaseStation.get_by_id(int(request.form.get('base_stations')))
        if bs:
            unit.base_station_key = bs.key
        unit.position_number = request.form.get('position_number', unit.position_number)
        unit.unit_name = request.form.get('unit_name', unit.unit_name)
        unit.status = int(request.form.get('status'))
        flash('successfully updated unit', 'success')
        unit.put()

    logs = models.log.PodLog.query(models.log.PodLog.item_key == unit.key).fetch()
    gate = models.system_settings.Gate.get_by_bs_key(unit.base_station_key)

    return render_template('admin_view_unit.html',
                           gate=gate,
                           base_stations=BaseStation.query().fetch(),
                           logs=[x.to_dictionary() for x in logs],
                           unit=unit,
                           user=user,
                           charges=[x.to_dictionary() for x in models.charge.Charge.query(*cparams).fetch()],
                           events=CalendarEvent.query(*eparams).fetch(), user_list=user_list)


@app.route("/users/<int:user_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def view_user(user_id):

    user = models.user.User.get_by_id(user_id)

    if request.method == "POST":
        if request.form.get('admin') == 'on':
            user.is_admin = True
        else:
            user.is_admin = False

        if request.form.get('driver') == 'on':
            user.is_driver = True
        else:
            user.is_driver = False

        if request.form.get('archive') == 'on':
            user.archived = True
        else:
            user.archived = False

        if request.form.get('recurring_billing') == 'on':
            user.recurring_billing = True
        else:
            user.recurring_billing = False

        user.put()

        cell_no_validation = services.user_orchestrator.cell_number_validator(request.form.get('cell_no'))
        if cell_no_validation.get('success'):
            user.cell_no = cell_no_validation.get('cell_no')
            user.name = request.form.get('name')
            user.put()
            flash('Successfully updated user', 'success')
        else:
            flash(cell_no_validation.get('message'), 'error')

    units = services.unit_controller.fetch_unit_dicts_for_user(user)
    # Orders
    orders = models.order.Order.query(models.order.Order.owner_key == user.key).fetch()
    orders_dicts = [o.to_dictionary() for o in orders]
    # Charges
    charges = models.charge.Charge().query(models.charge.Charge.owner_key == user.key).order(
        models.charge.Charge.created).fetch()
    charges_dicts = [x.to_dictionary() for x in charges]
    # Payments
    eft_payments = models.charge.EFTPayment.query(models.charge.EFTPayment.owner_key == user.key).fetch()
    credit_card_payments = models.charge.CreditCardPayment.query(
        models.charge.CreditCardPayment.owner_key == user.key).fetch()
    payments = eft_payments + credit_card_payments
    payment_dicts = [x.to_dictionary() for x in payments]
    # Communications
    comms = models.customer_communications.CustomerCommunication.query(
        models.customer_communications.CustomerCommunication.owner_key == user.key).order(
        models.customer_communications.CustomerCommunication.created).fetch()

    if user.shopping_cart_key:
        shopping_cart = user.shopping_cart_key.get()
        if bool(len(shopping_cart.items)):
            shopping_cart_info = dict(total=shopping_cart.calculate_total(),
                                      item_count=len(shopping_cart.items),
                                      cart_items=[x.get().to_dictionary() for x in shopping_cart.items])
        else:
            shopping_cart_info = None
    else:
        shopping_cart_info = None

    print user.key

    logs = models.log.PodLog.query(models.log.PodLog.owner_key == user.key).fetch()
    print logs



    return render_template('admin_view_user.html',
                           logs=[x.to_dictionary() for x in logs],
                           u=user,
                           units=units,
                           orders=orders_dicts,
                           charges_dicts=charges_dicts,
                           comms=comms,
                           payments=payment_dicts,
                           shopping_cart_info=shopping_cart_info)


@app.route("/payments", methods=["POST", "GET"])
@login_required
@is_admin_check
def view_payments():

    p = request.args.get('p', None)
    cursor = None
    if p:
        cursor = Cursor(urlsafe=p)

    payment_type = request.args.get('type', None)
    types = {
        'cc': models.charge.CreditCardPayment.query().order(-models.charge.CreditCardPayment.created),
        'eft': models.charge.EFTPayment.query().order(-models.charge.EFTPayment.created)
    }

    if not payment_type:
        q = types.get('cc')
    else:
        q = types.get(payment_type, types.get('cc'))

    payments, next_cursor, more = q.fetch_page(30, start_cursor=cursor)

    next_curs = None
    if more:
        next_curs = next_cursor.urlsafe()
    payment_dicts = list()
    for payment in payments:
        payment_dicts.append(payment.to_dictionary())

    return render_template('admin_list_payments.html', payments=payment_dicts, p=next_curs, type=payment_type)


@app.route("/users/invoices/reminders/<int:entity_id>", methods=["POST", "GET"])
def invoice_reminders(entity_id):
    charge = models.charge.Charge().get_by_id(entity_id)
    if charge:
        owner = charge.owner_key.get()
    else:
        owner = models.user.User.get_by_id(entity_id)

    if request.args.get('sms'):
        if owner.cell_no:
            services.sender.send_sms(owner.cell_no, "Please remember to pay your outstanding charges of R{} at www.podit.co.za".format(owner.unpaid_charges),
                                     owner.key.id())
        flash("SMS sent", 'success')
        return redirect('/users/{}'.format(owner.key.id()))

    if charge:
        if services.bill_collector.send_invoice_for_charge(charge):

            flash("Invoice reminder sent", 'success')

        else:
            flash("Problem sending invoice reminder", 'error')

    return redirect('/users/{}'.format(owner.key.id()))


@app.route("/archive/base-stations/<int:id>", methods=["GET", "POST"])
@login_required
@is_admin_check
def archive(id):
    bs = models.system_settings.BaseStation.get_by_id(id)
    bs.archived = True
    bs.put()
    flash('Archived base station', 'success')
    return redirect('/base-stations')


@app.route("/base-stations/<int:id>", methods=["GET", "POST"])
@login_required
@is_admin_check
def manage_bs(id):
    import utils.geo
    import models.address
    bs = models.system_settings.BaseStation.get_by_id(id)

    if request.method == "POST":

        bs.name = request.form.get('name')
        address_string = request.form.get('address')
        address = models.address.Address()
        coordinates = utils.geo.get_coordinates(address_string)
        if coordinates:
            address.coordinates = ndb.GeoPt("{},{}".format(coordinates[0], coordinates[1]))
            address.full_name = utils.geo.get_full_address(address_string)
            bs.address = address
            bs.put()
        else:
            flash("Address not found", 'error')

        flash("Updated base station", "success")

    return render_template('admin_base_station.html', bs=bs)


@app.route("/base-stations", methods=["GET", "POST"])
@login_required
@is_admin_check
def bases():
    import utils.geo
    import models.address

    base_stations = models.system_settings.BaseStation.fetch_unarchived()
    if request.method == "POST":
        bs = models.system_settings.BaseStation()
        bs.name = request.form.get('name')

        address_string = request.form.get('address')
        address = models.address.Address()
        coordinates = utils.geo.get_coordinates(address_string)
        if coordinates:
            address.coordinates = ndb.GeoPt("{},{}".format(coordinates[0], coordinates[1]))
            address.full_name = utils.geo.get_full_address(address_string)
            bs.address = address
            bs.put()
            flash("New base station created", "success")
            base_stations.append(bs)
        else:
            flash("Address not found, please type in the street number, name and city/suburb", 'error')

        flash("New base station created", "success")

    return render_template('admin_base_stations.html', base_stations=base_stations)


@app.route("/promos", methods=["GET", "POST"])
@login_required
@is_admin_check
def promos():
    promos = models.charge.Promo.query(models.charge.Promo.archived == False).fetch()

    if request.method == "POST":
        promo = models.charge.Promo()
        promo.code = request.form.get('code').title()
        promo.discount_percent = float(request.form.get('discount'))
        promo.put()
        flash("New promo created", "success")
        promos.append(promo)

    return render_template('admin_view_promos.html', promos=promos)


@app.route("/gallery", methods=["GET", "POST"])
@login_required
@is_admin_check
def gallery():
    if request.method == "POST":
        gi = models.blog_post.GalleryImage()
        image_file = request.files.get('gallery_image', None)
        if image_file:
            file_name = '/podit-169717.appspot.com/item-pictures/{}'.format(uuid.uuid1())
            write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                    options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                             }, retry_params=None)
            write_buffer.write(image_file.stream.read())
            write_buffer.close()
            gi.url = file_name
            gi.tags = request.form.get('image_tags')
            gi.description = request.form.get('description')
            gi.put()
            flash('Successfully added new image to gallery', 'success')

    return render_template('admin_gallery.html',
                           gallery=models.blog_post.GalleryImage.query(
                               models.blog_post.GalleryImage.archived == False).fetch())


@app.route("/gallery/removals/<int:image_id>")
@login_required
@is_admin_check
def remove_image(image_id):
    i = models.blog_post.GalleryImage.get_by_id(image_id)
    i.archived = True
    i.put()
    flash("Removed image", 'success')

    return redirect("/gallery")


@app.route("/promos/removals/<int:promo_id>")
@login_required
@is_admin_check
def remove_promo(promo_id):
    p = models.charge.Promo.get_by_id(promo_id)
    p.archived = True
    p.put()
    flash("Removed promo", 'success')

    return redirect("/promos")


@app.route("/orders/<int:order_id>")
@login_required
@is_admin_check
def order_viewer(order_id):
    order = models.order.Order.get_by_id(order_id)

    return render_template('admin_view_order.html',
                           total=order.calculate_total(),
                           order=order)


@app.route("/blogs/removals/<int:blog_id>")
@login_required
@is_admin_check
def remove_bp(blog_id):
    bp = models.blog_post.BlogPost.get_by_id(blog_id)
    bp.archived = True
    bp.put()

    return redirect("/blogs")


@app.route("/blogs/comments/<int:blog_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def comment(blog_id):
    blog = models.blog_post.BlogPost.get_by_id(blog_id)

    if request.method == "POST":
        c = models.blog_post.Comment()
        c.text = request.form.get('comment')
        c.owner_name = request.form.get('owner_name')
        c.put()
        blog.reply_keys.append(c.key)
        blog.put()

    return redirect("/blogs")


@app.route("/blogs/<int:blog_id>", methods=["POST", "GET"])
@login_required
@is_admin_check
def edit_blogs(blog_id):
    blog = models.blog_post.BlogPost.get_by_id(blog_id)
    if request.method == "POST":
        for k, v in request.form.iteritems():
            print k, v
            cp = models.blog_post.ContentPart.get_by_id(int(k))
            if cp:
                cp.text = str(v)
                cp.put()

        return redirect("/blogs")

    return render_template('admin_edit_blogs.html', bp=blog)


@app.route("/blogs", methods=["POST", "GET"])
@login_required
@is_admin_check
def blogs():
    import collections
    import string
    printable = set(string.printable)
    blog_posts = models.blog_post.BlogPost.query(models.blog_post.BlogPost.archived == False).fetch()
    if request.method == "POST":
        bp = models.blog_post.BlogPost()
        image_file = request.files.get('picture', None)
        if image_file:
            cp = models.blog_post.ContentPart()
            cp.category = "IMAGE"
            file_name = '/podit-169717.appspot.com/item-pictures/{}'.format(uuid.uuid1())
            write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                    options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                             }, retry_params=None)
            write_buffer.write(image_file.stream.read())
            write_buffer.close()
            cp.text = file_name
            cp.put()
            bp.content_part_keys.append(cp.key)

        content_part_dicts = dict()
        for k, v in request.form.iteritems():
            ordering = k.split(":")[0]
            content_part_dicts.update({int(ordering): (k.split(":")[1], v)})

        od = collections.OrderedDict(sorted(content_part_dicts.items()))
        for k, v in od.iteritems():
            cp = models.blog_post.ContentPart()

            text = filter(lambda x: x in printable, v[1])

            if v[0] == 'title':
                cp.category = "TITLE"
                cp.text = text
                cp.put()
                bp.content_part_keys.insert(0, cp.key)
            elif v[0] == 'text':
                cp.category = "TEXT"
                cp.text = text
                cp.put()
                bp.content_part_keys.append(cp.key)
            elif v[0] == 'subtitle':
                cp.category = "SUBTITLE"
                cp.text = text
                cp.put()
                bp.content_part_keys.append(cp.key)
            elif v[0] == "bold_text":
                cp.category = "BOLD_TEXT"
                cp.text = text
                cp.put()
                bp.content_part_keys.append(cp.key)
            else:
                cp.category = "LINK"
                cp.text = text
                cp.put()
                bp.content_part_keys.append(cp.key)

        if len(bp.content_part_keys) < 3:
            flash("Please add atleast one paragraph or subtitle", 'error')
            return render_template('admin_blogs.html', blog_posts=blog_posts)

        bp.put()
        blog_posts.append(bp)

    return render_template('admin_blogs.html', blog_posts=blog_posts)


@app.route("/users/<int:client_id>/checkouts")
@login_required
@is_admin_check
def checkout(client_id):
    user = models.user.User.get_by_id(client_id)
    data = dict(pack_date=datetime.datetime.now().strftime("%d/%m/%Y"),
                pack_time="MORNING")
    response = services.seven_eleven.create_order(user, data=data, admin=True)

    if response.get('success'):
        flash('Successfully created client order', 'success')
    else:
        flash('Error {}'.format(response.get('message')), 'error')

    return redirect('/users/{}'.format(client_id))


@app.route("/users/<int:client_id>/remove-baskets/<int:item_id>")
@login_required
@is_admin_check
def remove_items(client_id, item_id):
    user = models.user.User.get_by_id(client_id)
    response = services.seven_eleven.client_remove_from_cart(item_id, user)
    if response.get('success'):
        flash('Successfully removed item to cart', 'success')
    else:
        flash('Error {}'.format(response.get('message')), 'error')

    return redirect('/users/{}'.format(client_id))


@app.route("/users/<int:client_id>/add-baskets/<int:item_id>")
@login_required
@is_admin_check
def add_items(client_id, item_id):
    item = models.item.Item.get_by_id(item_id)
    user = models.user.User.get_by_id(client_id)
    response = services.seven_eleven.client_add_to_cart(item_id, user)
    if response.get('success'):
        flash('Successfully added item to cart', 'success')
    else:
        flash('Error {}'.format(response.get('message')), 'error')

    return redirect('/management/web-store2?client_id={}'.format(client_id))


@app.route("/management/web-store")
@login_required
@is_admin_check
def webstore_management():
    cat = request.args.get('item_cat_id')
    if cat:
        category = models.item.ItemCategory.get_by_id(int(cat))
        items = models.item.Item.query(models.item.Item.category == category.key).fetch()
    else:
        items = models.item.Item.query().fetch()

    categories = ItemCategory.query().fetch()
    if request.args.get('client_id'):
        user = models.user.User.get_by_id(int(request.args.get('client_id')))
    else:
        user = None

    item_dicts = [x.to_dictionary() for x in items]

    return render_template('admin_web_store_management2.html', user=user, categories=categories, items=item_dicts)


@app.route("/management/web-store2")
@login_required
@is_admin_check
def webstore_management2():
    cat = request.args.get('item_cat_id')
    if cat:
        category = models.item.ItemCategory.get_by_id(int(cat))
        items = models.item.Item.query(models.item.Item.category == category.key).fetch()
    else:
        items = models.item.Item.query().fetch()

    categories = ItemCategory.query().fetch()
    if request.args.get('client_id'):
        user = models.user.User.get_by_id(int(request.args.get('client_id')))
    else:
        user = None

    item_dicts = [x.to_dictionary() for x in items]

    return render_template('admin_web_store_management.html', user=user, categories=categories, items=item_dicts)


@app.route("/categories", methods=["POST"])
@login_required
@is_admin_check
def add_category():
    data = request.form.to_dict()
    print data
    name = data.get('category_name')
    if ItemCategory.get_by_name(name):
        return redirect('/management/items')
    description = data.get('category_description')
    item_category = ItemCategory()
    item_category.name = name
    item_category.description = description
    item_category.put()
    return redirect('/management/items')


@app.route("/management/items", methods=["POST", "GET"])
@login_required
@is_admin_check
def add_item():
    import handlers.admin_request_handler
    categories = ItemCategory.query().fetch()
    category_list = [x.name for x in categories]
    if request.method == "POST":
        print request.form.to_dict()
        data = request.form.to_dict()
        image_file = request.files.get('item_picture', None)
        print image_file
        print type(image_file)

        status = handlers.admin_request_handler.add_item(data, image_file)
        flash(status.get('message'), status.get('category'))
        return redirect('/management/items')

    return render_template('admin_item_add.html',
                           category_list=category_list)


@app.route("/items/<int:item_id>", methods=['POST', 'GET'])
@login_required
@is_admin_check
def edit_items(item_id):
    import logging
    item = Item.get_by_id(item_id)
    item_categories = ItemCategory.query().fetch()
    if request.method == 'POST':
        print type(request.form.get('delete_item'))
        if request.form.get('delete_item') == u'on':
            deleted_item_category_key = item.category
            item.archived = True
            item.put()
            if not Item.query(Item.category == deleted_item_category_key).count():
                logging.info("No more items in this category, deleting category")
                category = deleted_item_category_key.get()

                category.archived = True
                category.put()

            flash('Deleted item!', 'success')
            return redirect('/management/web-store')

        category = ItemCategory.get_by_name(request.form.get('category', None))
        item.category = category.key
        item.name = request.form.get('name')
        item.quantity = int(request.form.get('stock'))
        item.usage = request.form.get('usage')
        item.price = float(request.form.get('price'))
        item.description = request.form.get('description')

        image_file = request.files.get('item_picture', None)

        if image_file:
            file_name = '/podit-169717.appspot.com/item-pictures/{}.png'.format(uuid.uuid1())
            write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                    options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                             }, retry_params=None)
            write_buffer.write(image_file.stream.read())
            write_buffer.close()
            item.image_url = file_name
        flash("Successfully updated item!", "success")
        item.put()
        return redirect('/management/web-store')

    return render_template('admin_item_edit.html',
                           item=item,
                           item_categories=item_categories)


@app.route("/stats")
@login_required
def stats():
    return render_template('admin_stats.html')


@app.route("/new-user", methods=["POST", "GET"])
@login_required
@is_admin_check
def new_users():
    if request.method == "POST":

        cell_validator_status = services.user_orchestrator.cell_number_validator(request.form.get('cell_no'),
                                                                                 new_user=True)
        if not cell_validator_status.get('success'):
            flash(cell_validator_status.get('message'), 'error')
            return render_template('admin_new_user.html')

        u = models.user.User()
        u.firebase_id = "PASSIVE_USER"
        u.email = request.form.get('email', 'N/A')
        u.name = request.form.get('name')
        u.last_seen = datetime.datetime.now()
        u.cell_no = cell_validator_status.get('cell_no')
        u.put()

        flash('Successfully added user', 'success')
        return redirect('/users')

    return render_template('admin_new_user.html')


@app.route("/units/prices")
@login_required
def unit_prices():
    return render_template('admin_unit_prices.html')


@app.route("/exporter")
@login_required
def exporter():
    return render_template('admin_exporter.html')


@app.route("/users")
@login_required
def all_users():
    # ndb.put_multi(models.user.User.query().fetch())

    return render_template('admin_list_users.html')


@app.route("/list_users2")
@login_required
def all_users1():
    return render_template('admin_list_users2.html')


def utc_to_sast(value):
    return value + datetime.timedelta(hours=2)


app.jinja_env.filters['to_sast'] = utc_to_sast

if __name__ == "__main__":
    app.run(debug=True)
