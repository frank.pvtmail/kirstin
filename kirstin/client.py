# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import sys
import io
import csv
import ast
import stripe
import logging
import string
import datetime
import uuid
import mailjet_rest

import tablib
import requests
from flask import Flask, render_template, json, request, jsonify, abort, make_response, redirect, flash, session, \
    url_for

# noinspection PyUnresolvedReferences
from flask_login import (
    LoginManager,
    logout_user,
    login_user,
    current_user,
    login_required
)

app = Flask(__name__)
app.secret_key = '38uhsdfuh838uhsdfuh388hdjh88282'
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

import lib.cloudstorage as gcs
from google.appengine.ext import ndb
from google.appengine.ext import deferred
import urllib, urllib2, json
from google.appengine.datastore.datastore_query import Cursor
from models.user import User, Ad
from models.unit import Unit
import models.blog_post
from models.payment import Payment
from models.charge import CHARGE_TYPE_ONCE, CHARGE_TYPE_MONTHLY
from models.order import Order
import services.unit_controller
import services.bill_collector
import services.user_orchestrator

import utils.time_utils as time_utils
from models.system_settings import PoditSettings, CalendarEvent, BaseStation
from models.const import *
from models.item import ItemCategory, Item
import models.address
import utils.geo
import models.log
import services.sender
import handlers.client_request_handler as rh
import models.charge

from lib.requests_toolbelt.adapters import appengine

appengine.monkeypatch()


@login_manager.user_loader
def load_user(user_id):
    return User.get_by_id(int(user_id))


@app.context_processor
def inject_dict_for_all_templates():
    _dict = dict()
    _dict.update(js_version=str(uuid.uuid4()))
    _dict.update(live=True)
    # _dict.update(js_version="1")
    return _dict


@app.errorhandler(500)
def internal_error(e):
    logging.error(str(e))
    return redirect('/')


@app.route("/")
def main_base():
    print 'fgfg'
    # flash('If you have unpaid charges you will not be able to manage your Units', 'info')
    return render_template('index.html')


@app.route("/about")
def about():
    print 'fgfg'
    # flash('If you have unpaid charges you will not be able to manage your Units', 'info')
    return render_template('about.html')


@app.route("/portfolio")
def portfolio():
    print 'fgfg'
    # flash('If you have unpaid charges you will not be able to manage your Units', 'info')
    return render_template('portfolio.html')


@app.route("/contact")
def contact():
    print 'fgfg'
    # flash('If you have unpaid charges you will not be able to manage your Units', 'info')
    return render_template('contact.html')


if __name__ == "__main__":
    app.run(debug=True)
