import logging
import datetime
from models.charge import Charge
import models.const
from google.appengine.ext import ndb
import services.sender
import models.charge
import models.unit
import models.order
import models.item
import models.const
import requests


def charge_users_card(user, address_data, amount, merch_ref, initial=True):
        url = "https://oppwa.com/v1/registrations/{}/payments".format(str(user.peach_reg_id))


        data = [
            ('authentication.userId', models.const.PEACH_LIVE_USER_ID),
            ('authentication.password', models.const.PEACH_LIVE_PASSWORD),
            ('authentication.entityId', models.const.INITIAL_CHANNEL_ID_LIVE),
            ('amount', "{:.2f}".format(amount)),
            ('currency', 'ZAR'),
            ('merchantTransactionId', merch_ref ),
            ('customer.givenName', user.name.split(" ")[0] if user.name else 'N/A'),
            ('customer.surname', user.name.split(" ")[1:] if user.name else 'N/A'),
            ('customer.email', user.email),
            ('shipping.street1', address_data.get('street') ),
            ('shipping.city', address_data.get('city') ),
            ('shipping.country', address_data.get('country_code') )

        ]
        if initial:
            data.append(  ('recurringType', 'INITIAL'))
            data.append(('createRegistration', 'true'))
            data.append(('paymentType', 'DB'))
        else:
            data.append(  ('recurringType', 'REPEATED'))
            data.append(('paymentType', 'PA'))




        print data
        try:
            r = requests.post(url, data=data)
            if r:
                return r.json()
        except:
            return None

        return None


def mark_charges_as_paid(unpaid_charges, credit_card_id=None, eft_payment=None):
    paid_charges = list()
    for charge in unpaid_charges:
        charge.paid = True
        charge.peach_unique_id = credit_card_id if credit_card_id else 'N/A'
        charge.secure_eft_payment_key = eft_payment.get('payment_key') if eft_payment else "N/A"
        charge.peach_external_eft_id = eft_payment.get('gateway_reference') if eft_payment else "N/A"

        charge.pay_date = datetime.datetime.now()

        if charge.item_key.kind() == 'Unit':
            unit = charge.item_key.get()
            unit.paid_till = unit.term_end
            unit.put()

        if charge.item_key.kind() == 'Order':
            order = charge.item_key.get()
            order.paid = True
            order.put()
            for key in order.items:
                if key:
                    i = key.get()
                    i.sold_count += 1
                    i.quantity = i.quantity - 1
                    i.put()
        paid_charges.append(charge)

    ndb.put_multi(paid_charges)

def send_invoice_for_charge(charge, subj="Outstanding charges reminder"):
    user = charge.owner_key.get()
    item_names = list()

    if charge.item_key.kind() == 'Order':
        order = charge.item_key.get()
        for i in order.to_dictionary().get('item_dicts'):
            item_names.append({"x": {"name": i.get('name'), "price": i.get('price')}})

        return services.sender.send_email(to=user.email, subj=subj,
                                   template_id=models.const.MAIL_JET_NEW_CHARGE_TEMPLATE_ID,
                                   variables={"order_ref": "Order # {}".format(order.order_ref), "items": item_names,
                                              "total": order.to_dictionary().get('total_charge'),
                                              "client_name": user.name,
                                              "client_cell_no": user.cell_no,
                                              "client_address": user.address.full_name  if user.address else 'N/A',
                                              "client_email": user.email
                                              }, user_id=user.key.id())
    elif charge.item_key.kind() == 'Unit':
        unit = charge.item_key.get()
        item_names.append({"x": {"name": unit.unit_name, "price": charge.amount }})

        return services.sender.send_email(to=user.email, subj=subj,
                                   template_id=models.const.MAIL_JET_NEW_CHARGE_TEMPLATE_ID,
                                   variables={"order_ref": unit.unit_name, "items": item_names,
                                              "total": charge.amount,
                                              "client_name": user.name,
                                              "client_cell_no": user.cell_no,
                                              "client_address": user.address.full_name if user.address else 'N/A',
                                              "client_email": user.email
                                              }, user_id=user.key.id())

    else:
        return None