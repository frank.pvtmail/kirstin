import logging
import datetime
import uuid
import utils.time_utils as time_utils
import utils.geo
from models.unit import PENDING_DROPOFF, \
    PENDING_PICKUP, \
    POD_SIZE, POD_SIZE_HUMANIZE,\
    TYPES_HUMANIZE, \
    STATUS_TO_TEXT, \
    AT_FACILITY,\
    CANCELLED

from models.charge import CHARGE_TYPE, CHARGE_TYPE_MONTHLY, CHARGE_TYPE_ONCE, ITEM_CATEGORY_ORDER
import models.charge
import models.unit
import models.const
import models.item
import models.user
import models.shopping_cart
import models.system_settings
import services.sender
import services.user_orchestrator
import models.order
import models.address
from flask import Flask, jsonify, request
from google.appengine.api import search
from google.appengine.datastore.datastore_query import Cursor
import lib.cloudstorage as gcs
from google.appengine.ext import ndb
import models.item


def client_add_ad(data, image_files, owner):
    item = models.user.Ad()

    if len(image_files):
        for i in image_files:
            file_name = '/zarprojectx.appspot.com/ad-pictures/{}'.format(uuid.uuid1())
            write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                    options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                             }, retry_params=None)
            write_buffer.write(i.stream.read())
            write_buffer.close()
            item.image_urls.append(file_name)

    item.title = data.get('title')
    item.shipping = data.get('shipping')

    item.link = data.get('link')
    item.content = data.get('content')
    for c in item.content:
        print c
        if c == "\n":
            print 'newline'
    item.keywords = data.get('keywords')
    item.location = data.get('location')
    item.price = round(float(data.get('price')), 2)
    item.owner_key = owner.key
    item.put()
    return dict(success=True, category="success", message="Successfully added item")


def fetch_ad_dicts_by_search(search_term):
    index = models.user.Ad.get_class_search_index()

    cursor = request.args.get('p')
    if cursor == 'None':
        cursor = search.Cursor()
    else:
        cursor = search.Cursor(web_safe_string=cursor)

    query_string = search_term

    sort_expression_params = {
        'expression': 'created',
        'direction': search.SortExpression.ASCENDING
    }
    sort_name = search.SortExpression(**sort_expression_params)
    sort_options = search.SortOptions(expressions=[sort_name])

    # Build the query using the cursor
    query_options_params = {
        'limit': 30,
        'cursor': cursor,
        'sort_options': sort_options
    }
    options = search.QueryOptions(**query_options_params)
    query = search.Query(query_string=query_string, options=options)
    results = index.search(query=query)

    cursor = results.cursor
    if cursor:
        cursor_string = cursor.web_safe_string
    else:
        cursor_string = 'None'

    all_ads = list()
    # Populate dict with props of search results
    for scored_document in results.results:
        # Create dict containing props from search results
        results_dict = dict()
        for prop in scored_document.fields:
            results_dict[prop.name] = prop.value

        ad = models.user.Ad.get_by_id(int(results_dict.get('ad_id')))
        if not ad.archived:
            ad_dict = ad.to_dictionary()
            all_ads.append(ad_dict)

    result_count = results.number_found
    return {'ads': all_ads,
            'next_page': cursor_string,
            "total": result_count}

