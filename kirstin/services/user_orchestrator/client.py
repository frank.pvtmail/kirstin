import logging
import datetime
import utils.geo
import models.charge
import models.user
import models.unit
import models.shopping_cart
import models.address
from flask import Flask, jsonify, request
from google.appengine.api import search
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext import ndb
from lib.validate_email import validate_email
import services.sender
import models.const


def verify_user(firebase_token, admin):
    u = models.user.User.query(models.user.User.firebase_id == firebase_token['sub']).get()
    if u:

        if admin and (not u.is_admin and not u.is_driver):
            return jsonify(
                {"success": False})

        # user exists
        u.last_seen = datetime.datetime.now() + datetime.timedelta(hours=2)
        if not u.shopping_cart_key:
            shopping_cart = models.shopping_cart.ShoppingCart()
            shopping_cart.owner_key = u.key
            shopping_cart.put()
            u.shopping_cart_key = shopping_cart.key

        u.put()
        complete = False
        if u.address and u.cell_no != "":
            complete = True

        unpaid = models.charge.Charge.query(models.charge.Charge.paid == False, models.charge.Charge.owner_key == u.key,
                                            models.charge.Charge.archived == False).count()
        return jsonify({"success": True, "profile_complete": True, "charges": unpaid, "last_seen": u.last_seen,
                        "user_id": u.key.id(), "user_dict": u.to_dictionary(), "is_driver": u.is_driver})

    msg = "New user created"

    u = models.user.User()
    if firebase_token['email'] == "frank.pvtmail@gmail.com" or firebase_token['email'] == "marius@fame.net.za":
        u.is_admin = True
        msg += " ADMIN"

    try:
        u.firebase_id = firebase_token['sub']
        u.email = firebase_token['email']
        u.name = firebase_token['name']
    except Exception as e:
        logging.error(str(e))
    u.cell_no = ""
    u.put()
    services.sender.send_email(u.email, template_id=models.const.MAIL_JET_WELCOME_TEMPLATE_ID, user_id=u.key.id())

    shopping_cart = models.shopping_cart.ShoppingCart()
    shopping_cart.owner_key = u.key

    shopping_cart.put()

    u.shopping_cart_key = shopping_cart.key
    u.put()

    return jsonify({"success": True, "profile_complete": False, "message": msg, "charges": 0, "user_id": u.key.id(),
                    "user_dict": u.to_dictionary()})


def validate_and_format_cell(cell_number):
    if not cell_number[0] == '+' and not cell_number[0] == '0':
        return None

    if cell_number[0] == '0':
        if len(cell_number) != 10:
            return None

        cell_number = cell_number[1:]
    else:
        if len(cell_number) != 12:
            return None

        cell_number = cell_number[3:]

    for c in cell_number:
        if not c.isdigit():
            return None

    return '+27' + cell_number


def edit_user(data, user):
    user.name = str(data.get("name", 'not set'))
    user.business_name = str(data.get("business_name", 'not set'))
    user.vat_number = str(data.get("vat_number", 'not set'))

    user.email = str(data.get('email', 'not set'))
    if not validate_email(str(user.email)):
        return jsonify({"success": False, "message": "Email in the incorrect format"})

    if data.get("cell_no", '') == '':
        logging.info('No cell phone specified')
    else:
        cell_number = str(data.get("cell_no", ''))
        if not cell_number[0] == '+' and not cell_number[0] == '0':
            return jsonify({"success": False, "message": "Cell phone number in wrong format"})

        if cell_number[0] == '0':
            if len(cell_number) != 10:
                return jsonify({"success": False, "message": "Cell phone number in wrong format"})
            cell_number = cell_number[1:]
        else:
            if len(cell_number) != 12:
                return jsonify({"success": False, "message": "Cell phone number in wrong format"})

            cell_number = cell_number[3:]

        for c in cell_number:
            if not c.isdigit():
                return jsonify({"success": False, "message": "Cell phone number in wrong format"})

        user.cell_no = '+27' + cell_number

    if data.get("address", '') == '':
        logging.info('No address specified')
    else:
        address = models.address.Address()
        coordinates = utils.geo.get_coordinates(str(data.get("address", '')))
        if coordinates is None:
            return jsonify({"success": False,
                            "message": "Address not found, please provide the Street, Suburb, City and Province"})
        address.coordinates = ndb.GeoPt("{},{}".format(coordinates[0], coordinates[1]))
        address.full_name = utils.geo.get_full_address(str(data.get("address", '')))
        address.name = str(data.get("address", ''))
        address.owner_key = user.key
        print address
        user.address = address
    user.put()

    return jsonify({"success": True, "message": "Successfully updated profile!"})


def user_to_dictionary(user_obj):
    # charges = models.charge.Charge.query(models.charge.Charge.owner_key == user_obj.key,
    #                                      models.charge.Charge.paid == False,
    #                                      models.charge.Charge.archived == False).fetch()

    units = ''
    for unit in models.unit.Unit.query(models.unit.Unit.owner_key == user_obj.key).fetch():
        units += "{} | ".format(unit.unit_name)

    user_dict = {
        'name': user_obj.name,
        'cell_no': user_obj.cell_no,
        'email': user_obj.email,
        'address': user_obj.address.full_name if user_obj.address else 'N/A',
        'user_id': user_obj.key.id(),
        'friendly_id': user_obj.friendly_id,
        'units': units,
        'last_seen': user_obj.last_seen.strftime("%Y/%m/%d"),
        'outstanding_charges': user_obj.unpaid_charges
        # 'outstanding_charges': sum([c.amount for c in charges])
    }
    return user_dict


def fetch_user_dicts_by_query(query, filter='all'):
    cursor = request.args.get('p', None)
    if cursor == 'None':
        cursor = None
    if cursor:
        cursor = Cursor(urlsafe=cursor)

    usersq, next_curs, more = query.fetch_page(30, start_cursor=cursor)

    if not len(usersq):
        return {'users': [], 'next_page': 'None'}

    if more:
        next_curs = next_curs.urlsafe()
    else:
        next_curs = 'None'

    users = []

    for user in usersq:
        user_dict = user_to_dictionary(user)
        if filter == 'all':
            users.append(user_dict)
            continue

        if filter == 'unpaid' and user_dict.get('outstanding_charges') > 0:
            users.append(user_dict)

    return {'users': users, 'next_page': next_curs, "total": len(usersq)}


def toggle_admin_status(user):
    if user.is_admin:
        user.is_admin = False
    else:
        user.is_admin = True
    user.put()

    return dict(success=True, message="Successfully {} as admin".format(["unmarked", "marked"][user.is_admin]))


def fetch_user_dicts_by_search(search_term):
    index = models.user.User.get_class_search_index()

    cursor = request.args.get('p')
    if cursor == 'None':
        cursor = search.Cursor()
    else:
        cursor = search.Cursor(web_safe_string=cursor)

    query_string = search_term
    results = index.search(query=search.Query(query_string,
                                              options=search.QueryOptions(
                                                  limit=30,
                                                  cursor=cursor,
                                                  sort_options=None))
                           )
    cursor = results.cursor
    if cursor:
        cursor_string = cursor.web_safe_string
    else:
        cursor_string = 'None'

    all_users = []
    # Populate dict with props of search results
    for scored_document in results.results:
        # Create dict containing props from search results
        results_dict = dict()
        for prop in scored_document.fields:
            results_dict[prop.name] = prop.value

        user = models.user.User.get_by_id(int(results_dict.get('user_id')))
        if not user.archived:
            user_dict = user_to_dictionary(user)
            all_users.append(user_dict)

    result_count = results.number_found
    return {'users': all_users,
            'next_page': cursor_string,
            "total": result_count}
