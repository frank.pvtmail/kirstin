
import logging
import models.user


def cell_number_validator(cell_no, new_user=False):
    if cell_no == '' or not cell_no:
        logging.info('No cell phone specified')
        return {"success": False, "message": "Cellphone number must be provided"}
    else:

        if not cell_no[0] == '+' and not cell_no[0] == '0':
            return {"success": False, "message": "Cell phone number in wrong format"}

        if cell_no[0] == '0':
            if len(cell_no) != 10:
                return {"success": False, "message": "Cell phone number in wrong format"}
            cell_number = cell_no[1:]
        else:
            if len(cell_no) != 12:
                return {"success": False, "message": "Cell phone number in wrong format"}

            cell_number = cell_no[3:]

        for c in cell_number:
            if not c.isdigit():
                return {"success": False, "message": "Cell phone number in wrong format"}

        if new_user:
            if models.user.User.query(models.user.User.cell_no == "+27" + cell_number).get():
                return {"success": False, "message": "Cell number already registered"}

        return dict(success=True, cell_no='+27' + cell_number)