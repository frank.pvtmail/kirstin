import logging
from lib import mailjet_rest
import lib.twilio.rest as tc


from models.const import *
import models.customer_communications
import models.user

def send_email(to, subj="PODit HQ", content="test", template_id=None, variables=None, user_id=1234):
    """
    This function sends a email to a specified email
    :param to: String email to whom the email is sent to
    :param content: content of the email
    :return: JSON data with result of the mail delivery
    """

    client = mailjet_rest.Client(
        auth=(MAILJET_API_KEY, MAILJET_API_SECRET), version='v3.1')

    data = {
        'FromEmail': MAILJET_SENDER,
        'FromName': 'Podit',
        'Subject': 'PODit',
        'Text-part': 'This is an example email.',
        'Html-part': content,
        'Recipients': [{'Email': to}]
    }

    logging.info("Sending email to {} using template {}".format(to, template_id))
    if template_id:
        if variables:

            data = {
                'Messages': [
                    {
                        "From": {
                            "Email": MAILJET_SENDER,
                            "Name": "Podit"
                        },
                        "To": [
                            {
                                "Email": to,
                                "Name": "PODit Client"
                            }
                        ],
                        "TemplateID": template_id,
                        "TemplateLanguage": True,
                        "Subject": subj,

                        "Variables": variables
                    }
                ]
            }
        else:
            data = {
                'Messages': [
                    {
                        "From": {
                            "Email": MAILJET_SENDER,
                            "Name": "Podit"
                        },
                        "To": [
                            {
                                "Email": to,
                                "Name": "PODit Client"
                            }
                        ],
                        "TemplateID": template_id,
                        "TemplateLanguage": True,
                        "Subject": subj,

                    }
                ]
            }

        data.get("Messages")

    result = client.send.create(data=data)

    message_list = result.json().get('Messages')

    user = models.user.User.get_by_id(user_id)
    if user:
        cc = models.customer_communications.CustomerCommunication()
        cc.owner_key = user.key
        cc.medium = "EMAIL"
        cc.content = {MAIL_JET_NEW_POD_TEMPLATE_ID: "NEW POD EMAIL",
                      MAIL_JET_WELCOME_TEMPLATE_ID: "WELCOME EMAIL",
                      MAIL_JET_NEW_ORDER_TEMPLATE_ID: "NEW ORDER"}.get(template_id, "N/A")
        if len(message_list):
            if message_list[0].get('Status') == "success":
                cc.status = "SUCCESS"
                cc.service_uid = str(message_list[0].get('To')[0].get('MessageID'))

        else:
            cc.status = "FAILED"
        cc.put()
    return result.json()


def send_sms(to, message, user_id=12345):
    """Sends a simple SMS message."""

    logging.info("Sending from {}".format(TWILIO_NUMBER))
    if not to:
        return ('Please provide the number to message in the "to" query string'
                ' parameter.'), 400

    client = tc.Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    rv = client.messages.create(
        to=to,
        from_=TWILIO_NUMBER,
        body=str(message))
    print rv
    user = models.user.User.get_by_id(user_id)
    if user:
        cc = models.customer_communications.CustomerCommunication()
        cc.owner_key = user.key
        cc.content = str(message)
        cc.medium = "SMS"
        cc.service_uid = str(rv.sid)
        cc.put()

    return rv