import logging
import datetime
import calendar
import utils.time_utils as time_utils
import utils.geo
import models.unit
import requests
import uuid
from models.charge import CHARGE_TYPE, CHARGE_TYPE_MONTHLY, CHARGE_TYPE_ONCE
import models.charge
from utils import geo
import models.system_settings as ss
import services.sender
import models.log
import models.address
import models.item
from flask import Flask, jsonify, request
from google.appengine.api import search
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext import ndb
from google.appengine.ext import deferred
import services.sender as communicator
import models.system_settings
import delivery
from models.const import *
import services.bill_collector


def bulk_add_units(pod_size_text, position_number, name):
    bs = ss.BaseStation.find_by_name("PODit Stellenbosch")
    size = models.unit.POD_SIZE[pod_size_text.upper()]
    u = models.unit.Unit()
    u.available = True
    u.status = models.unit.AT_FACILITY
    u.price = 0
    u.unit_size = size
    u.unit_type = 0
    u.base_station_key = bs.key if bs else ss.BaseStation.query().get().key
    u.position_number = position_number.upper()
    u.unit_name = ("{}|{}".format(pod_size_text, name)).upper()

    if models.unit.Unit.query(models.unit.Unit.unit_name == u.unit_name).get():
        logging.error('POD name already exists')
        return dict(success=False, message="POD name already exists")

    if models.unit.Unit.query(models.unit.Unit.position_number == u.position_number).get():
        logging.error('POD name already exists')
        return dict(success=False, message="POD position number already exists")

    u.put()
    logging.info('POD created with ndb ID: {}'.format(u.key.id()))
    return dict(success=True, message="Added pod successfully")



def get_latest_labor_prices_by_size(size):
    settings = ss.PoditSettings.fetch()
    return round(float({
                        3: settings.JUMBO_POD_LABOR,
                        4: settings.C_LABOR,
                        5: settings.HC_LABOR
                        }.get(size, 0.0)), 2)

def get_latest_unit_prices():
    settings = ss.PoditSettings.fetch()
    return {0: settings.MINI_POD_PRICE,
            1: settings.POD_PRICE,
            2: settings.LARGE_POD_PRICE,
            3: settings.JUMBO_POD_PRICE,
            4: settings.C_PRICE,
            5: settings.HC_PRICE,
            6: settings.B_PRICE
            }


def get_latest_unit_prices_by_size(size):
    settings = ss.PoditSettings.fetch()
    return round(float({0: settings.MINI_POD_PRICE,
                        1: settings.POD_PRICE,
                        2: settings.LARGE_POD_PRICE,
                        3: settings.JUMBO_POD_PRICE,
                        4: settings.C_PRICE,
                        5: settings.HC_PRICE,
                        6: settings.B_PRICE
                        }.get(size, 0)), 2)


def _calculate_pro_rata_rate(price, start_date=datetime.datetime.now() + datetime.timedelta(hours=2)):
    d = start_date
    print 'start date for pro rata'
    print d
    days_current_month = calendar.monthrange(d.year, d.month)[1]
    discount = float(d.day - 1.0) / float(days_current_month)
    price_today = price - price * discount
    return round(price_today, 2)


def get_unit_quote(size, monthly, end_date, start_date=None):
    """
    This function calculates the price of a POD over a given period if opting
    for a once-off payment else it returns the monthly cost of the POD
    :param size: int: size of the POD (0 - x)
    :param monthly: Bool: True if the POD will be paid on a subscribtion basis as opposed to once-off
    :param end_date: String(mm/YYY): For once-off payments the end of rent date must be given to calculate the price over the given period
    :return: float: price of the POD as off right now
    """
    settings = ss.PoditSettings.fetch()
    if start_date:
        d = datetime.datetime.strptime(start_date, "%d/%m/%Y")
    else:
        d = datetime.datetime.now() + datetime.timedelta(hours=2)
    per_month_price = get_latest_unit_prices_by_size(int(size))
    pro_rate_this_month = _calculate_pro_rata_rate(per_month_price, start_date=d)
    price_today = pro_rate_this_month

    months = 0
    quote = models.charge.Quote()

    if not monthly:
        end_date = str("01/{}".format(end_date))
        ed = datetime.datetime.strptime(end_date, "%d/%m/%Y")
        months = (ed.year - d.year) * 12 + ed.month - d.month
        price_today += per_month_price * months
        if months > 0:
            quote.details = "Once off payment, calculated as follows: R {} x {} + R {} for this month, for months {} to and including {}".format(
            per_month_price,
            months,
            pro_rate_this_month,
            d.strftime("%B %Y"),
            ed.strftime("%B %Y"))
        else:
            quote.details = "Once off payment of  R{} for {}".format(pro_rate_this_month,
                                                                     ed.strftime("%B %Y"))
        quote.amount = price_today

    else:
        quote.details = "First monthly payment of R {} for {}".format(price_today, d.strftime("%B %Y"))
        quote.amount = price_today
    quote.put()

    return price_today, per_month_price, settings.delivery_cost, months, pro_rate_this_month, str(quote.key.id())


def delete_charges_and_events_for_unit(unit, reason=""):
    try:
        # Delete Unit costs
        for charge in models.charge.Charge.query(models.charge.Charge.owner_key == unit.owner_key,
                                                 models.charge.Charge.item_key == unit.key,
                                                 models.charge.Charge.paid == False,
                                                 models.charge.Charge.item_category == models.charge.ITEM_CATEGORY_UNIT,
                                                 models.charge.Charge.archived == False).fetch():
            charge.archived = True

            charge.details += " ARCHIVED CHARGE - This charge was cancelled - reason: {}".format(reason)
            charge.put()
        # Delete labor costs
        for charge in models.charge.Charge.query(models.charge.Charge.owner_key == unit.owner_key,
                                                 models.charge.Charge.item_key == unit.key,
                                                 models.charge.Charge.paid == False,
                                                 models.charge.Charge.item_category == models.charge.ITEM_CATEGORY_LABOR,
                                                 models.charge.Charge.archived == False).fetch():
            charge.archived = True

            charge.details += " ARCHIVED CHARGE - This charge was cancelled - reason: {}".format(reason)
            charge.put()

        # deletes calendars event and archives related charges
        unit.reset()
        return True
    except Exception as e:
        logging.error(e)

        return False


def cancel_rent_and_schedule_return(current_user, data, admin=False):
    """
     This function cancels the rent of a Unit
     :param admin:
     :return:
     :param current_user: int: User Object
     :param data: dict():  containing information about the delivery and unit to be cancelled
     :return: dict(): Information about the success or failure of the operation
     """
    unit = models.unit.Unit.get_by_name((data.get('unit_name')))

    charge_query = models.charge.Charge.query(models.charge.Charge.owner_key == current_user.key,
                                              models.charge.Charge.item_key == unit.key,
                                              models.charge.Charge.item_category == models.charge.ITEM_CATEGORY_UNIT,
                                              models.charge.Charge.archived == False)

    unpaid_charge_query = models.charge.Charge.query(models.charge.Charge.owner_key == current_user.key,
                                                     models.charge.Charge.item_key == unit.key,
                                                     models.charge.Charge.paid == False,
                                                     models.charge.Charge.archived == False)

    if charge_query.count() == 1 and not charge_query.get().paid or admin:
        pod_log = models.log.PodLog()
        pod_log.owner_key = unit.owner_key
        pod_log.item_key = unit.key
        pod_log.description = "Unit {} with owner {} cancelled by User, first charge of client so immediately voided charges".format(
            unit.unit_name, unit.owner_key.get().name)
        pod_log.category = "CANCELLED"
        pod_log.put()
        delete_charges_and_events_for_unit(unit, reason=data.get('reason', "User cancellation"))
        # User: The user has 1 charge and it is unpaid, meaning the POD is still at the facility and the client never received
        # Admin: All unpaid charges are cancelled and delivery events cancelled

        return dict(success=True,
                    message="Successfully cancelled POD. You have not used your POD thus all charges are cancelled", charges=0)

    if unpaid_charge_query.count():
        # user has unpaid charges

        return dict(success=False,
                    message="You have outstanding charges, please settle them before cancelling the unit")

    crm_text = """You have opted to cancel rent.<br>Please vacate the unit as soon as 
                                           possible as charges will only stop incurring once you have vacated the unit
    """
    # delete all existing calendar events and set status to cancelled

    unit.cancel_rent()

    if data.get('has_delivery'):

        # Schedule new deliveries for the unit based on data from the user

        scheduler_status = delivery.schedule_deliveries(data, unit.key.id(), evac_delivery=True)

        if scheduler_status.get('success', False):

            crm_text = (
                "You have opted to cancel rent.  Your delivery is scheduled for {} at {} and pickup of Unit "
                "for {} at {}. Where MORNING is (8am - 12pm) and AFTERNOON is (1pm - 5pm). Please have "
                "someone ready to accept the unit.").format(
                data.get('dropoff_date'), data.get('dropoff_time'), data.get('pickup_date'),
                data.get('pickup_time'))

        else:
            return dict(sucess=False, message=scheduler_status.get('message'))
    else:
        pd = datetime.datetime.strptime(data["evac_date"], "%d/%m/%Y")
        client_visit = models.system_settings.CalendarEvent()
        client_visit.category = 'CLIENT_VISIT_EVAC'

        client_visit.item_keys.append(unit.key)
        client_visit.owner_key = current_user.key
        client_visit.date = pd
        client_visit.time_category = str(data.get('evac_time'))
        client_visit.put()

        crm_text = """
               You have opted to cancel rent. Your appointment to evacuate the unit at the Podit HQ is scheduled for {}
            in the {}. Where MORNING is (8am - 12pm) and AFTERNOON is (1pm - 5pm).                                 
               """.format(data.get('evac_date'), data.get('evac_time'))

    pod_log = models.log.PodLog()
    pod_log.owner_key = unit.owner_key
    pod_log.item_key = unit.key
    pod_log.description = "Unit {} with owner {} cancelled by User, a delivery or client  visit was scheduled".format(
        unit.unit_name, unit.owner_key.get().name)
    pod_log.category = "CANCELLED"
    pod_log.put()

    services.sender.send_email(current_user.email, subj="Cancel POD!",
                               template_id=MAIL_JET_CANCEL_UNIT_TEMPLATE_ID,
                               variables={"pod_name": unit.unit_name,
                                          "client_visit": "{} {}".format(data.get("evac_date", "N/A"),
                                                                         data.get("evac_time", "")) if not data.get(
                                              'has_delivery') else 'N/A',
                                          "delivery_date_pickup": "{} {}".format(data.get("pickup_date", "N/A"),
                                                                                 data.get("pickup_time",
                                                                                          "")) if data.get(
                                              'has_delivery') else 'N/A',
                                          "delivery_date_dropoff": "{} {}".format(data.get("dropoff_date", "N/A"),
                                                                                  data.get("dropoff_time",
                                                                                           "")) if data.get(
                                              'has_delivery') else 'N/A'
                                          })
    communicator.send_sms(current_user.cell_no, message=crm_text)

    return dict(success=True,
                message="Your rent has been cancelled sucessfully,"
                                      " We have sent you an email with further instructions",
                charges=unpaid_charge_query.count())


def reserve_unit_for_user(user, data, unit_id=None):
    import requests
    from requests.auth import HTTPBasicAuth
    if unit_id:
        unit = models.unit.Unit.get_by_id(unit_id)
        logging.info('Admin reserving unit {}'.format(unit.unit_name))
    else:
        logging.info('Querying for available and unarchived unit of size {}'.format(data.get('size')))
        unit = models.unit.Unit.query(
            ndb.AND(models.unit.Unit.unit_size == models.unit.POD_SIZE[data["size"]],
                    models.unit.Unit.unit_type == data["unit_type"],
                    models.unit.Unit.available == True,
                    models.unit.Unit.archived == False)).get()

    if not unit:
        logging.error("Unit no longer available for user {}".format(user))
        return False

    price_today = float(data.get('quoted_price', 0))

    monthly = data.get('monthly')
    charges = list()
    calendar_event_pickup, calendar_event_dropoff, client_visit = None, None, None
    charge_details = "First monthly payment of R {}".format(price_today)

    unit.owner_key = user.key
    unit.available = False
    unit.price = price_today
    unit.message = data.get("message", '')
    unit.status = models.unit.AT_FACILITY

    if data.get("has_delivery", None):
        delivery_id = str("{}{}".format(uuid.uuid4(), datetime.datetime.now().strftime("%d%m%Y%H%S")))

        if data.get('existing_delivery', None):
            dropoff_event = models.system_settings.CalendarEvent.get_by_id(int(data['existing_delivery']))
            pickup_event = models.system_settings.CalendarEvent.query(
                models.system_settings.CalendarEvent.delivery_id == dropoff_event.delivery_id,
                models.system_settings.CalendarEvent.category == 'PICKUP').get()

            dropoff_event.item_keys.append(unit.key)
            pickup_event.item_keys.append(unit.key)
            dropoff_event.put()
            pickup_event.put()
            term_end = time_utils.get_first_of_next_month_given_date(dropoff_event.date)

        else:
            address = models.address.Address()
            coordinates = utils.geo.get_coordinates(str(data.get("address", '')))

            address.coordinates = ndb.GeoPt("{},{}".format(coordinates[0], coordinates[1]))
            address.full_name = utils.geo.get_full_address(str(data.get("address", '')))
            address.name = str(data.get("address", ''))
            address.owner_key = user.key

            unit.status = models.unit.PENDING_DROPOFF

            dod = datetime.datetime.strptime(data["dropoff_date"], "%d/%m/%Y")
            pud = datetime.datetime.strptime(data["pickup_date"], "%d/%m/%Y")

            calendar_event_pickup = models.system_settings.CalendarEvent()
            calendar_event_pickup.category = 'PICKUP'

            calendar_event_pickup.item_keys.append(unit.key)
            calendar_event_pickup.owner_key = user.key
            calendar_event_pickup.address = address
            calendar_event_pickup.delivery_id = delivery_id
            calendar_event_pickup.date = pud
            calendar_event_pickup.time_category = str(data.get('pickup_time'))
            calendar_event_pickup.user_comment = str(data.get('delivery_note'))

            calendar_event_dropoff = models.system_settings.CalendarEvent()
            calendar_event_dropoff.category = 'DROPOFF'

            calendar_event_dropoff.item_keys.append(unit.key)
            calendar_event_dropoff.address = address
            calendar_event_dropoff.delivery_id = delivery_id
            calendar_event_dropoff.owner_key = user.key
            calendar_event_dropoff.date = dod
            calendar_event_dropoff.time_category = str(data.get('dropoff_time'))
            calendar_event_dropoff.user_comment = str(data.get('delivery_note'))
            term_end = time_utils.get_first_of_next_month_given_date(dod)

    else:
        # Client choose to pack unit at HQ

        gate = models.system_settings.Gate.get_by_bs_key(unit.base_station_key)

        if gate:

            nold_data = {
                "guests": [
                    {
                        "email": user.email,
                        "name": user.name,
                        'phone': user.cell_no
                    }
                ],
                "devices": [
                    {
                        "_id": "5d24991c020e6c6b9294a055", #gate.id,
                        "relay": 1
                    }
                ],
                "key": {
                    "type": "online"
                }
            }
            auth = HTTPBasicAuth("5e197b86-086e-4d39-9b60-bc63ae347000",
                                 "7d699f74-c26f-46cb-b3a6-b3fef4a25862")
            # r = requests.post(auth=auth, url='https://api.nold.io/public/invite', json=nold_data)
            # print r.text

        pd = datetime.datetime.strptime(data.get("pack_date"), "%d/%m/%Y")

        client_visit = models.system_settings.CalendarEvent()
        client_visit.category = 'CLIENT_VISIT'
        client_visit.delivery_id = str("{}{}".format(uuid.uuid4(), datetime.datetime.now().strftime("%d%m%Y%H%S")))
        client_visit.item_keys.append(unit.key)
        client_visit.owner_key = user.key
        client_visit.date = pd
        client_visit.time_category = str(data.get('pack_time'))
        term_end = time_utils.get_first_of_next_month_given_date(pd)

    if not monthly:
        end_date = str("01/{}".format(data.get('end_date')))
        ed = datetime.datetime.strptime(end_date, "%d/%m/%Y")
        end_month = ed.month + 1
        term_end = datetime.datetime.strptime("01/" + str(end_month) + "/" + str(ed.year), "%d/%m/%Y")
        charge_details = "Once off payment of {}".format(price_today)

    unit.term_end = term_end
    unit.friendly_id = user.email
    logging.info('Putting unit {}'.format(unit))

    unit.last_order_date = datetime.datetime.now()
    unit.put()
    user.put()

    try:
        try:
            quote = models.charge.Quote.get_by_id(int((data.get('quote_id'))))
        except:
            quote = None

        charge = models.charge.Charge()
        if not monthly:
            charge.charge_type = models.charge.CHARGE_TYPE_ONCE
        charge.item_key = unit.key
        charge.owner_key = user.key
        charge.item_category = models.charge.ITEM_CATEGORY_UNIT
        charge.term_end = term_end
        charge.amount = price_today
        charge.details = quote.details if quote else charge_details
        if data.get('promo_code'):
            promo = models.charge.Promo.find_by_code(data.get('promo_code'))
            if promo:
                charge.details += " Promo applied {} {} % discount".format(promo.code, promo.discount_percent)
                promo.archived = True
                promo.put()

        if client_visit:
            client_visit.put()

        if data.get('has_labor', None):
            dcharge = models.charge.Charge()
            dcharge.item_category = models.charge.ITEM_CATEGORY_LABOR
            dcharge.item_key = unit.key
            dcharge.owner_key = user.key
            dcharge.amount = get_latest_labor_prices_by_size(unit.unit_size)
            dcharge.details = ' Labor cost of R{} for unit: {}'.format(
                dcharge.amount, unit.unit_name)
            logging.info('putting labor charge')
            if dcharge.amount > 1:
                dcharge.put()

        if data.get('lock_required', None):
            try:
                if data.get('lock') == 'lock2':
                    lock_cost = models.item.Item.get_by_id(5676305852923904).price
                else:
                    lock_cost = models.item.Item.get_by_id(5640101560320000).price
            except:
                lock_cost = 50.0
                pass

            dcharge = models.charge.Charge()
            dcharge.item_category = models.charge.ITEM_CATEGORY_LABOR
            dcharge.item_key = unit.key
            dcharge.owner_key = user.key
            dcharge.amount = lock_cost
            dcharge.details = ' Lock cost of R{} for unit: {}'.format(
                dcharge.amount, unit.unit_name)
            logging.info('putting lock charge')
            dcharge.put()

        if calendar_event_dropoff and calendar_event_pickup:
            delivery_price = float(data.get('delivery_price', 0))
            dcharge = models.charge.Charge()
            dcharge.item_category = models.charge.ITEM_CATEGORY_DELIVERY
            dcharge.item_key = unit.key
            dcharge.owner_key = user.key
            dcharge.amount = delivery_price
            dcharge.details = ' Delivery and Pickup cost of R{}'.format(
                dcharge.amount)
            logging.info('putting delivery charge')
            dcharge.put()
            calendar_event_pickup.charge_key = dcharge.key
            calendar_event_dropoff.charge_key = dcharge.key
            calendar_event_pickup.put()
            calendar_event_dropoff.put()
        logging.info('Putting charge {}'.format(charges))

        charge.put()

    except Exception as e:
        logging.error(e)
        logging.info('Resetting unit')
        unit.reset()
        return False

    try:
        services.bill_collector.send_invoice_for_charge(charge)
    except Exception as e:
        logging.info("Could not send invoice")
        logging.info(e)

    pod_log = models.log.PodLog()
    pod_log.owner_key = unit.owner_key
    pod_log.item_key = unit.key
    pod_log.description = "Unit {} reserved with owner {}".format(unit.unit_name, unit.owner_key.get().name)
    pod_log.category = "RENTED"
    pod_log.put()

    needs_insurance = False
    if data.get('insurance_options') == 'needs_insurance':
        needs_insurance = True

    services.sender.send_email(user.email, subj="PODit New POD!",
                               template_id=MAIL_JET_NEW_POD_TEMPLATE_ID,
                               variables={"pod_name": unit.unit_name,
                                          "order_date": unit.last_order_date.strftime("%d %B %Y"),
                                          "client_notes": unit.message,
                                          "client_visit": data.get("pack_date", "N/A") if not data.get(
                                              'has_delivery') else 'N/A',
                                          "delivery_date_pickup": data.get("pickup_date", "N/A") if data.get(
                                              'has_delivery') else 'N/A',
                                          "delivery_date_dropoff": data.get("dropoff_date", "N/A") if data.get(
                                              'has_delivery') else 'N/A',
                                          "insurance": "LINK TO INSURANCE" if needs_insurance else 'N/A'},
                               user_id=user.key.id())

    services.sender.send_sms(user.cell_no, (" Nice! You have reserved POD:{}\n\n"
                                            "You have 48 hours to complete payment.\n\n"
                                            "To change any particulars please go to your account, my pods & shops orders.\n"
                                            "There you can manage and/or cancel the POD. www.podit.co.za").format(
        unit.unit_name), user_id=user.key.id())

    return True


def edit_unit(data, unit_id):
    try:
        unit = models.unit.Unit.get_by_id(unit_id)

        dod = datetime.datetime.strptime("{} {}".format(data["dropoff_date"], data["dropoff_time"]),
                                         "%Y-%m-%d %H:%M")
        pud = datetime.datetime.strptime("{} {}".format(data["pickup_date"], data["pickup_time"]),
                                         "%Y-%m-%d %H:%M")
        unit.dropoff_date = dod
        unit.pickup_date = pud

        unit.message = data.get('note', '')
        # images = {}
        # for prop in request.files:
        #     image = request.files.get(prop)
        #     if image:
        #         images.update({str(prop): image})
        # for key, value in images.iteritems():
        #     file_name = '/podit-169717.appspot.com/pod-pictures/{}'.format(unit.key.id())
        #     image_file = gcs.open(file_name, mode='w', content_type='image/png',
        #                           options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
        #                                    }, retry_params=None)
        #     image_file.write(value.stream.read())
        #     image_file.close()
        #     unit.unit_picture = file_name
        unit.put()
    except Exception as e:
        logging.error('Error updating unit {}'.format(e))
        return False
    return True


def unit_to_dictionary(unit):
    owner_name = 'No owner'
    owner_id = ''
    owner_cell = 'N/A'
    client_email = 'N/A'
    pickup_dates = ''
    address = 'N/A'
    delivery_note = 'N/A'
    dropoff_dates = ''
    outstanding_amount = 0

    unpaid_charges = models.charge.Charge.query(
        models.charge.Charge.item_key == unit.key,
        models.charge.Charge.item_category == models.charge.ITEM_CATEGORY_UNIT,
        models.charge.Charge.paid == False,
        models.charge.Charge.archived == False
    ).fetch()
    for charge in unpaid_charges:
        outstanding_amount += charge.amount

    if unit.owner_key:
        owner = unit.owner_key.get()
        calendar_events = ss.CalendarEvent().query(ss.CalendarEvent.item_keys == unit.key,
                                                   ss.CalendarEvent.owner_key == owner.key,
                                                   ss.CalendarEvent.archived == False).order(
            ss.CalendarEvent.created).fetch()
        for ce in calendar_events:
            if ce.user_comment:
                delivery_note = ce.user_comment
            if ce.address:
                address = ce.address.full_name

            if ce.category == 'PICKUP':
                pickup_dates += "{} - {}{} STATUS: {}".format(ce.date.strftime("%A %d %m %Y"), ce.time_category,
                                                              ce.internal_datetime.strftime("%H:%M"), ce.status)
            if ce.category == 'DROPOFF':
                dropoff_dates += "{} - {}{} STATUS: {}".format(ce.date.strftime("%A %d %m %Y"), ce.time_category,
                                                               ce.internal_datetime.strftime("%H:%M"), ce.status)

        owner_cell = "{}.".format(owner.cell_no)
        client_email = owner.email
        owner_id = unit.owner_key.id()
        if owner.name:
            owner_name = owner.name

    return dict(id=unit.key.id(),
                archived=unit.archived,
                position_number=unit.position_number,
                owner_name=owner_name,
                delivery_note=delivery_note, client_email=client_email, client_cell=owner_cell, price=unit.price,
                created=unit.created + datetime.timedelta(hours=2),
                type_name=unit.type_name, logged_in=['LOGGED_OUT', 'LOGGED_IN'][unit.logged_in],
                address=address, size=models.unit.POD_SIZE_HUMANIZE[unit.unit_size], unit_name=unit.unit_name,
                pickup_date=pickup_dates, dropoff_date=dropoff_dates, status=models.unit.STATUS_TO_TEXT[unit.status],
                owner_id=owner_id, paid_till=unit.paid_till, unpaid_charges=outstanding_amount,
                position_numner=unit.position_number, last_order_date=unit.last_order_date,
                term_end=unit.term_end if unit.term_end else 'N/A')


def fetch_unit_dicts_by_filter(filters):
    cursor = request.args.get('p', None)
    if cursor == 'None' or not cursor:
        cursor = None
    else:
        cursor = Cursor(urlsafe=cursor)

    params = list()

    for name, value in filters.iteritems():
        params.append(getattr(models.unit.Unit, name) == value)

    units, next_curs, more = models.unit.Unit.query(*params).fetch_page(20, start_cursor=cursor)
    all_units = []
    for u in units:
        unit_dict = unit_to_dictionary(u)
        all_units.append(unit_dict)

    if more:
        next_curs = next_curs.urlsafe()
    else:
        next_curs = "None"

    return dict(units=all_units, next_page=next_curs)


def fetch_unit_dicts_by_search(search_term):
    index = models.unit.Unit.get_class_search_index()

    cursor = request.args.get('p', None)
    if cursor == 'None' or not cursor:
        cursor = search.Cursor()
    else:
        cursor = search.Cursor(web_safe_string=cursor)

    query_string = search_term
    results = index.search(query=search.Query(query_string,
                                              options=search.QueryOptions(
                                                  limit=20,
                                                  cursor=cursor,
                                                  sort_options=None))
                           )
    cursor = results.cursor
    if cursor:
        cursor_string = cursor.web_safe_string
    else:
        cursor_string = 'None'

    all_units = []
    # Populate dict with props of search results
    for scored_document in results.results:
        # Create dict containing props from search results
        results_dict = dict()
        for prop in scored_document.fields:
            results_dict[prop.name] = prop.value

        unit = models.unit.Unit.get_by_id(int(results_dict.get('unit_id')))
        if unit:
            unit_dict = unit_to_dictionary(unit)
            all_units.append(unit_dict)

    result_count = results.number_found
    return {'units': all_units, 'next_page': cursor_string, "total": result_count}


def fetch_unit_dicts_for_user(user_obj):
    owner_key = user_obj.key
    query = models.unit.Unit.query(models.unit.Unit.owner_key == owner_key).order(-models.unit.Unit.created)
    unitq = query.fetch()

    units = []

    for unit in unitq:
        charge_query = models.charge.Charge.query(models.charge.Charge.owner_key == owner_key,
                                                  models.charge.Charge.item_key == unit.key)
        unpaid_charge_query = models.charge.Charge.query(models.charge.Charge.owner_key == owner_key,
                                                         models.charge.Charge.item_key == unit.key,
                                                         models.charge.Charge.paid == False,
                                                         models.charge.Charge.archived == False)

        pickup_event = models.system_settings.CalendarEvent.get_pending_calendar_events(unit_id=unit.key.id(),
                                                                                        user_id=user_obj.key.id(),
                                                                                        category='PICKUP')

        if not pickup_event:
            pickup_event = models.system_settings.CalendarEvent.get_pending_calendar_events(unit_id=unit.key.id(),
                                                                                            user_id=user_obj.key.id(),
                                                                                            category='PICKUP_EVAC')

        dropoff_event = models.system_settings.CalendarEvent.get_pending_calendar_events(unit_id=unit.key.id(),
                                                                                         user_id=user_obj.key.id(),
                                                                                         category='DROPOFF')

        if not dropoff_event:
            dropoff_event = models.system_settings.CalendarEvent.get_pending_calendar_events(unit_id=unit.key.id(),
                                                                                             user_id=user_obj.key.id(),
                                                                                             category='DROPOFF_EVAC')

        has_delivery = False
        if pickup_event or dropoff_event:
            has_delivery = True
        may_cancel = True
        outstanding_amount = 0

        client_visit_date = models.system_settings.CalendarEvent.get_pending_calendar_events(unit_id=unit.key.id(),
                                                                                             user_id=user_obj.key.id(),
                                                                                             category='CLIENT_VISIT')
        if not client_visit_date:
            client_visit_date = models.system_settings.CalendarEvent.get_pending_calendar_events(unit_id=unit.key.id(),
                                                                                                 user_id=user_obj.key.id(),
                                                                                                 category='CLIENT_VISIT_EVAC')

        for charge in unpaid_charge_query.fetch():
            outstanding_amount += charge.amount

        if charge_query.count() > 1 and unpaid_charge_query.count() > 0:
            may_cancel = False

        if unit.status == "CANCELLED":
            may_cancel = False
        gate = models.system_settings.Gate.get_by_bs_key(unit.base_station_key)

        units.append(
            dict(has_gate=True if gate else False,
                 friendly_id=unit.friendly_id,
                 message=unit.message,
                 outstanding_charges=round(outstanding_amount, 2),
                 term_end=unit.term_end,
                 created=unit.created,
                 type_name=unit.type_name,
                 size=models.unit.POD_SIZE_HUMANIZE[unit.unit_size],
                 unit_name=unit.unit_name,
                 delivery_address=pickup_event.address.full_name if pickup_event and pickup_event.address else '-',
                 pickup_date=pickup_event.date.strftime("%a %d %b ") + pickup_event.time_category + {
                     "AFTERNOON": " (1pm - 5pm)"}.get(pickup_event.time_category,
                                                      '') if pickup_event else 'N/A',
                 dropoff_date=dropoff_event.date.strftime("%a %d %b ") + dropoff_event.time_category + {
                     "MORNING": " (8am - 12pm)"}.get(dropoff_event.time_category,
                                                     '') if dropoff_event else 'N/A',
                 status=models.unit.STATUS_TO_TEXT[unit.status], has_delivery=has_delivery,
                 unit_id=unit.key.id(),
                 image_url=unit.unit_picture, user_id=owner_key.id(), may_cancel=True,
                 visit_date=client_visit_date.date.strftime("%d %b %y") if client_visit_date else 'N/A',
                 paid_till=unit.paid_till if unit.paid_till else 'Not yet paid'))

    return units
