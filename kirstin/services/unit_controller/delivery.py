import datetime
import logging
import models.system_settings
import models.unit
import models.address
import utils.geo

import services.sender
import models.charge
import uuid
from google.appengine.ext import ndb


def calculate_delivery_cost(destination):
    SYSTEM_SETTINGS = models.system_settings.PoditSettings.fetch()
    coordinates = utils.geo.get_coordinates("{}, South Africa".format(str(destination)))
    if coordinates is None:
        return dict(success=False,
                    message="Only addresses in Stellenbosch allowed. Please provide the street name and "
                            "number")

    base_delivery_price = models.unit.Unit.get_delivery_cost()

    try:
        time, distance = utils.geo.get_time_and_distance(
            '{},{}'.format(SYSTEM_SETTINGS.podit_hq_lat, SYSTEM_SETTINGS.podit_hq_lon),
            '{},{}'.format(coordinates[0], coordinates[1]))
    except:
        return dict(success=False, message="Address not within 100KM from PODit HQ")

    distance_over_threshold = distance - SYSTEM_SETTINGS.min_distance * 1000  # To convert to m
    extra = 0
    if distance_over_threshold > 0:
        extra = distance_over_threshold * (float(SYSTEM_SETTINGS.per_km_cost) / 1000)

    delivery_price = round((base_delivery_price + extra), 2)

    # Check if the destinace is farther than the max delivery distance specified by PODit admins
    if not (distance < SYSTEM_SETTINGS.max_delivery_distance_range * 1000):
        return dict(success=False, message="Address not within 100KM from PODit HQ")

    return dict(success=True, delivery_price=delivery_price)


def validate_and_quote_unit_delivery_data(data, user=None, unit=None, client_packing=False):
    try:
        print client_packing
        print data
        SYSTEM_SETTINGS = models.system_settings.PoditSettings.fetch()

        cutoff = (SYSTEM_SETTINGS.booking_cutoff_days if SYSTEM_SETTINGS.booking_cutoff_days else 60)

        if data.get("pack_date", None) and client_packing:
            pack_date = datetime.datetime.strptime(data.get("pack_date"), "%d/%m/%Y")

            if (pack_date - datetime.datetime.now()).days > cutoff:
                return dict(success=False, message="The pack date is too far in advance, please select an earlier date")
            else:
                return dict(success=True, message="Valid packing date")

        address = data.get('address', None)

        coordinates = utils.geo.get_coordinates("{}, South Africa".format(str(address)))
        print coordinates
        print coordinates[0]
        print coordinates[1]
        if coordinates is None:
            return dict(success=False,
                        message="Only addresses in Stellenbosch allowed. Please provide the street name and "
                                "number")

        base_delivery_price = models.unit.Unit.get_delivery_cost()
        bs = None
        if data.get('base_station', None):
            bs = models.system_settings.BaseStation.get_by_id(int(data.get('base_station')))

        bs_coordinates = [SYSTEM_SETTINGS.podit_hq_lat, SYSTEM_SETTINGS.podit_hq_lon]

        if bs and bs.address:
            logging.info('Base station found')
            bs_coordinates = [bs.address.coordinates.lat, bs.address.coordinates.lon]

        try:
            time, distance = utils.geo.get_time_and_distance('{},{}'.format(bs_coordinates[0], bs_coordinates[1]),
                                                   '{},{}'.format(coordinates[0], coordinates[1]))
        except:
            return dict(success=False, message="Address not within 100KM from PODit HQ")

        distance_over_threshold = distance - SYSTEM_SETTINGS.min_distance*1000 # To convert to m
        extra = 0
        if distance_over_threshold > 0:
            extra = distance_over_threshold*(float(SYSTEM_SETTINGS.per_km_cost)/1000)

        delivery_price = round((base_delivery_price + extra), 2)

        # Check if the destinace is farther than the max delivery distance specified by PODit admins
        if not (distance < SYSTEM_SETTINGS.max_delivery_distance_range*1000):
            return dict(success=False, message="Address not within 100KM from PODit HQ")

        dropoff_date = datetime.datetime.strptime(data["dropoff_date"], "%d/%m/%Y")
        pickup_date = datetime.datetime.strptime(data["pickup_date"], "%d/%m/%Y")


        if (dropoff_date - datetime.datetime.now()).days >  cutoff or (pickup_date - datetime.datetime.now()).days > cutoff:
            return dict(success=False, message="The delivery date is too far in advance, please select an earlier date")

        if unit and user:
            if models.system_settings.CalendarEvent.query(models.system_settings.CalendarEvent.item_keys == unit.key,
                                                          models.system_settings.CalendarEvent.status == 'PENDING',
                                                            models.system_settings.CalendarEvent.category == 'DROPOFF',
                                                          models.system_settings.CalendarEvent.owner_key == user.key).get():
                return dict(success=False, message="There are currently ongoing deliveries for the POD,"
                                                   " please wait till they have completed before scheduling a new delivery")

        banned_days = [5, 6]
        # 6 sun, 5 sat, 4 fri 3 thu 2 wed 1 tue 0 mon

        # if dropoff_date.weekday() in banned_days or pickup_date.weekday() in banned_days:
        #     return dict(success=False, message="Dropoff and pickup dates cannot be Saturday nor Sunday")

        # Max hour of today
        max_today = datetime.datetime.today().replace(hour=datetime.datetime.max.hour,
                                                      minute=datetime.datetime.max.minute,
                                                      microsecond=datetime.datetime.max.microsecond)

        if dropoff_date <= max_today or pickup_date <= max_today:
            return dict(success=False, message="Dropoff and pickup need to placed for tomorrow or later")

        if dropoff_date > pickup_date:
            return dict(success=False, message="Drop off date needs to be earlier than the pick up date")

        if data.get('pickup_time') == 'MORNING' and data.get(
                'dropoff_time') == 'AFTERNOON' and dropoff_date == pickup_date:
            return dict(success=False, message="Drop off timeslot needs to be earlier than the pick up timeslot")

        dropoff_time_slot_count = models.system_settings.CalendarEvent.query(
            ndb.OR(models.system_settings.CalendarEvent.status == 'PENDING',models.system_settings.CalendarEvent.status == 'COMPLETED'),
            models.system_settings.CalendarEvent.time_category == data.get('dropoff_time'),
            models.system_settings.CalendarEvent.date == dropoff_date).count()

        pickup_time_slot_count = models.system_settings.CalendarEvent.query(
            ndb.OR(models.system_settings.CalendarEvent.status == 'PENDING',
                   models.system_settings.CalendarEvent.status == 'COMPLETED'),
            models.system_settings.CalendarEvent.time_category == data.get('pickup_time'),
            models.system_settings.CalendarEvent.date == pickup_date).count()

        if pickup_time_slot_count >= int(SYSTEM_SETTINGS.slot_limit):
            return dict(success=False,
                        message="We are too busy on that day to do your pickup, please select a different day")

        if dropoff_time_slot_count >= int(SYSTEM_SETTINGS.slot_limit):
            return dict(success=False,
                        message="We can pickup on that date, but unfortunately we are too busy on that day "
                                "to do your dropoff, please select a different dropoff date")

        return dict(success=True,
                    message="Address Valid",
                    delivery_price=delivery_price,
                    distance=float(distance)/1000,
                    base_price=SYSTEM_SETTINGS.delivery_cost,
                    per_km_cost=SYSTEM_SETTINGS.per_km_cost,
                    min_distance=SYSTEM_SETTINGS.min_distance)
    except Exception as e:
        return dict(success=False,
                    message="Error occured {}, please contact support".format(e))


def schedule_deliveries(data, unit_id, evac_delivery=False, price=None):
    unit = models.unit.Unit.get_by_id(unit_id)
    unit.cancel_calendar_events()

    delivery_id = str("{}{}".format(uuid.uuid4(), datetime.datetime.now().strftime("%d%m%Y%H%S")))
    user = unit.owner_key.get()

    validation_status = validate_and_quote_unit_delivery_data(data, user, unit)
    if not validation_status.get('success'):
        return validation_status

    address = models.address.Address()
    coordinates = utils.geo.get_coordinates(str(data.get("address", '')) + ', Stellenbosch, South Africa')

    address.coordinates = ndb.GeoPt("{},{}".format(coordinates[0], coordinates[1]))
    address.full_name = utils.geo.get_full_address(str(data.get("address", '')))
    address.name = str(data.get("address", ''))
    address.owner_key = user.key

    unit.status = models.unit.PENDING_DROPOFF

    dod = datetime.datetime.strptime(data["dropoff_date"], "%d/%m/%Y")
    pud = datetime.datetime.strptime(data["pickup_date"], "%d/%m/%Y")

    calendar_event_pickup = models.system_settings.CalendarEvent()
    if evac_delivery:
        calendar_event_pickup.category = 'PICKUP_EVAC'
    else:
        calendar_event_pickup.category = 'PICKUP'

    calendar_event_pickup.item_keys.append(unit.key)
    calendar_event_pickup.owner_key = user.key
    calendar_event_pickup.delivery_id = delivery_id
    calendar_event_pickup.address = address
    calendar_event_pickup.date = pud
    calendar_event_pickup.time_category = str(data.get('pickup_time'))
    calendar_event_pickup.user_comment = str(data.get('delivery_note'))

    calendar_event_dropoff = models.system_settings.CalendarEvent()
    if evac_delivery:
        calendar_event_dropoff.category = 'DROPOFF_EVAC'
    else:
        calendar_event_dropoff.category = 'DROPOFF'

    calendar_event_dropoff.item_keys.append(unit.key)
    calendar_event_dropoff.address = address
    calendar_event_dropoff.delivery_id = delivery_id
    calendar_event_dropoff.owner_key = user.key
    calendar_event_dropoff.date = dod
    calendar_event_dropoff.time_category = str(data.get('dropoff_time'))
    calendar_event_dropoff.user_comment = str(data.get('delivery_note'))

    charge = models.charge.Charge()
    charge.charge_type = models.charge.CHARGE_TYPE_ONCE
    charge.item_category = models.charge.ITEM_CATEGORY_DELIVERY
    charge.item_key = unit.key
    charge.owner_key = user.key
    charge.amount = price if price else validation_status.get('delivery_price', 1.0)
    charge.details = "Delivery cost for unit: {}".format(unit.unit_name)
    charge.put()

    unit.put()
    calendar_event_dropoff.charge_key = charge.key
    calendar_event_pickup.charge_key = charge.key
    calendar_event_dropoff.put()
    calendar_event_pickup.put()

    return dict(success=True, message="Successfully scheduled delivery for pod!")
