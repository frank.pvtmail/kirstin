import logging
import datetime
import uuid
import utils.time_utils as time_utils
import utils.geo
from models.unit import PENDING_DROPOFF, \
    PENDING_PICKUP, \
    POD_SIZE, POD_SIZE_HUMANIZE,\
    TYPES_HUMANIZE, \
    STATUS_TO_TEXT, \
    AT_FACILITY,\
    CANCELLED

from models.charge import CHARGE_TYPE, CHARGE_TYPE_MONTHLY, CHARGE_TYPE_ONCE, ITEM_CATEGORY_ORDER
import models.charge
import models.unit
import models.const
import models.item
import models.shopping_cart
import models.system_settings
import services.sender
import services.user_orchestrator
import models.order
import models.address
from flask import Flask, jsonify, request
from google.appengine.api import search
from google.appengine.datastore.datastore_query import Cursor
import lib.cloudstorage as gcs
from google.appengine.ext import ndb
import models.item


def admin_add_item(data, image_file):
    item = models.item.Item()
    item.image_urls = list()
    if image_file:
        file_name = '/podit-169717.appspot.com/item-pictures/{}'.format(uuid.uuid1())
        write_buffer = gcs.open(file_name, mode='w', content_type='image/png',
                                options={'x-goog-acl': 'public-read', 'cache-control': 'public, max-age=0'
                                         }, retry_params=None)
        write_buffer.write(image_file.stream.read())
        write_buffer.close()
        item.image_url = file_name
        #item.image_urls.append(file_name)

    item.name = data.get('name')
    item.usage = data.get('usage')
    item.quantity = int(data.get('stock'))
    item.description = data.get('description')
    item.category = models.item.ItemCategory.query(models.item.ItemCategory.name == data.get('category')).get().key
    item.price = round(float(data.get('price')), 2)
    item.put()
    return dict(success=True, category="success", message="Successfully added item")





def client_get_items_per_category(category):

    if not category:
        items = models.item.Item.query(models.item.Item.archived == False).fetch()
        item_dict = [x.to_dictionary() for x in items]

        return dict(items=item_dict, item_type="all")

    query = models.item.ItemCategory.query(models.item.ItemCategory.name == category, models.item.ItemCategory.archived == False)
    if not query.count():
        return dict(status="fail", message="No category found for type {}".format(category))

    items = models.item.Item.query(models.item.Item.archived == False, models.item.Item.category == query.fetch(1)[0].key).fetch()
    item_dict = [x.to_dictionary() for x in items]

    return dict(items=item_dict, item_type="{}".format(category.lower().title()))


def client_check_cart(user):
    shopping_cart = user.shopping_cart_key.get()
    if not shopping_cart:
        return dict(success=False, message="No cart")

    shopping_cart_items = [x.get() for x in shopping_cart.items]

    distinct_cart_items = list()
    distinct_cart_item_dicts = list()
    for i in shopping_cart_items:
        if i not in distinct_cart_items:
            distinct_cart_items.append(i)
            item_dict = i.to_dictionary()
            item_dict.update(count=shopping_cart_items.count(i))
            distinct_cart_item_dicts.append(item_dict)

    return dict(count=len(shopping_cart.items), status="success",
                items=distinct_cart_item_dicts, total="{:0.2f}".format(round(shopping_cart.calculate_total(), 2)))



def client_update_item_count(item_id, count, user):
    item = models.item.Item.get_by_id(int(item_id))
    shopping_cart = user.shopping_cart_key.get()
    shopping_cart_items = [x.get() for x in shopping_cart.items]

    if count < 0:
        return dict(success=False, message="Cannot have quantity smaller than 0")

    if count > item.quantity:
        return dict(success=False, message="There are only {} {} available, please select a different quantity".format(item.quantity, item.name))

    for i in shopping_cart_items:
        if i.key.id() == int(item_id):
            shopping_cart.items.remove(item.key)

    for i in range(0, count):
        shopping_cart.items.append(item.key)

    shopping_cart.put()
    return dict(success=True, message="Successfully update item count in cart")


def client_add_to_cart(item_id, user):
    item = models.item.Item.get_by_id(int(item_id))
    try:
        if item.quantity > 0:
            shopping_cart = user.shopping_cart_key.get()
            shopping_cart.items.append(item.key)
            shopping_cart.put()
        else:
            return dict(success=False, message="Item out of stock")
    except:
        logging.error('No cart for user')
    return dict(success=True, message="Successfully added item to cart", extra=True,
                extra_content="<a href='/shoppingcart'>    Click here to view cart</a>")


def client_remove_from_cart(item_id, user):
    item = models.item.Item.get_by_id(int(item_id))

    try:
        print  'removing items'
        shopping_cart = user.shopping_cart_key.get()
        shopping_cart_items = [x.get() for x in shopping_cart.items]

        for i in shopping_cart_items:
            if i.key.id() == item_id:
                shopping_cart.items.remove(i.key)
                break
        shopping_cart.put()
    except Exception as e:
        print e
        logging.error('No cart for user')
        return dict(success=False, message="Problem removing cart item {}".format(e))
    return dict(success=True, message="Successfully removed item from cart", count=len(shopping_cart.items),
                items=[x.get().to_dictionary() for x in shopping_cart.items], total=shopping_cart.calculate_total())


def create_order(user, data=None, admin=False):
    """
    THis function creates a charge and order from a users shopping cart
    :param data: Id of the order to cancell AKA archvied
    :param user: Id of the order to cancell AKA archvied
    :return:
    """
    event_id = None
    order = None
    print data
    check_out_message = "contact support"
    if data:

        cell_no_validation = services.user_orchestrator.cell_number_validator(data.get('cell_no', None))
        if not cell_no_validation.get('success') and not admin:
            return cell_no_validation

        event_id = data.get('existing_delivery_id')
        if event_id:
            check_out_message, order = checkout(user.key.id(), event_id=event_id)

        else:
            # Client wants to collect
            try:
                pd = datetime.datetime.strptime(data["pack_date"], "%d/%m/%Y")

                client_visit = models.system_settings.CalendarEvent()
                client_visit.category = 'CLIENT_VISIT'
                client_visit.delivery_id = str("{}{}".format(uuid.uuid4(), datetime.datetime.now().strftime("%d%m%Y%H%S")))
                client_visit.owner_key = user.key
                client_visit.date = pd
                client_visit.time_category = str(data.get('pack_time'))
            except:
                return dict(success=False, message="Problem creating your order, please check input data for the collection",
                            extra=False)
            check_out_message, order = checkout(user.key.id(), event_id=event_id)
            client_visit.item_keys.append(order.key)
            client_visit.put()

    if order:
        item_names = list()
        for i in order.to_dictionary().get('item_dicts'):
            item_names.append({"x": {"name": i.get('name'), "price": i.get('price')}})

        services.sender.send_email(to=user.email, subj="PODit New Order!",
                                   template_id=models.const.MAIL_JET_NEW_ORDER_TEMPLATE_ID,
                                   variables={"order_ref": order.order_ref, "items": item_names,
                                              "total": order.to_dictionary().get('total_charge'),
                                              "client_name": user.name,
                                              "client_cell_no": user.cell_no,
                                              "client_address": user.address.full_name  if user.address else 'N/A',
                                              "client_email": user.email
                                              },  user_id=user.key.id())
        return dict(success=True, message="Successfully created order, please settle your outstanding charges", extra=True,
                extra_content = "<a href='/payments'>    HERE</a>")
    else:
        return dict(success=False, message="Problem creating your order, {}".format(check_out_message), extra=False)


def client_cancel_order(order_id):
    """
    THis function cancels the order and archvied any related charges
    :param order_id: Id of the order to cancell AKA archvied

    :return:
    """
    try:
        order = models.order.Order.get_by_id(order_id)
        if order.status == "CANCELLED":
            return dict(success=False, message="Order already cancelled", category="error")

        order.status = "CANCELLED"
        order.archived = True
        if order.paid:
            for key in order.items:
                if key:
                    i = key.get()
                    i.sold_count -= 1
                    i.quantity = i.quantity + 1
                    i.put()

        charge = models.charge.Charge().query(models.charge.Charge.item_key == order.key).get()
        charge.archived = True

        calendar_events = models.system_settings.CalendarEvent.query(models.system_settings.CalendarEvent.item_keys == order.key).fetch()
        for e in calendar_events:
            if e.category == "CLIENT_VISIT":
                e.status = "CANCELLED"
            else:
                e.item_keys.remove(order.key)

            e.updated = True
            e.put()

        charge.put()
        order.put()

    except Exception as e:
        return dict(success=False, message="Error {}".format(e), category="error")
    owner = order.owner_key.get()
    item_names = list()
    for i in order.to_dictionary().get('item_dicts'):
        item_names.append({"x": {"name": i.get('name'), "price": i.get('price')}})
    services.sender.send_email(to=owner.email, subj="PODit Cancel Order",
                               template_id=models.const.MAIL_JET_CANCEL_ORDER_TEMPLATE_ID,
                               variables={"order_ref": order.order_ref,
                                          "items": item_names,
                                          "total": order.to_dictionary().get('total_charge'),
                                          "client_name": owner.name,
                                          "client_cell_no": owner.cell_no,
                                          "client_address": owner.address.full_name if owner.address else 'N/A',
                                          "client_email": owner.email
                                          })
    return dict(success=True, message="Successfully cancelled order ", category="success")


def checkout(user_id, event_id=None):
    """
    Fetches the user's shopping cart and creates a charge based on the total amount of the cart.
    :param user_id:The ndb user id used to fetch the user's shopping cart
    :return:
    """
    try:
        owner_key = ndb.Key('User', user_id)

        shopping_cart = models.shopping_cart.ShoppingCart.query(models.shopping_cart.ShoppingCart.owner_key == owner_key).get()
        if not shopping_cart:
            user = owner_key.get()
            shopping_cart = user.shopping_cart_key.get()

        if not shopping_cart:
            logging.error("Shopping cart does not exist for user id {}".format(user_id))
            return "Shopping cart does not exist" , None

        print shopping_cart
        order, status = models.order.Order.create_from_shopping_cart(shopping_cart)
        print order

        if order:
            charge = models.charge.Charge()
            charge.charge_type = CHARGE_TYPE['ONCE']
            charge.item_key = order.key
            charge.details = "Charge for Order # {}".format(order.order_ref)
            charge.item_category = ITEM_CATEGORY_ORDER
            charge.owner_key = owner_key
            charge.amount = order.calculate_total()
            charge.put()
            logging.info("Successfully added charge for client order {}".format(order.key.id()))
            if event_id:
                calendar_event = models.system_settings.CalendarEvent.get_by_id(int(event_id))
                if calendar_event:
                    logging.info("adding order id {} to calendar event id {}".format(order.key.id(), event_id))
                    calendar_event.item_keys.append(order.key)
                    calendar_event.put()
        else:
            return status, None
    except Exception as e:

        logging.error('Failed to checkout: {}'.format(e))
        return "Problem occurred, contact PODit support", None
    return status, order
