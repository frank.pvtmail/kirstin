import logging
import lib.cloudstorage as gcs
import services.sender
from google.appengine.api import runtime
from models.charge import Charge, EFTPayment, CreditCardPayment
import services.unit_controller
import models.unit
import utils.time_utils as tt
import models.item
import models.order
import models.const
import datetime
import uuid
import csv


def export_entity(entity_kind='Units', sd="2018-01-01", ed="2018-01-01", email_list=None, filters=None):
    """
    This function exports entities to CSV format
    :param sd: start date in string format
    :param ed: end date in string format
    :param email: email to send the export to
    :return:
    """

    export_uid = str(uuid.uuid1())[:5]

    date_format = "%Y-%m-%d"
    query = None
    query_filters = list()

    queries = list()
    if entity_kind == 'AllPayment':
        query = EFTPayment.query()
        queries.append(CreditCardPayment.query())

    if entity_kind == 'EFTPayment':
        query = EFTPayment.query()

    if entity_kind == 'CreditCardPayment':
        query = CreditCardPayment.query()

    if entity_kind == 'Charge':

        if filters:
            for k, v in filters.iteritems():
                query_filters.append(getattr(Charge, k) == v)

        query = Charge.query(*query_filters)

    if entity_kind == 'Unit':
        query = models.unit.Unit().query()

    if entity_kind == 'Item':
        query = models.item.Item().query()

    if entity_kind == 'Order':
        query = models.order.Order().query()

    queries.append(query)

    if not query:
        raise ValueError('Unsupported entity')

    field_name_dict = {
        'Item': ['quantity', 'sold_count', 'shipped_count', 'name', 'price', 'description'],
        'Order': ['items', 'paid', 'total_charge', 'item_count', 'created', 'owner_email', 'owner_name'],
        'AllPayment': ['owner_name', 'total', 'created', 'id', 'details',
                       'unique_id', 'payment_type', 'merchant_ref', 'holder'],
        'EFTPayment': ['owner_name',  'total', 'created', 'id', 'details'],
        'CreditCardPayment': ['owner_name', 'total', 'created', 'id', 'details'],
        'Charge': ['eft_id',
                    'peach_eft_key',
                   'peach_id',
                   'amount',
                   'unit_name',
                   'created',
                   'owner_name',
                   'owner_id',
                   'unit_id',
                   'id',
                   'details',
                   'merchant_ref',
                   'pay_date',
                   'status',
                   'archived'],
        'Unit': [
            'id',
            'owner_name',
            'client_cell',
            'owner_id',
            'client_email',
            'unpaid_charges',
            'position_number',
            'created',
            'type_name',
            'size',
            'unit_name',
            'pickup_date',
            'dropoff_date',
            'address',
            'delivery_note',
            'status',
            'price',
            'paid_till',
        'logged_in']

    }

    fieldnames = field_name_dict.get(entity_kind)
    file_path = '/podit-169717.appspot.com/{}_csvs/{}/{}{}.csv'.format(entity_kind, export_uid,
                                                                            datetime.datetime.now().strftime("D_%d-%m-%Y_T%Hh%Mm"),
                                                                            entity_kind)
    fp = gcs.open(file_path, mode='w',
                  content_type='text/csv',
                  options={'x-goog-acl': 'public-read'}, retry_params=None)
    a = csv.DictWriter(fp, delimiter=',', fieldnames=fieldnames)

    a.writerow(dict((fn, fn) for fn in fieldnames))
    logging.info("Writing db data to csv")

    for q in queries:
        for e in q.iter():
            logging.info("Memory Usage: \n")
            logging.info(runtime.memory_usage())

            if entity_kind == 'EFTPayment' or entity_kind == "CreditCardPayment" or entity_kind == 'AllPayment':
                entity_to_dict = e.to_dictionary()
            elif entity_kind == 'Unit':
                entity_to_dict = services.unit_controller.unit_to_dictionary(e)
            elif entity_kind == 'Charge':
                entity_to_dict = e.to_dictionary()
            elif entity_kind == 'Item':
                entity_to_dict = e.to_dictionary()
            elif entity_kind == 'Order':
                entity_to_dict = e.to_dictionary()
                del entity_to_dict['owner_key']

            for k, v in entity_to_dict.copy().iteritems():
                if k not in fieldnames:
                    entity_to_dict.pop(k, None)

            a.writerow(entity_to_dict)

    fp.close()
    logging.info("Attempting to send export email to admins")
    for email in email_list:



        services.sender.send_email(to=email, subj="PODit Admin export {}".format(tt.za_time_now().strftime("%d%b%y")),
                                template_id=models.const.MAIL_JET_ADMIN_EXPORT_TEMPLATE_ID,
                                variables={"export_link": 'https://storage.googleapis.com{}'.format(file_path)})
    return True


def export_payments(sd, ed, email):
    """
    This function exports Charge entities between a start_date and end_date
    :param sd: start date in string format
    :param ed: end date in string format
    :param email: email to send the export to
    :return:
    """
    id = str(uuid.uuid1())[:5]

    start_date = datetime.datetime.strptime(sd, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(ed, "%Y-%m-%d")
    logging.info("querying for charges between {} and {}".format(start_date, end_date))
    cursor = None
    query = Charge.query(Charge.created >= start_date, Charge.created <= end_date).order(
        -Charge.created,
        Charge.key)
    logging.info("Number of charges {}".format(query.count()))

    payments = []
    while True:
        logging.info("Memory Usage: \n")
        logging.info(runtime.memory_usage())
        batch, next_curs, more = query.fetch_page(100, start_cursor=cursor)
        if next_curs:
            cursor = next_curs
        payments += batch
        if not next_curs:
            break

    payments_dict = [x.to_dictionary() for x in payments]
    fieldnames = ['peach_id', 'unit_name', 'amount', 'created', 'owner_name', 'owner_id', 'unit_id', 'id', 'pay_date',
                  'status']
    fp = gcs.open('/podit-169717.appspot.com/payment_csvs/{}/payments.csv'.format(id), mode='w',
                  content_type='text/csv',
                  options={'x-goog-acl': 'public-read'}, retry_params=None)
    a = csv.DictWriter(fp, delimiter=',', fieldnames=fieldnames)
    a.writerow(dict((fn, fn) for fn in fieldnames))
    logging.info("writing to file")
    for row in payments_dict:
        a.writerow(row)
    fp.close()
    logging.info("sending email to {}".format(email))
    communicator.send_email(email, 'Download the csv here: {}'.format(
        'https://storage.googleapis.com/podit-169717.appspot.com/payment_csvs/{}/payments.csv'.format(id)))
    return True
