#!/bin/bash
osascript -e 'tell app "Terminal"
   do script "cd dev/podit; yes | gcloud app deploy app.yaml --project=podit-169717 --no-promote --version=test; exit"
   do script "cd dev/podit; yes | gcloud app deploy admin.yaml --project=podit-169717 --no-promote --version=test; exit"
   do script "cd dev/podit; yes | gcloud app deploy client.yaml --project=podit-169717 --no-promote --version=test;  exit"
   do script "cd dev/podit; yes | gcloud app deploy cronworker.yaml --project=podit-169717 --no-promote --version=test; printf '\7';  exit"
end tell'
