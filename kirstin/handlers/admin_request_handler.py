import ast
import calendar
import datetime
import json
import logging
import uuid
from functools import wraps

import lib.flask_cors
import google.auth.transport.requests
import google.oauth2.id_token
import requests
import requests_toolbelt.adapters.appengine
from flask import Flask, jsonify, request
from google.appengine.api import search
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext import deferred
from google.appengine.ext import ndb
from lib.validate_email import validate_email

import lib.cloudstorage as gcs
import utils.geo as geo
import utils.time_utils as time_utils
from models.address import Address
from models.charge import Charge, CHARGE_TYPE
from models.item import Item, ItemCategory
from models.order import Order
from models.shopping_cart import ShoppingCart
from models.system_settings import SystemSettings, CalendarEvent, PoditSettings
from models.unit import Unit, STATUS_TO_TEXT, POD_SIZE_HUMANIZE, POD_SIZE, AT_FACILITY, PENDING_DROPOFF, CANCELLED
import models.user

import services.exporter
from services.sender import client as communicator
from services.user_orchestrator import client as user_manager
from services.unit_controller import client as unit_manager
import services.seven_eleven

def export_entities(session_token):
    sd = request.args.get('sd', '2018-01-01')
    ed = request.args.get('ed', '2018-01-01')
    print session_token
    try:
        services.exporter.export_entity(entity_kind=request.args.get('entity'),
                                        email_list=[session_token.get('current_admin').email, "podit.build@gmail.com"], sd=sd, ed=ed)
    except Exception as e:
        return jsonify(dict(success=False, message="Something went wrong: {}".format(e)))

    return jsonify(dict(success=True, message="An email will be sent to {} containing a link to the {} export".format(
        session_token.get('current_admin').email,
        request.args.get('entity'))))


def  set_as_admin(session_token):
    data = request.get_json()
    user = models.user.User.get_by_id(int(data.get('user_id')))
    return jsonify(user_manager.toggle_admin_status(user))


def set_unit_prices(session_token):
    SYSTEM_SETTINGS = PoditSettings.fetch()
    """
       MINI_POD_PRICE = 225
    POD_PRICE = 325
    LARGE_POD_PRICE = 425
    JUMBO_POD_PRICE = 500"""
    try:
        prices = request.get_json()
        for k, v in prices.iteritems():
            try:
                float(v)
            except ValueError:
                raise ValueError('price is not numeric')



        SYSTEM_SETTINGS.MINI_POD_PRICE = float(prices.get('mp'))
        SYSTEM_SETTINGS.POD_PRICE = float(prices.get('p'))
        SYSTEM_SETTINGS.B_PRICE = float(prices.get('sb'))

        SYSTEM_SETTINGS.LARGE_POD_PRICE = float(prices.get('lp'))
        SYSTEM_SETTINGS.JUMBO_POD_PRICE = float(prices.get('jp'))
        SYSTEM_SETTINGS.HC_PRICE = float(prices.get('hc'))
        SYSTEM_SETTINGS.C_PRICE = float(prices.get('c'))
        SYSTEM_SETTINGS.delivery_cost = float(prices.get('delivery_cost'))
        SYSTEM_SETTINGS.per_km_cost = float(prices.get('per_km_cost'))
        SYSTEM_SETTINGS.min_distance = float(prices.get('min_distance'))
        SYSTEM_SETTINGS.max_delivery_distance_range = float(prices.get('max_distance'))
        SYSTEM_SETTINGS.slot_limit = int(prices.get('slot_limit'))
        SYSTEM_SETTINGS.booking_cutoff_days = int(prices.get('cutoff'))
        SYSTEM_SETTINGS.JUMP_POD_LABOR = round(float(prices.get('jp_lb_cost')), 2)
        SYSTEM_SETTINGS.C_LABOR = round(float(prices.get('c_lb_cost')), 2)
        SYSTEM_SETTINGS.HC_LABOR = round(float(prices.get('hc_lb_cost')), 2)

        SYSTEM_SETTINGS.put()
        return jsonify({"success": True,
                        "message": "Successfully updated prices"})
    except Exception as e:
        return jsonify({"success": False,
                        "message": "Something went wrong: {}".format(e)})


def fetch_unit_prices(session_token):
    SYSTEM_SETTINGS = PoditSettings.fetch()
    return jsonify({"success": True, "MP": SYSTEM_SETTINGS.MINI_POD_PRICE,
                    "P": SYSTEM_SETTINGS.POD_PRICE,
                    "LP": SYSTEM_SETTINGS.LARGE_POD_PRICE,
                    "JP": SYSTEM_SETTINGS.JUMBO_POD_PRICE,
                    "HC": SYSTEM_SETTINGS.HC_PRICE,
                    "C": SYSTEM_SETTINGS.C_PRICE,
                    "B": SYSTEM_SETTINGS.B_PRICE,

                    "delivery_cost": SYSTEM_SETTINGS.delivery_cost,
                    "slot_limit": SYSTEM_SETTINGS.slot_limit,
                    'min_distance': SYSTEM_SETTINGS.min_distance,
                    "per_km_cost": SYSTEM_SETTINGS.per_km_cost,
                    'max_distance': SYSTEM_SETTINGS.max_delivery_distance_range,
                    'cutoff': SYSTEM_SETTINGS.booking_cutoff_days,
                    'jp_lb_cost': SYSTEM_SETTINGS.JUMBO_POD_LABOR,
                    'c_lb_cost': SYSTEM_SETTINGS.C_LABOR,
                    'hc_lb_cost': SYSTEM_SETTINGS.HC_LABOR})


def fetch_unit_sizes():
    return jsonify(dict(success=True, sizes=POD_SIZE_HUMANIZE.values()))


def fetch_stats():
    total = Unit.query().count()
    occupied = Unit.query(Unit.owner_key != None).count()
    XXS = XS = S = M = L = XL = 0
    mp_count = 0
    sp_count = 0
    lp_count = 0
    jp_count = 0
    mp_count_occ = 0
    sp_count_occ = 0
    lp_count_occ = 0
    jp_count_occ = 0
    logged_in = Unit.query(Unit.logged_in == True).count()
    logged_out = Unit.query(Unit.logged_in == False).count()
    if occupied:
        mp_count_occ = Unit.query(Unit.unit_size == 0, Unit.owner_key != None).count()
        sp_count_occ = Unit.query(Unit.unit_size == 1, Unit.owner_key != None).count()
        lp_count_occ = Unit.query(Unit.unit_size == 2, Unit.owner_key != None).count()
        jp_count_occ = Unit.query(Unit.unit_size == 3, Unit.owner_key != None).count()

        mp_count = Unit.query(Unit.unit_size == 0).count()
        sp_count = Unit.query(Unit.unit_size == 1).count()
        lp_count = Unit.query(Unit.unit_size == 2).count()
        jp_count = Unit.query(Unit.unit_size == 3).count()

        XXS = int(float(mp_count_occ) / occupied * 100)
        XS = int(float(sp_count_occ) / occupied * 100)
        S = int(float(lp_count_occ) / occupied * 100)
        M = int(float(jp_count_occ) / occupied * 100)

    vacant = total - occupied

    percentage_unoccupied = int(float(vacant) / total * 100)
    percentage_occupied = int(float(occupied) / total * 100)

    return jsonify({"occupancy": [percentage_occupied, percentage_unoccupied],
                    "unit_counts":[mp_count_occ, sp_count_occ, lp_count_occ, jp_count_occ],
                    "unit_totals": [mp_count, sp_count, lp_count, jp_count],
                    "sizes": [XXS, XS, S, M], "total": total,
                    "logged_out":logged_out , "logged_in":logged_in })


def add_units(data):

    return jsonify(unit_manager.bulk_add_units(pod_size_text=data['prefix'], position_number=data['position_number'], name=data['unit_name']))



def add_item(data, image_file):
    return services.seven_eleven.admin_add_item(data, image_file)


def units_by_filter(filter):
    result_dict = unit_manager.fetch_unit_dicts_by_filter(filter)
    return jsonify(result_dict)


def unit_search(search_term):
    result_dict = unit_manager.fetch_unit_dicts_by_search(search_term)
    return jsonify(result_dict)


def user_search(search_term):
    result_dict = user_manager.fetch_user_dicts_by_search(search_term)
    return jsonify(result_dict)


def unpaid_users():
    query = models.user.User.query(models.user.User.unpaid_charges > 0.0).order(-models.user.User.unpaid_charges)

    result_dict = user_manager.fetch_user_dicts_by_query(query, 'all')
    return jsonify(result_dict)


def archived_users():
    query = models.user.User.query(models.user.User.archived == True)
    result_dict = user_manager.fetch_user_dicts_by_query(query)
    return jsonify(result_dict)


def recently_online_users():
    query = models.user.User.query(models.user.User.archived == False).order(-models.user.User.last_seen)
    result_dict = user_manager.fetch_user_dicts_by_query(query)
    return jsonify(result_dict)


def fetch_user_info(session_token):
    user_id = request.args.get('user_id', None)

    u = models.user.User.get_by_id(user_id)
    return jsonify({"name": u.name, "email": u.email, "cell_no": u.cell_no, "address": str(u.address.full_name)})
