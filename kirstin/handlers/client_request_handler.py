import ast
import calendar
import datetime
import json
import logging
import uuid
from functools import wraps

import lib.flask_cors
import google.auth.transport.requests
import google.oauth2.id_token
import requests
import lib.requests_toolbelt.adapters.appengine
from flask import Flask, jsonify, request
from google.appengine.api import search
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext import deferred
from google.appengine.ext import ndb
from lib.validate_email import validate_email

import lib.cloudstorage as gcs
import utils.geo as geo
import utils.time_utils as time_utils
from models.address import Address
from models.charge import Charge, CHARGE_TYPE, Promo
from models.item import Item, ItemCategory
from models.order import Order
from models.shopping_cart import ShoppingCart
from models.system_settings import SystemSettings, CalendarEvent
from models.unit import Unit, STATUS_TO_TEXT, POD_SIZE_HUMANIZE, POD_SIZE, AT_FACILITY, PENDING_DROPOFF, CANCELLED
import models.user
import models.item
from services.exporter import client as exporter
from services.sender import client as communicator
import services.user_orchestrator
import services.unit_controller
import services.seven_eleven
import services.classifier

def search_ads(term):
    """Implements a verification of the user and creates a user profile if one does not already exist

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

    Returns:
        JSON Object: Information about the users profile.

    """

    return services.classifier.fetch_ad_dicts_by_search(term)


def add_ad(image_files, data, owner):
    """Implements a verification of the user and creates a user profile if one does not already exist

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

    Returns:
        JSON Object: Information about the users profile.

    """


    return services.classifier.client_add_ad(data, image_files, owner)




def verify_user(session_token, admin):
    """Implements a verification of the user and creates a user profile if one does not already exist

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

    Returns:
        JSON Object: Information about the users profile.

    """

    firebase_token = session_token.get('firebase_token')
    return services.user_orchestrator.verify_user(firebase_token, admin)


def delivery_data_validation(session_token):
    data = request.get_json()
    print data

    if data.get('existing_delivery', None):
        if data.get('size') not in ["C", "HC"]:
            return jsonify(dict(success=True, existing_delivery=True))
        else:
            return jsonify(dict(success=False, message="Deliveries are not possible with that select unit size"))

    response = services.unit_controller.validate_and_quote_unit_delivery_data(data, client_packing=True if data.get(
        'has_delivery') == 'false' or data.get('has_delivery') == False else False)

    if data.get('has_labor', None):
        labor_cost = services.unit_controller.get_latest_labor_prices_by_size(POD_SIZE.get(data.get('size'), 0))
        response.update({'labor_cost': labor_cost})

    if data.get('lock_required', None):
        try:
            if data.get('lock') == 'lock2':
                lock_cost = models.item.Item.get_by_id(5676305852923904).price
            else:
                lock_cost = models.item.Item.get_by_id(5640101560320000).price
        except:
            lock_cost = 50.0
            pass

        response.update({'lock_cost': lock_cost})

    return jsonify(response)


def validate_unit_reservation_data(session_token):
    data = request.get_json()
    if data.get('size', None):
        cell_no_validation = services.user_orchestrator.cell_number_validator(data.get('cell_no', None))
        if not cell_no_validation.get('success'):
            return jsonify(cell_no_validation)

        if Unit.query(ndb.AND(Unit.unit_size == POD_SIZE[data.get('size')], Unit.available == True)).count() < 1:
            return jsonify(dict(success=False, message="That pod size is not available anymore"))

        if data.get('has_delivery') and data.get('size') in ["C", "HC"]:
            return jsonify(dict(success=False, message="Deliveries are not possible with that select unit size"))

        if data.get('has_delivery') and not data.get('existing_delivery'):
            return jsonify(services.unit_controller.validate_and_quote_unit_delivery_data(data))
        else:
            current_user = session_token.get('current_user')
            current_user.cell_no = cell_no_validation.get('cell_no')
            current_user.put()
            return jsonify(dict(success=True, message="Validation passed", cell_no=cell_no_validation.get('cell_no')))

    else:
        return jsonify(dict(success=False, message="Size info must be supplied"))


def cancel_rent_handler(session_token):
    """Cancel unit and schedule a possible drop off

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

    Returns:
        JSON Object: Information about the status of the operation

    """
    current_user = session_token.get('current_user')

    return jsonify(services.unit_controller.cancel_rent_and_schedule_return(current_user, request.get_json()))


def fetch_user_details(session_token):
    """Retrieve the current users information

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

    Returns:
        JSON Object: Information about the users profile.

    """
    current_user = session_token.get('current_user')
    address = ''
    if current_user.address:
        address = current_user.address.full_name

    return jsonify({"success": True, "business_name": current_user.business_name, "vat_number": current_user.vat_number,
                    "name": current_user.name, "email": current_user.email, "cell_no": current_user.cell_no,
                    "address": address, 'id': str(current_user.key.id())})


def edit_user(session_token):
    """Modifies the current logged in users profile

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

    Returns:
        JSON Object: Status information about the operation

    """

    data = request.get_json()
    u = session_token.get('current_user')
    return services.user_orchestrator.edit_user(data=data, user=u)


def get_current_user_units(session_token):
    """Fetches a list of units that the current user owns

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User Object: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

     Returns:
         List [Dict]: information about the users units

     Raises:
         None
     """
    current_user = session_token.get('current_user')

    units = services.unit_controller.fetch_unit_dicts_for_user(current_user)
    if len(units) == 0:
        return jsonify({"status": "failure"})
    return jsonify({"status": "success", "units": units})


def unit_quote(session_token):
    """Get quotes based on parameters given : size, term length

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User Object: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

     Returns:
         JSON Object: information about the added unit

     Raises:
         None
     """
    hsize = request.args.get('size')
    monthly = request.args.get('monthly')
    pack_date = request.args.get('pack_date')
    promo_code = request.args.get('promo_code')
    dropoff_date = request.args.get('dropoff_date')
    has_delivery = request.args.get('has_delivery')
    existing_delivery = request.args.get('existing_delivery')

    print has_delivery
    print pack_date
    print dropoff_date
    discount = 1
    print 'EXISTING DELIVERY'
    print existing_delivery
    print type(existing_delivery)

    if has_delivery == 'true':
        try:
            if existing_delivery.isdigit():
                dropoff_event = CalendarEvent.get_by_id(int(existing_delivery))
                if dropoff_event:
                    start_date = dropoff_event.date.strftime("%d/%m/%Y")
                else:
                    start_date = dropoff_date
            else:
                start_date = dropoff_date
        except:
            start_date = dropoff_date
    else:
        start_date = pack_date

    end_date = request.args.get('end_date')

    monthly = False if monthly == 'false' else True
    size = POD_SIZE[hsize]
    price, price_per_month, delivery_price, months, pro_rata_this_month, quote_id = services.unit_controller.get_unit_quote(
        size=size, monthly=monthly, end_date=end_date, start_date=start_date)

    response_data = dict(success=True, price_monthly=round(price_per_month, 2),
                         price_today=round(price, 2),
                         start_date=start_date, end_date=end_date,
                         delivery_price=delivery_price, num_months=months,
                         pro_rata_this_month=round(pro_rata_this_month, 2),
                         quote_id=quote_id)

    if promo_code:
        if not monthly:
            response_data.update(promo_fail="Error: Promo codes only applicable when doing a monthly payment")
        else:
            promo = Promo.query(Promo.code == promo_code, Promo.archived == False).get()
            if promo:
                if (datetime.datetime.now() - datetime.timedelta(minutes=1)) > promo.created:
                    response_data.update(promo_fail="Error: Promo code expired")
                else:
                    discount = (100.0 - promo.discount_percent) / 100.0
                    response_data.update(promo_price=round(round(price, 2) * discount, 2))
            else:
                response_data.update(promo_fail="Error: Promo code not found")

    return jsonify(response_data)


def cancel_order(order_id):
    return jsonify(services.seven_eleven.client_cancel_order(order_id))


def create_order(session_token):
    return jsonify(services.seven_eleven.create_order(session_token.get('current_user'), data=request.get_json()))


def get_items_per_category():
    return jsonify(services.seven_eleven.client_get_items_per_category(request.args.get('item_category', None)))


def inspect_client_cart(session_token):
    return jsonify(services.seven_eleven.client_check_cart(session_token.get('current_user')))


def update_item_count(session_token, item_id, count):
    return jsonify(services.seven_eleven.client_update_item_count(item_id, count, session_token.get('current_user')))


def add_to_cart(item_id, session_token):
    return jsonify(services.seven_eleven.client_add_to_cart(item_id, session_token.get('current_user')))


def remove_from_cart(item_id, session_token):
    return jsonify(services.seven_eleven.client_remove_from_cart(item_id, session_token.get('current_user')))


def reserve_unit(session_token):
    """Fetches an available unit from the database and assigns the logged in user to that unit
    initializing the unit with the users selected parameters

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User Object: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

     Returns:
         JSON Object: information about the added unit

     Raises:
         None
     """

    data = request.get_json()
    logging.info('using data to reserve unit')
    logging.info(data)
    current_user = session_token.get('current_user')
    if services.unit_controller.reserve_unit_for_user(user=current_user, data=data):
        return jsonify({"success": True, "message": "Successfully reserved unit!"})
    else:
        return jsonify({"success": False, "message": "That unit size is no longer available, please try again"})


def edit_unit(session_token):
    """Updates the current users unit with the information in the JSON Object in the request

    Args:
        session_token Dict: token containing the following information about logged in user.
        - current_user User Object: Currently logged in user
        - firebase_token Mapping[str, Any]: Decoded firebase token

     Returns:
         JSON Object: Status information about the entire operation

     Raises:
         None
     """

    data = request.get_json()

    if services.unit_controller.edit_unit(data=data, unit_id=data.get('unit_id', None)):
        return jsonify({"success": True, "message": "Successfully updated unit!"})
    else:
        return jsonify({"success": False, "message": "Failed to update unit, please contact PODit support"})
